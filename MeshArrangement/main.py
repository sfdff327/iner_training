import numpy as np
import pandas as pd
import time
import dem
import pyvista as pv
import plotPyvista

import os
import pyFabSTL



def createSmallDomain(domainCenter,dx,dy,dz):
  xyz=np.zeros((26,3),dtype=np.float64)
  xyz[0]=domainCenter-np.array([dx/2,dy/2,dz/2])
  
  xyz[1]=xyz[0]+np.array([dx,0.,0.])
  xyz[2]=xyz[0]+np.array([dx,dy,0.])
  xyz[3]=xyz[0]+np.array([0.,dy,0.])
  xyz[4]=xyz[0]+np.array([0.,0.,dz])
  xyz[5]=xyz[1]+np.array([0.,0.,dz])
  xyz[6]=xyz[2]+np.array([0.,0.,dz])
  xyz[7]=xyz[3]+np.array([0.,0.,dz])

  xyz[8] =xyz[0]+np.array([dx/2,0.,0.])
  xyz[9] =xyz[0]+np.array([dx,dy/2,0.])
  xyz[10]=xyz[0]+np.array([dx/2,dy,0.])
  xyz[11]=xyz[0]+np.array([0.,dy/2,0.])

  xyz[12]=xyz[4]+np.array([dx/2,0.,0.])
  xyz[13]=xyz[4]+np.array([dx,dy/2,0.])
  xyz[14]=xyz[4]+np.array([dx/2,dy,0.])
  xyz[15]=xyz[4]+np.array([0.,dy/2,0.])

  xyz[16]=xyz[0]+np.array([0.,0.,250.])
  xyz[17]=xyz[16]+np.array([dx/2.,0.,0.])
  xyz[18]=xyz[16]+np.array([dx,0.,0.])
  xyz[19]=xyz[16]+np.array([dx,dy/2,0.])
  xyz[20]=xyz[16]+np.array([dx,dy,0.])
  xyz[21]=xyz[16]+np.array([dx/2.,dy,0.])
  xyz[22]=xyz[16]+np.array([0.,dy,0.])
  xyz[23]=xyz[16]+np.array([0.,dy/2,0.])

  xyz[24]=xyz[0]+np.array([dx/2.,dy/2,0.])
  xyz[25]=xyz[4]+np.array([dx/2.,dy/2,0.])

  faces=np.hstack([
[4, 0,11,24,8],[4,11,3,10,24],[4,8,24,9,1],[4, 24,10,2,9],[4,4,15,23,16],[4,16,23,11,0],
[4,15,7,22,23],[4,23,22,3,11],[4,5,12,17,18],[4, 18,17,8,1],[4,12,4,16,17],[4, 17,16,0,8],
[4,6,13,19,20],[4, 20,19,9,2],[4,13,5,18,19],[4, 19,18,1,9],[4,7,14,21,22],[4,22,21,10,3],
[4,14,6,20,21],[4,21,20,2,10],[4,4,12,25,15],[4,15,25,14,7],[4,12,5,13,25],[4,25,13,6,14],])
  smallDomain=pv.PolyData(xyz,faces)

  ''' 利用等差數列函式, 創造面的編號'''
  nb=-np.linspace(0,24,num=24,endpoint=False,dtype=np.int64)

  ''' 將每個面的編號存入vtk檔案'''
  smallDomain['faceNb']=nb

  smallDomain.save('smallDomain.vtk')
  # print(smallDomain.points)
  # print(smallDomain.faces)
  
  return smallDomain


if __name__ == "__main__":
  start = time.time()
  
  OriginPointsF=r'../../INER_DATA/Domain2D/OriginPoints.txt'
  # path=r'../../HybridTW/000_fabToMesh/testWriteMeshALL.poly'
  # q123Folder=r'../../HybridTW/001_stlToMesh'
  # FABResfolder=r'../../HybridTW/000_fabToMesh/FABRes'

  EpsgIn=3825
  EpsgOut=4326

  '''
  # 模擬底部高程
  '''
  STLminZ=-2000.0

  '''
  # 表土層深度
  '''
  depthSurfaceSoil=-70.0  

  # OriginPointsF=r'/root/ihsienlee/CGAL/CGAL_LEE/INER_DATA/Domain2D/OriginPoints.txt'
  orPointCsv = pd.read_csv(OriginPointsF,sep='\\s+')
  orPointCsv = orPointCsv.fillna('')
  orPointCsv.columns=["x", "y"]
  delta_x=np.float64(orPointCsv['x'].values[0])
  delta_y=np.float64(orPointCsv['y'].values[0])
  # print(orPointCsv)
  print("delta_x,delta_y=",delta_x,delta_y)
  # quit()  

  CsvFile=r'../../INER_DATA/Domain2D/DEM_120m_resolution500m_201x_139y.dat'
  domainFile=r'../../INER_DATA/Domain2D/domainboundary_large.xy'
  nx=201
  ny=139
  # CsvFile=r'../../INER_DATA/Domain2D/DTM_largescale_263x_263y.dat'
  # nx=263
  # ny=263  

  case=0
  if (case==1):
    '''
    # 讀取500m DEM
    '''    
    xyzCsv = pd.read_csv(CsvFile,sep='\\s+',header=None)
    xyzCsv = xyzCsv.fillna('')
    xyzCsv.columns=["x", "y", "z"]   
    # print(xyzCsv) 
    # quit()
    '''csv points to vtk'''
    points=np.zeros((len(xyzCsv),3),dtype=np.float64)
    points[:,0]=xyzCsv['x'].values[:]
    points[:,1]=xyzCsv['y'].values[:]
    xyzPoints = pv.PolyData(points)
    xyzPoints.save("xyzPoints.vtk")


    ''' # 讀取模擬範圍,並且依據dem計算表面高程 '''    
    xyzDomain = pd.read_csv(domainFile,sep='\\s+',header=None)
    xyzDomain = xyzDomain.fillna('')
    xyzDomain.columns=["x", "y"]
    

    domain2d,demClip2d,largeDem= dem.demToMesh(nx,ny,xyzCsv,xyzDomain)
    print("read dem and doamin files")
    domain2d,demClip2d,largeDem=plotPyvista.dem2dClip(delta_x,delta_y,EpsgIn,EpsgOut,domain2d,demClip2d,largeDem)
  # quit()
  domain2d=pv.read('domain2d.vtk')
  demClip2d=pv.read('demClip2d.vtk')
  largeDem=pv.read('largeDem.vtk') 
  # print("largeDem") 
  # print(largeDem)

  case=0
  if (case==1):   
    STLminZ=np.float64(STLminZ)
    '''
    use demClip2d create 3d dem
    '''
    demClip3dsurf=plotPyvista.dem2dTo3d(demClip2d)
    StlFile=r'../../INER_DATA/STL/GeoModel_SKB_AModify_fault_1.stl'
    fault_1=pv.read(StlFile)
    StlFile=r'../../INER_DATA/STL/GeoModel_SKB_AModify_fracture.stl'
    fault_2=pv.read(StlFile)
    fault_1Mtr=100.0
    fault_2Mtr=20.0
    fault12=plotPyvista.faultStl(demClip2d,delta_x,delta_y,fault_1,fault_2,fault_1Mtr,fault_2Mtr)
    boxMtr=500.0
    poly3d=plotPyvista.mergeOutsideBox(depthSurfaceSoil,demClip3dsurf,STLminZ,boxMtr)    
  else:
    demClip3dsurf=pv.read('demClip3dsurf.vtk')
    poly3d=pv.read('poly3d.vtk')
    fault12=pv.read('fault12.vtk')
  # print(fault12.array_names)
  # print(poly3d.array_names)
  # print('poly3d, demclip3d and fault12')

  case=0
  if (case==1):
    dfndataF=r'dfnData.csv'    
    dfndata = pd.read_csv(dfndataF,sep='\\s+')
    # dfndata.columns=["no_node", "Transmissivity", "Storativity", "aperture"]
    no_node=dfndata['no_node']
    Transmissivity=dfndata['Transmissivity']
    Storativity=dfndata['Storativity']
    aperture=dfndata['aperture']
    fractureK=Transmissivity/aperture
    fracNb=0
    lineNb=0
    len_dfndata=len(no_node)
    dfnmesh = pv.PolyData()
    while(lineNb<len_dfndata):      
      fracNb=fracNb+1
      no_each_frac_node=np.int64(no_node[lineNb])
      fractureK=np.float64(Transmissivity[lineNb])/np.float64(aperture[lineNb])
      fractureAperture=np.float64(aperture[lineNb])
      xyz=np.zeros((no_each_frac_node,3),dtype=np.float64)
      faces=np.linspace(start=-1,stop=no_each_frac_node,num=no_each_frac_node+1,endpoint=False,dtype=np.int64)
      faces[0]=no_each_frac_node
      xyz[:,0]=np.float64(Transmissivity[lineNb+1:lineNb+1+no_each_frac_node])
      xyz[:,1]=np.float64(Storativity[lineNb+1:lineNb+1+no_each_frac_node])
      xyz[:,2]=np.float64(aperture[lineNb+1:lineNb+1+no_each_frac_node])
      lineNb=lineNb+1+no_each_frac_node
      dfnpoly=pv.PolyData(xyz,faces)
      dfnpoly.translate([delta_x,delta_y,0],inplace=True)
      dfnpoly['fractureK']=[fractureK]
      dfnpoly['aperture']=[fractureAperture]
      dfnpoly['No']=[fracNb]
      dfnmesh=dfnmesh+dfnpoly
    dfnmesh.save('dfnPoly.vtk') 
    dfnPoly=dfnmesh
    print('fracNb=',fracNb)
  elif (case==0):
    dfnPoly=pv.read('dfnPoly.vtk') 
  # print("dfnPoly",dfnPoly)
  # print("dfnPoly.array_names=",dfnPoly.array_names)

  case=0
  if (case==1):
    noface=np.max(dfnPoly["No"])
    ind=np.zeros(noface,dtype=np.int64)
    fractureK=np.zeros(noface,dtype=np.float64)
    aperture=np.zeros(noface,dtype=np.float64)
    for i in range(0,dfnPoly.n_cells):
      # if (ind[dfnPoly["No"][i]-1]==0):
      #  fractureK[dfnPoly["No"][i]-1]=dfnPoly["fractureK"][i]
      #  aperture[dfnPoly["No"][i]-1]=dfnPoly["aperture"][i]
      if (ind[i]==0):
       fractureK[i]=dfnPoly["fractureK"][i]
       aperture[i]=dfnPoly["aperture"][i]
    np.savez('dfnPolyNp', 
    fractureK=fractureK, 
    aperture=aperture)
    print("save dfnPolyNp OK")  
  dfnPolyNp = np.load('dfnPolyNp.npz')
  print("dfnPolyNp OK")    

  '''網格參數'''
  sp= 50.       # 模擬範圍(處置區外)網格密度
  smallsp=50.   # 處置區周圍網格密度
  dfnsp =20.    # DFN網格密度
  edzsp=20.     # EDZ網格密度
  max_angle=181.0  # max三角形網格角度
  min_area=0.      # min三角形面積

  '''水文參數設定'''
  fracture_alpha_w=1.0 
  alpha=0.008
  sa=3.2                      # 海水密度參數
  density=1.0*(1.0+alpha*sa)  # 地下水密度

  matrix_alpha_w=4.8
  Ksurface=1.0e-5             # 表土層K
  poro_surface=1.e-3          # 表土層孔隙率
  surface_alpha_w=4.8

  F1F2K=5.0e-6 # 基準案例
  # F1F2K=1.0e-04 # 3號案例 改斷層
  # F1F2K=1.0e-08 # 4號案例 改斷層
  poro_F1F2K=0.4
  F1F2K_alpha_w=1.0

  Krock=[1.0e-10,3.3e-8] # 基準案例'matrix', 'edz'
  # Krock=[4.1e-12,3.3e-8] # 1號案例只有改母岩 'matrix', 'edz'
  # Krock=[1.0e-09,3.3e-8] # 2號案例只有改母岩 'matrix', 'edz'
  poro_rock=[1.0e-5,1.0e-4] # 'matrix', 'edz'

  ''' EDZ厚度'''
  edzThickness=0.3

  ''' 
  # edz重建
  '''
  edz=pv.read(r'edzNew.vtk')

  '''
  # 計算dfn與edz座標範圍
  '''
  dfncoorMax=np.max(dfnPoly.points,axis=0)
  dfncoorMin=np.min(dfnPoly.points,axis=0)
  edzcoorMax=np.max(edz.points,axis=0)
  edzcoorMin=np.min(edz.points,axis=0)

  ''' # 計算處置區範圍'''
  coormax=np.zeros(3,dtype=np.float64)
  coormin=np.zeros(3,dtype=np.float64)
  for i in range(0,3):
    if dfncoorMax[i]>=edzcoorMax[i]:
      coormax[i]=dfncoorMax[i]
    else:
      coormax[i]=edzcoorMax[i]
    if dfncoorMin[i]<=edzcoorMin[i]:
      coormin[i]=dfncoorMin[i]
    else:
      coormin[i]=edzcoorMin[i]

  ''' # 計算處置區xyz方向長度'''
  lencoor=coormax-coormin
  print('lencoor=',lencoor)

  ''' 計算處置區中心座標'''
  domainCenter=(coormax+coormin)/2

  ''' 固定處置區中心z座標=-500'''
  domainCenter[2]=np.float64(-500.0)
  # quit()

  ''' create local small domain vtk'''
  smallDomain=createSmallDomain(
    domainCenter,
    lencoor[0]+250.0,
    lencoor[1]+250.0,
    500)
  # quit()

  ''' 計算模擬邊界內處置設施與dfn等三角形交錯關係'''
  case=1
  if (case==1):    
    innerOBJ=pyFabSTL.mesh_arrangement(
      dfnPoly,
      smallDomain,
      edz,
      smallsp,dfnsp,edzsp,
      max_angle,min_area) 
  else:
    innerOBJ=pv.read("innerOBJ.vtk")