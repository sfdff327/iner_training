import numpy as np
import re
import pyvista as pv
import os
from pathlib import Path
import pickle
from scipy.io import FortranFile

import pyToTecplot

import copy
from meshpy.tet import MeshInfo, build,Options
import vtk
import pandas as pd
from mesh import Mesh

from  gltflib  import (
     GLTF , GLTFModel , Asset , Scene , Node , Mesh , Primitive , Attributes , Buffer , BufferView , Accessor , AccessorType ,
     BufferTarget , ComponentType , GLBResource , FileResource )
# from xvfbwrapper import Xvfb
# display = Xvfb(width=1920, height=1080)
# display.start()

def readOneReposiroty():

  points=np.zeros((30,3),dtype=np.float64)
  points[ 0]=[-916.601882279655,447.611526237952,-493.096093006794]
  points[ 1]=[-342.811482228282,1444.02431314562,-493.255504976025]
  points[ 2]=[-917.344441598413,448.039871085727,-493.255504976213]
  points[ 3]=[-342.068922908843,1443.59596829789,-493.096093006607]
  points[ 4]=[-344.929645689963,1445.24617432117,-494.166054227598]
  points[ 5]=[-336.267513339034,1440.24942906922,-500.000000054379]
  points[ 6]=[-344.929645689962,1445.24617432117,-500.000000054379]
  points[ 7]=[-336.267513339198,1440.24942906932,-494.485093166346]
  points[ 8]=[-336.927957801324,1440.63040601433,-494.062061838412]
  points[ 9]=[-344.247186683558,1444.85249830188,-493.792483988421]
  points[10]=[-337.617528633780,1441.02818448791,-493.706322919683]
  points[11]=[-343.539376089161,1444.44419823164,-493.488212661032]
  points[12]=[-338.331093272216,1441.43980377286,-493.420524220650]
  points[13]=[-339.063340565000,1441.86220013228,-493.206792977759]
  points[14]=[-339.808820298387,1442.29222961972,-493.066720020120]
  points[15]=[-341.317225095870,1443.16235192021,-493.011163276553]
  points[16]=[-340.561983773752,1442.72669146509,-493.001347928759]
  points[17]=[-919.462605059664,449.261732262778,-500.000000054566]
  points[18]=[-919.462605059665,449.261732262777,-494.166054227785]
  points[19]=[-918.072335459052,448.459756172920,-493.488212661220]
  points[20]=[-918.780146055832,448.868056240164,-493.792483988609]
  points[21]=[-910.800472709946,444.264987007597,-500.000000054566]
  points[22]=[-910.800472709947,444.264987007596,-494.485093166534]
  points[23]=[-915.094943144400,446.742249405057,-493.001347928946]
  points[24]=[-915.850184466530,447.177909857123,-493.011163276740]
  points[25]=[-911.460917170938,444.645963954579,-494.062061838600]
  points[26]=[-912.150488004605,445.043742430601,-493.706322919871]
  points[27]=[-912.864052643872,445.455361712969,-493.420524220837]
  points[28]=[-913.596299936114,445.877758072960,-493.206792977946]
  points[29]=[-914.341779669716,446.307787559648,-493.066720020307]
  faces=[]
  faces.append([	3	,	0	,	1	,	2	])
  faces.append([	3	,	1	,	0	,	3	])
  faces.append([	3	,	4	,	5	,	6	])
  faces.append([	3	,	5	,	4	,	7	])
  faces.append([	3	,	7	,	4	,	8	])
  faces.append([	3	,	8	,	4	,	9	])
  faces.append([	3	,	8	,	9	,	10	])
  faces.append([	3	,	10	,	9	,	11	])
  faces.append([	3	,	10	,	11	,	12	])
  faces.append([	3	,	12	,	11	,	1	])
  faces.append([	3	,	12	,	1	,	13	])
  faces.append([	3	,	13	,	1	,	3	])
  faces.append([	3	,	13	,	3	,	14	])
  faces.append([	3	,	14	,	3	,	15	])
  faces.append([	3	,	14	,	15	,	16	])
  faces.append([	3	,	4	,	17	,	18	])
  faces.append([	3	,	17	,	4	,	6	])
  faces.append([	3	,	19	,	9	,	20	])
  faces.append([	3	,	9	,	19	,	11	])
  faces.append([	3	,	21	,	7	,	22	])
  faces.append([	3	,	7	,	21	,	5	])
  faces.append([	3	,	23	,	15	,	24	])
  faces.append([	3	,	15	,	23	,	16	])
  faces.append([	3	,	2	,	11	,	19	])
  faces.append([	3	,	11	,	2	,	1	])
  faces.append([	3	,	22	,	8	,	25	])
  faces.append([	3	,	8	,	22	,	7	])
  faces.append([	3	,	24	,	3	,	0	])
  faces.append([	3	,	3	,	24	,	15	])
  faces.append([	3	,	6	,	21	,	17	])
  faces.append([	3	,	21	,	6	,	5	])
  faces.append([	3	,	25	,	18	,	22	])
  faces.append([	3	,	18	,	25	,	20	])
  faces.append([	3	,	20	,	25	,	26	])
  faces.append([	3	,	20	,	26	,	19	])
  faces.append([	3	,	19	,	26	,	27	])
  faces.append([	3	,	19	,	27	,	2	])
  faces.append([	3	,	2	,	27	,	28	])
  faces.append([	3	,	2	,	28	,	0	])
  faces.append([	3	,	0	,	28	,	29	])
  faces.append([	3	,	0	,	29	,	24	])
  faces.append([	3	,	24	,	29	,	23	])
  faces.append([	3	,	22	,	17	,	21	])
  faces.append([	3	,	17	,	22	,	18	])
  faces.append([	3	,	20	,	4	,	18	])
  faces.append([	3	,	4	,	20	,	9	])
  faces.append([	3	,	29	,	16	,	23	])
  faces.append([	3	,	16	,	29	,	14	])
  faces.append([	3	,	28	,	14	,	29	])
  faces.append([	3	,	14	,	28	,	13	])
  faces.append([	3	,	27	,	13	,	28	])
  faces.append([	3	,	13	,	27	,	12	])
  faces.append([	3	,	26	,	12	,	27	])
  faces.append([	3	,	12	,	26	,	10	])
  faces.append([	3	,	25	,	10	,	26	])
  faces.append([	3	,	10	,	25	,	8	])
  mt=pv.PolyData(points,np.hstack(faces))
  
  points=np.zeros((30,3),dtype=np.float64)
  points[ 0]=[-280.455064361281,1036.48898004843,-495.333518793952]
  points[ 1]=[-496.621338067389,1162.06713597970,-495.200000052309]
  points[ 2]=[-496.924284086961,1161.54196241506,-495.333518793884]
  points[ 3]=[-280.151650222554,1037.01388357931,-495.200000052375]
  points[ 4]=[-279.842023700059,1037.54953446976,-495.248128982444]
  points[ 5]=[-496.022932645983,1163.10450464593,-495.473843072748]
  points[ 6]=[-496.312189250269,1162.60306243459,-495.248128982378]
  points[ 7]=[-279.552320129702,1038.05071884942,-495.473843072814]
  points[ 8]=[-280.943497891457,1035.64399475790,-496.086037025073]
  points[ 9]=[-497.555533303721,1160.44765722560,-496.641517065155]
  points[10]=[-281.087288998593,1035.39523753100,-496.641517065222]
  points[11]=[-497.411964043143,1160.69654242454,-496.086037025007]
  points[12]=[-281.115006392075,1035.34878324785,-496.925799579462]
  points[13]=[-497.582560466093,1160.40080415512,-497.880438535743]
  points[14]=[-281.115006385230,1035.34878325061,-497.880438535810]
  points[15]=[-497.582560468995,1160.40080415009,-496.925799579395]
  points[16]=[-495.777984173978,1163.52913627627,-495.858089961740]
  points[17]=[-279.306993157522,1038.47513214303,-495.858089961807]
  points[18]=[-279.126750645444,1038.78694994934,-496.368435680720]
  points[19]=[-495.598019745459,1163.84111449790,-496.368435680653]
  points[20]=[-280.726655173458,1036.01913056705,-495.637414997335]
  points[21]=[-497.195455877842,1161.07187122109,-495.637414997268]
  points[22]=[-495.487038125179,1164.03350723247,-500.000000052326]
  points[23]=[-279.015597539918,1038.97924374879,-497.588104355040]
  points[24]=[-279.015597532046,1038.97924376127,-500.000000052391]
  points[25]=[-495.487038131790,1164.03350722101,-497.588104354975]
  points[26]=[-495.498230003980,1164.01410549584,-496.961802348529]
  points[27]=[-279.026806706183,1038.95985199928,-496.961802348595]
  points[28]=[-281.114357924325,1035.34840855163,-500.000000052598]
  points[29]=[-497.582560465360,1160.40080415639,-500.000000052531]
  faces=[]
  faces.append([	3	,	0	,	1	,	2	])
  faces.append([	3	,	1	,	0	,	3	])
  faces.append([	3	,	4	,	5	,	6	])
  faces.append([	3	,	5	,	4	,	7	])
  faces.append([	3	,	3	,	6	,	1	])
  faces.append([	3	,	6	,	3	,	4	])
  faces.append([	3	,	8	,	9	,	10	])
  faces.append([	3	,	9	,	8	,	11	])
  faces.append([	3	,	12	,	13	,	14	])
  faces.append([	3	,	13	,	12	,	15	])
  faces.append([	3	,	7	,	16	,	5	])
  faces.append([	3	,	16	,	7	,	17	])
  faces.append([	3	,	18	,	16	,	17	])
  faces.append([	3	,	16	,	18	,	19	])
  faces.append([	3	,	20	,	11	,	8	])
  faces.append([	3	,	11	,	20	,	21	])
  faces.append([	3	,	22	,	23	,	24	])
  faces.append([	3	,	23	,	22	,	25	])
  faces.append([	3	,	23	,	26	,	27	])
  faces.append([	3	,	26	,	23	,	25	])
  faces.append([	3	,	27	,	19	,	18	])
  faces.append([	3	,	19	,	27	,	26	])
  faces.append([	3	,	28	,	14	,	29	])
  faces.append([	3	,	29	,	14	,	13	])
  faces.append([	3	,	23	,	28	,	24	])
  faces.append([	3	,	28	,	23	,	14	])
  faces.append([	3	,	14	,	23	,	12	])
  faces.append([	3	,	12	,	23	,	27	])
  faces.append([	3	,	12	,	27	,	18	])
  faces.append([	3	,	12	,	18	,	10	])
  faces.append([	3	,	10	,	18	,	8	])
  faces.append([	3	,	8	,	18	,	17	])
  faces.append([	3	,	8	,	17	,	20	])
  faces.append([	3	,	20	,	17	,	7	])
  faces.append([	3	,	20	,	7	,	0	])
  faces.append([	3	,	0	,	7	,	4	])
  faces.append([	3	,	0	,	4	,	3	])
  faces.append([	3	,	20	,	2	,	21	])
  faces.append([	3	,	2	,	20	,	0	])
  faces.append([	3	,	9	,	12	,	10	])
  faces.append([	3	,	12	,	9	,	15	])
  faces.append([	3	,	15	,	26	,	25	])
  faces.append([	3	,	26	,	15	,	19	])
  faces.append([	3	,	19	,	15	,	9	])
  faces.append([	3	,	19	,	9	,	11	])
  faces.append([	3	,	19	,	11	,	16	])
  faces.append([	3	,	16	,	11	,	21	])
  faces.append([	3	,	16	,	21	,	5	])
  faces.append([	3	,	5	,	21	,	2	])
  faces.append([	3	,	5	,	2	,	6	])
  faces.append([	3	,	6	,	2	,	1	])
  faces.append([	3	,	13	,	25	,	29	])
  faces.append([	3	,	25	,	13	,	15	])
  faces.append([	3	,	22	,	28	,	29	])
  faces.append([	3	,	28	,	22	,	24	])
  faces.append([	3	,	25	,	22	,	29	])
  dt=pv.PolyData(points,np.hstack(faces))

  points=np.zeros((144,3),dtype=np.float64)
  points[	0	]=	[	-478.5693797	,	1151.107501	,	-500.0000001	]
  points[	1	]=	[	-478.5886087	,	1151.335111	,	-508.1550001	]
  points[	2	]=	[	-478.5693797	,	1151.107501	,	-508.1550001	]
  points[	3	]=	[	-478.5886087	,	1151.335111	,	-500.0000001	]
  points[	4	]=	[	-478.1868968	,	1150.557774	,	-508.1550001	]
  points[	5	]=	[	-478.3614383	,	1150.705122	,	-500.0000001	]
  points[	6	]=	[	-478.3614383	,	1150.705122	,	-508.1550001	]
  points[	7	]=	[	-478.1868968	,	1150.557774	,	-500.0000001	]
  points[	8	]=	[	-476.8405099	,	1151.25356	,	-500.0000001	]
  points[	9	]=	[	-476.880846	,	1151.028728	,	-508.1550001	]
  points[	10	]=	[	-476.8405099	,	1151.25356	,	-508.1550001	]
  points[	11	]=	[	-476.880846	,	1151.028728	,	-500.0000001	]
  points[	12	]=	[	-478.5482727	,	1151.559942	,	-500.0000001	]
  points[	13	]=	[	-478.4511205	,	1151.766673	,	-508.1550001	]
  points[	14	]=	[	-478.5482727	,	1151.559942	,	-508.1550001	]
  points[	15	]=	[	-478.4511205	,	1151.766673	,	-500.0000001	]
  points[	16	]=	[	-478.3037728	,	1151.941214	,	-508.1550001	]
  points[	17	]=	[	-478.3037728	,	1151.941214	,	-500.0000001	]
  points[	18	]=	[	-476.9779982	,	1150.821998	,	-508.1550001	]
  points[	19	]=	[	-476.9779982	,	1150.821998	,	-500.0000001	]
  points[	20	]=	[	-478.4918962	,	1150.892623	,	-508.1550001	]
  points[	21	]=	[	-478.4918962	,	1150.892623	,	-500.0000001	]
  points[	22	]=	[	-477.3128474	,	1150.516998	,	-500.0000001	]
  points[	23	]=	[	-477.527725	,	1150.439515	,	-508.1550001	]
  points[	24	]=	[	-477.3128474	,	1150.516998	,	-508.1550001	]
  points[	25	]=	[	-477.527725	,	1150.439515	,	-500.0000001	]
  points[	26	]=	[	-477.755335	,	1150.420286	,	-508.1550001	]
  points[	27	]=	[	-477.9801662	,	1150.460622	,	-500.0000001	]
  points[	28	]=	[	-477.9801662	,	1150.460622	,	-508.1550001	]
  points[	29	]=	[	-477.755335	,	1150.420286	,	-500.0000001	]
  points[	30	]=	[	-477.1253458	,	1150.647456	,	-500.0000001	]
  points[	31	]=	[	-477.1253458	,	1150.647456	,	-508.1550001	]
  points[	32	]=	[	-477.6737837	,	1152.168385	,	-500.0000001	]
  points[	33	]=	[	-477.9013937	,	1152.149156	,	-508.1550001	]
  points[	34	]=	[	-477.9013937	,	1152.149156	,	-500.0000001	]
  points[	35	]=	[	-477.6737837	,	1152.168385	,	-508.1550001	]
  points[	36	]=	[	-478.1162713	,	1152.071672	,	-508.1550001	]
  points[	37	]=	[	-478.1162713	,	1152.071672	,	-500.0000001	]
  points[	38	]=	[	-477.0676803	,	1151.883549	,	-508.1550001	]
  points[	39	]=	[	-477.4489524	,	1152.128049	,	-508.1550001	]
  points[	40	]=	[	-477.2422219	,	1152.030896	,	-508.1550001	]
  points[	41	]=	[	-476.9372225	,	1151.696047	,	-508.1550001	]
  points[	42	]=	[	-476.8597389	,	1151.48117	,	-508.1550001	]
  points[	43	]=	[	-477.4489524	,	1152.128049	,	-500.0000001	]
  points[	44	]=	[	-477.0676803	,	1151.883549	,	-500.0000001	]
  points[	45	]=	[	-477.2422219	,	1152.030896	,	-500.0000001	]
  points[	46	]=	[	-476.9372225	,	1151.696047	,	-500.0000001	]
  points[	47	]=	[	-476.8597389	,	1151.48117	,	-500.0000001	]
  points[	48	]=	[	-458.5827042	,	1185.756207	,	-500.0000001	]
  points[	49	]=	[	-458.6019332	,	1185.983817	,	-508.1550001	]
  points[	50	]=	[	-458.5827042	,	1185.756207	,	-508.1550001	]
  points[	51	]=	[	-458.6019332	,	1185.983817	,	-500.0000001	]
  points[	52	]=	[	-458.2002212	,	1185.20648	,	-508.1550001	]
  points[	53	]=	[	-458.3747628	,	1185.353827	,	-500.0000001	]
  points[	54	]=	[	-458.3747628	,	1185.353827	,	-508.1550001	]
  points[	55	]=	[	-458.2002212	,	1185.20648	,	-500.0000001	]
  points[	56	]=	[	-456.8538344	,	1185.902265	,	-500.0000001	]
  points[	57	]=	[	-456.8941704	,	1185.677434	,	-508.1550001	]
  points[	58	]=	[	-456.8538344	,	1185.902265	,	-508.1550001	]
  points[	59	]=	[	-456.8941704	,	1185.677434	,	-500.0000001	]
  points[	60	]=	[	-458.5615971	,	1186.208648	,	-500.0000001	]
  points[	61	]=	[	-458.4644449	,	1186.415378	,	-508.1550001	]
  points[	62	]=	[	-458.5615971	,	1186.208648	,	-508.1550001	]
  points[	63	]=	[	-458.4644449	,	1186.415378	,	-500.0000001	]
  points[	64	]=	[	-458.3170973	,	1186.58992	,	-508.1550001	]
  points[	65	]=	[	-458.3170973	,	1186.58992	,	-500.0000001	]
  points[	66	]=	[	-456.9913226	,	1185.470703	,	-508.1550001	]
  points[	67	]=	[	-456.9913226	,	1185.470703	,	-500.0000001	]
  points[	68	]=	[	-458.5052206	,	1185.541329	,	-508.1550001	]
  points[	69	]=	[	-458.5052206	,	1185.541329	,	-500.0000001	]
  points[	70	]=	[	-457.3261718	,	1185.165704	,	-500.0000001	]
  points[	71	]=	[	-457.5410494	,	1185.08822	,	-508.1550001	]
  points[	72	]=	[	-457.3261718	,	1185.165704	,	-508.1550001	]
  points[	73	]=	[	-457.5410494	,	1185.08822	,	-500.0000001	]
  points[	74	]=	[	-457.7686594	,	1185.068992	,	-508.1550001	]
  points[	75	]=	[	-457.9934907	,	1185.109328	,	-500.0000001	]
  points[	76	]=	[	-457.9934907	,	1185.109328	,	-508.1550001	]
  points[	77	]=	[	-457.7686594	,	1185.068992	,	-500.0000001	]
  points[	78	]=	[	-457.1386703	,	1185.296162	,	-500.0000001	]
  points[	79	]=	[	-457.1386703	,	1185.296162	,	-508.1550001	]
  points[	80	]=	[	-457.6871081	,	1186.81709	,	-500.0000001	]
  points[	81	]=	[	-457.9147181	,	1186.797861	,	-508.1550001	]
  points[	82	]=	[	-457.9147181	,	1186.797861	,	-500.0000001	]
  points[	83	]=	[	-457.6871081	,	1186.81709	,	-508.1550001	]
  points[	84	]=	[	-458.1295957	,	1186.720378	,	-508.1550001	]
  points[	85	]=	[	-458.1295957	,	1186.720378	,	-500.0000001	]
  points[	86	]=	[	-457.0810048	,	1186.532254	,	-508.1550001	]
  points[	87	]=	[	-457.4622769	,	1186.776754	,	-508.1550001	]
  points[	88	]=	[	-457.2555463	,	1186.679602	,	-508.1550001	]
  points[	89	]=	[	-456.9505469	,	1186.344753	,	-508.1550001	]
  points[	90	]=	[	-456.8730634	,	1186.129875	,	-508.1550001	]
  points[	91	]=	[	-457.4622769	,	1186.776754	,	-500.0000001	]
  points[	92	]=	[	-457.0810048	,	1186.532254	,	-500.0000001	]
  points[	93	]=	[	-457.2555463	,	1186.679602	,	-500.0000001	]
  points[	94	]=	[	-456.9505469	,	1186.344753	,	-500.0000001	]
  points[	95	]=	[	-456.8730634	,	1186.129875	,	-500.0000001	]
  points[	96	]=	[	-498.5560553	,	1116.458795	,	-500.0000001	]
  points[	97	]=	[	-498.5752843	,	1116.686405	,	-508.1550001	]
  points[	98	]=	[	-498.5560553	,	1116.458795	,	-508.1550001	]
  points[	99	]=	[	-498.5752843	,	1116.686405	,	-500.0000001	]
  points[	100	]=	[	-498.1735724	,	1115.909069	,	-508.1550001	]
  points[	101	]=	[	-498.3481139	,	1116.056416	,	-500.0000001	]
  points[	102	]=	[	-498.3481139	,	1116.056416	,	-508.1550001	]
  points[	103	]=	[	-498.1735724	,	1115.909069	,	-500.0000001	]
  points[	104	]=	[	-496.8271855	,	1116.604854	,	-500.0000001	]
  points[	105	]=	[	-496.8675216	,	1116.380023	,	-508.1550001	]
  points[	106	]=	[	-496.8271855	,	1116.604854	,	-508.1550001	]
  points[	107	]=	[	-496.8675216	,	1116.380023	,	-500.0000001	]
  points[	108	]=	[	-498.5349483	,	1116.911237	,	-500.0000001	]
  points[	109	]=	[	-498.4377961	,	1117.117967	,	-508.1550001	]
  points[	110	]=	[	-498.5349483	,	1116.911237	,	-508.1550001	]
  points[	111	]=	[	-498.4377961	,	1117.117967	,	-500.0000001	]
  points[	112	]=	[	-498.2904484	,	1117.292509	,	-508.1550001	]
  points[	113	]=	[	-498.2904484	,	1117.292509	,	-500.0000001	]
  points[	114	]=	[	-496.9646738	,	1116.173292	,	-508.1550001	]
  points[	115	]=	[	-496.9646738	,	1116.173292	,	-500.0000001	]
  points[	116	]=	[	-498.4785717	,	1116.243918	,	-508.1550001	]
  points[	117	]=	[	-498.4785717	,	1116.243918	,	-500.0000001	]
  points[	118	]=	[	-497.299523	,	1115.868293	,	-500.0000001	]
  points[	119	]=	[	-497.5144006	,	1115.790809	,	-508.1550001	]
  points[	120	]=	[	-497.299523	,	1115.868293	,	-508.1550001	]
  points[	121	]=	[	-497.5144006	,	1115.790809	,	-500.0000001	]
  points[	122	]=	[	-497.7420106	,	1115.77158	,	-508.1550001	]
  points[	123	]=	[	-497.9668418	,	1115.811916	,	-500.0000001	]
  points[	124	]=	[	-497.9668418	,	1115.811916	,	-508.1550001	]
  points[	125	]=	[	-497.7420106	,	1115.77158	,	-500.0000001	]
  points[	126	]=	[	-497.1120214	,	1115.998751	,	-500.0000001	]
  points[	127	]=	[	-497.1120214	,	1115.998751	,	-508.1550001	]
  points[	128	]=	[	-497.6604593	,	1117.519679	,	-500.0000001	]
  points[	129	]=	[	-497.8880693	,	1117.50045	,	-508.1550001	]
  points[	130	]=	[	-497.8880693	,	1117.50045	,	-500.0000001	]
  points[	131	]=	[	-497.6604593	,	1117.519679	,	-508.1550001	]
  points[	132	]=	[	-498.1029469	,	1117.422967	,	-508.1550001	]
  points[	133	]=	[	-498.1029469	,	1117.422967	,	-500.0000001	]
  points[	134	]=	[	-497.0543559	,	1117.234843	,	-508.1550001	]
  points[	135	]=	[	-497.435628	,	1117.479343	,	-508.1550001	]
  points[	136	]=	[	-497.2288975	,	1117.382191	,	-508.1550001	]
  points[	137	]=	[	-496.9238981	,	1117.047342	,	-508.1550001	]
  points[	138	]=	[	-496.8464145	,	1116.832464	,	-508.1550001	]
  points[	139	]=	[	-497.435628	,	1117.479343	,	-500.0000001	]
  points[	140	]=	[	-497.0543559	,	1117.234843	,	-500.0000001	]
  points[	141	]=	[	-497.2288975	,	1117.382191	,	-500.0000001	]
  points[	142	]=	[	-496.9238981	,	1117.047342	,	-500.0000001	]
  points[	143	]=	[	-496.8464145	,	1116.832464	,	-500.0000001	]       
  faces=[]
  faces.append([	3	,	0	,	1	,	2	])
  faces.append([	3	,	1	,	0	,	3	])
  faces.append([	3	,	4	,	5	,	6	])
  faces.append([	3	,	5	,	4	,	7	])
  faces.append([	3	,	8	,	9	,	10	])
  faces.append([	3	,	9	,	8	,	11	])
  faces.append([	3	,	12	,	13	,	14	])
  faces.append([	3	,	13	,	12	,	15	])
  faces.append([	3	,	15	,	16	,	13	])
  faces.append([	3	,	16	,	15	,	17	])
  faces.append([	3	,	11	,	18	,	9	])
  faces.append([	3	,	18	,	11	,	19	])
  faces.append([	3	,	20	,	0	,	2	])
  faces.append([	3	,	0	,	20	,	21	])
  faces.append([	3	,	22	,	23	,	24	])
  faces.append([	3	,	23	,	22	,	25	])
  faces.append([	3	,	26	,	27	,	28	])
  faces.append([	3	,	27	,	26	,	29	])
  faces.append([	3	,	30	,	24	,	31	])
  faces.append([	3	,	24	,	30	,	22	])
  faces.append([	3	,	32	,	33	,	34	])
  faces.append([	3	,	33	,	32	,	35	])
  faces.append([	3	,	23	,	29	,	26	])
  faces.append([	3	,	29	,	23	,	25	])
  faces.append([	3	,	17	,	36	,	16	])
  faces.append([	3	,	36	,	17	,	37	])
  faces.append([	3	,	37	,	33	,	36	])
  faces.append([	3	,	33	,	37	,	34	])
  faces.append([	3	,	3	,	14	,	1	])
  faces.append([	3	,	14	,	3	,	12	])
  faces.append([	3	,	38	,	39	,	40	])
  faces.append([	3	,	39	,	38	,	35	])
  faces.append([	3	,	35	,	38	,	41	])
  faces.append([	3	,	35	,	41	,	33	])
  faces.append([	3	,	33	,	41	,	42	])
  faces.append([	3	,	33	,	42	,	36	])
  faces.append([	3	,	36	,	42	,	10	])
  faces.append([	3	,	36	,	10	,	16	])
  faces.append([	3	,	16	,	10	,	9	])
  faces.append([	3	,	16	,	9	,	13	])
  faces.append([	3	,	13	,	9	,	18	])
  faces.append([	3	,	13	,	18	,	14	])
  faces.append([	3	,	14	,	18	,	31	])
  faces.append([	3	,	14	,	31	,	1	])
  faces.append([	3	,	1	,	31	,	24	])
  faces.append([	3	,	1	,	24	,	2	])
  faces.append([	3	,	2	,	24	,	23	])
  faces.append([	3	,	2	,	23	,	20	])
  faces.append([	3	,	20	,	23	,	26	])
  faces.append([	3	,	20	,	26	,	6	])
  faces.append([	3	,	6	,	26	,	28	])
  faces.append([	3	,	6	,	28	,	4	])
  faces.append([	3	,	43	,	35	,	32	])
  faces.append([	3	,	35	,	43	,	39	])
  faces.append([	3	,	43	,	44	,	45	])
  faces.append([	3	,	44	,	43	,	32	])
  faces.append([	3	,	44	,	32	,	46	])
  faces.append([	3	,	46	,	32	,	34	])
  faces.append([	3	,	46	,	34	,	47	])
  faces.append([	3	,	47	,	34	,	37	])
  faces.append([	3	,	47	,	37	,	8	])
  faces.append([	3	,	8	,	37	,	17	])
  faces.append([	3	,	8	,	17	,	11	])
  faces.append([	3	,	11	,	17	,	15	])
  faces.append([	3	,	11	,	15	,	19	])
  faces.append([	3	,	19	,	15	,	12	])
  faces.append([	3	,	19	,	12	,	30	])
  faces.append([	3	,	30	,	12	,	3	])
  faces.append([	3	,	30	,	3	,	22	])
  faces.append([	3	,	22	,	3	,	0	])
  faces.append([	3	,	22	,	0	,	25	])
  faces.append([	3	,	25	,	0	,	21	])
  faces.append([	3	,	25	,	21	,	29	])
  faces.append([	3	,	29	,	21	,	5	])
  faces.append([	3	,	29	,	5	,	27	])
  faces.append([	3	,	27	,	5	,	7	])
  faces.append([	3	,	45	,	39	,	43	])
  faces.append([	3	,	39	,	45	,	40	])
  faces.append([	3	,	44	,	40	,	45	])
  faces.append([	3	,	40	,	44	,	38	])
  faces.append([	3	,	47	,	10	,	42	])
  faces.append([	3	,	10	,	47	,	8	])
  faces.append([	3	,	47	,	41	,	46	])
  faces.append([	3	,	41	,	47	,	42	])
  faces.append([	3	,	46	,	38	,	44	])
  faces.append([	3	,	38	,	46	,	41	])
  faces.append([	3	,	28	,	7	,	4	])
  faces.append([	3	,	7	,	28	,	27	])
  faces.append([	3	,	6	,	21	,	20	])
  faces.append([	3	,	21	,	6	,	5	])
  faces.append([	3	,	19	,	31	,	18	])
  faces.append([	3	,	31	,	19	,	30	])
  faces.append([	3	,	48	,	49	,	50	])
  faces.append([	3	,	49	,	48	,	51	])
  faces.append([	3	,	52	,	53	,	54	])
  faces.append([	3	,	53	,	52	,	55	])
  faces.append([	3	,	56	,	57	,	58	])
  faces.append([	3	,	57	,	56	,	59	])
  faces.append([	3	,	60	,	61	,	62	])
  faces.append([	3	,	61	,	60	,	63	])
  faces.append([	3	,	63	,	64	,	61	])
  faces.append([	3	,	64	,	63	,	65	])
  faces.append([	3	,	59	,	66	,	57	])
  faces.append([	3	,	66	,	59	,	67	])
  faces.append([	3	,	68	,	48	,	50	])
  faces.append([	3	,	48	,	68	,	69	])
  faces.append([	3	,	70	,	71	,	72	])
  faces.append([	3	,	71	,	70	,	73	])
  faces.append([	3	,	74	,	75	,	76	])
  faces.append([	3	,	75	,	74	,	77	])
  faces.append([	3	,	78	,	72	,	79	])
  faces.append([	3	,	72	,	78	,	70	])
  faces.append([	3	,	80	,	81	,	82	])
  faces.append([	3	,	81	,	80	,	83	])
  faces.append([	3	,	71	,	77	,	74	])
  faces.append([	3	,	77	,	71	,	73	])
  faces.append([	3	,	65	,	84	,	64	])
  faces.append([	3	,	84	,	65	,	85	])
  faces.append([	3	,	85	,	81	,	84	])
  faces.append([	3	,	81	,	85	,	82	])
  faces.append([	3	,	51	,	62	,	49	])
  faces.append([	3	,	62	,	51	,	60	])
  faces.append([	3	,	86	,	87	,	88	])
  faces.append([	3	,	87	,	86	,	83	])
  faces.append([	3	,	83	,	86	,	89	])
  faces.append([	3	,	83	,	89	,	81	])
  faces.append([	3	,	81	,	89	,	90	])
  faces.append([	3	,	81	,	90	,	84	])
  faces.append([	3	,	84	,	90	,	58	])
  faces.append([	3	,	84	,	58	,	64	])
  faces.append([	3	,	64	,	58	,	57	])
  faces.append([	3	,	64	,	57	,	61	])
  faces.append([	3	,	61	,	57	,	66	])
  faces.append([	3	,	61	,	66	,	62	])
  faces.append([	3	,	62	,	66	,	79	])
  faces.append([	3	,	62	,	79	,	49	])
  faces.append([	3	,	49	,	79	,	72	])
  faces.append([	3	,	49	,	72	,	50	])
  faces.append([	3	,	50	,	72	,	71	])
  faces.append([	3	,	50	,	71	,	68	])
  faces.append([	3	,	68	,	71	,	74	])
  faces.append([	3	,	68	,	74	,	54	])
  faces.append([	3	,	54	,	74	,	76	])
  faces.append([	3	,	54	,	76	,	52	])
  faces.append([	3	,	91	,	83	,	80	])
  faces.append([	3	,	83	,	91	,	87	])
  faces.append([	3	,	91	,	92	,	93	])
  faces.append([	3	,	92	,	91	,	80	])
  faces.append([	3	,	92	,	80	,	94	])
  faces.append([	3	,	94	,	80	,	82	])
  faces.append([	3	,	94	,	82	,	95	])
  faces.append([	3	,	95	,	82	,	85	])
  faces.append([	3	,	95	,	85	,	56	])
  faces.append([	3	,	56	,	85	,	65	])
  faces.append([	3	,	56	,	65	,	59	])
  faces.append([	3	,	59	,	65	,	63	])
  faces.append([	3	,	59	,	63	,	67	])
  faces.append([	3	,	67	,	63	,	60	])
  faces.append([	3	,	67	,	60	,	78	])
  faces.append([	3	,	78	,	60	,	51	])
  faces.append([	3	,	78	,	51	,	70	])
  faces.append([	3	,	70	,	51	,	48	])
  faces.append([	3	,	70	,	48	,	73	])
  faces.append([	3	,	73	,	48	,	69	])
  faces.append([	3	,	73	,	69	,	77	])
  faces.append([	3	,	77	,	69	,	53	])
  faces.append([	3	,	77	,	53	,	75	])
  faces.append([	3	,	75	,	53	,	55	])
  faces.append([	3	,	93	,	87	,	91	])
  faces.append([	3	,	87	,	93	,	88	])
  faces.append([	3	,	92	,	88	,	93	])
  faces.append([	3	,	88	,	92	,	86	])
  faces.append([	3	,	95	,	58	,	90	])
  faces.append([	3	,	58	,	95	,	56	])
  faces.append([	3	,	95	,	89	,	94	])
  faces.append([	3	,	89	,	95	,	90	])
  faces.append([	3	,	94	,	86	,	92	])
  faces.append([	3	,	86	,	94	,	89	])
  faces.append([	3	,	76	,	55	,	52	])
  faces.append([	3	,	55	,	76	,	75	])
  faces.append([	3	,	54	,	69	,	68	])
  faces.append([	3	,	69	,	54	,	53	])
  faces.append([	3	,	67	,	79	,	66	])
  faces.append([	3	,	79	,	67	,	78	])
  faces.append([	3	,	96	,	97	,	98	])
  faces.append([	3	,	97	,	96	,	99	])
  faces.append([	3	,	100	,	101	,	102	])
  faces.append([	3	,	101	,	100	,	103	])
  faces.append([	3	,	104	,	105	,	106	])
  faces.append([	3	,	105	,	104	,	107	])
  faces.append([	3	,	108	,	109	,	110	])
  faces.append([	3	,	109	,	108	,	111	])
  faces.append([	3	,	111	,	112	,	109	])
  faces.append([	3	,	112	,	111	,	113	])
  faces.append([	3	,	107	,	114	,	105	])
  faces.append([	3	,	114	,	107	,	115	])
  faces.append([	3	,	116	,	96	,	98	])
  faces.append([	3	,	96	,	116	,	117	])
  faces.append([	3	,	118	,	119	,	120	])
  faces.append([	3	,	119	,	118	,	121	])
  faces.append([	3	,	122	,	123	,	124	])
  faces.append([	3	,	123	,	122	,	125	])
  faces.append([	3	,	126	,	120	,	127	])
  faces.append([	3	,	120	,	126	,	118	])
  faces.append([	3	,	128	,	129	,	130	])
  faces.append([	3	,	129	,	128	,	131	])
  faces.append([	3	,	119	,	125	,	122	])
  faces.append([	3	,	125	,	119	,	121	])
  faces.append([	3	,	113	,	132	,	112	])
  faces.append([	3	,	132	,	113	,	133	])
  faces.append([	3	,	133	,	129	,	132	])
  faces.append([	3	,	129	,	133	,	130	])
  faces.append([	3	,	99	,	110	,	97	])
  faces.append([	3	,	110	,	99	,	108	])
  faces.append([	3	,	134	,	135	,	136	])
  faces.append([	3	,	135	,	134	,	131	])
  faces.append([	3	,	131	,	134	,	137	])
  faces.append([	3	,	131	,	137	,	129	])
  faces.append([	3	,	129	,	137	,	138	])
  faces.append([	3	,	129	,	138	,	132	])
  faces.append([	3	,	132	,	138	,	106	])
  faces.append([	3	,	132	,	106	,	112	])
  faces.append([	3	,	112	,	106	,	105	])
  faces.append([	3	,	112	,	105	,	109	])
  faces.append([	3	,	109	,	105	,	114	])
  faces.append([	3	,	109	,	114	,	110	])
  faces.append([	3	,	110	,	114	,	127	])
  faces.append([	3	,	110	,	127	,	97	])
  faces.append([	3	,	97	,	127	,	120	])
  faces.append([	3	,	97	,	120	,	98	])
  faces.append([	3	,	98	,	120	,	119	])
  faces.append([	3	,	98	,	119	,	116	])
  faces.append([	3	,	116	,	119	,	122	])
  faces.append([	3	,	116	,	122	,	102	])
  faces.append([	3	,	102	,	122	,	124	])
  faces.append([	3	,	102	,	124	,	100	])
  faces.append([	3	,	139	,	131	,	128	])
  faces.append([	3	,	131	,	139	,	135	])
  faces.append([	3	,	139	,	140	,	141	])
  faces.append([	3	,	140	,	139	,	128	])
  faces.append([	3	,	140	,	128	,	142	])
  faces.append([	3	,	142	,	128	,	130	])
  faces.append([	3	,	142	,	130	,	143	])
  faces.append([	3	,	143	,	130	,	133	])
  faces.append([	3	,	143	,	133	,	104	])
  faces.append([	3	,	104	,	133	,	113	])
  faces.append([	3	,	104	,	113	,	107	])
  faces.append([	3	,	107	,	113	,	111	])
  faces.append([	3	,	107	,	111	,	115	])
  faces.append([	3	,	115	,	111	,	108	])
  faces.append([	3	,	115	,	108	,	126	])
  faces.append([	3	,	126	,	108	,	99	])
  faces.append([	3	,	126	,	99	,	118	])
  faces.append([	3	,	118	,	99	,	96	])
  faces.append([	3	,	118	,	96	,	121	])
  faces.append([	3	,	121	,	96	,	117	])
  faces.append([	3	,	121	,	117	,	125	])
  faces.append([	3	,	125	,	117	,	101	])
  faces.append([	3	,	125	,	101	,	123	])
  faces.append([	3	,	123	,	101	,	103	])
  faces.append([	3	,	141	,	135	,	139	])
  faces.append([	3	,	135	,	141	,	136	])
  faces.append([	3	,	140	,	136	,	141	])
  faces.append([	3	,	136	,	140	,	134	])
  faces.append([	3	,	143	,	106	,	138	])
  faces.append([	3	,	106	,	143	,	104	])
  faces.append([	3	,	143	,	137	,	142	])
  faces.append([	3	,	137	,	143	,	138	])
  faces.append([	3	,	142	,	134	,	140	])
  faces.append([	3	,	134	,	142	,	137	])
  faces.append([	3	,	124	,	103	,	100	])
  faces.append([	3	,	103	,	124	,	123	])
  faces.append([	3	,	102	,	117	,	116	])
  faces.append([	3	,	117	,	102	,	101	])
  faces.append([	3	,	115	,	127	,	114	])
  faces.append([	3	,	127	,	115	,	126	])
  dh=pv.PolyData(points,np.hstack(faces))
    
  return dh,dt,mt

def createEDZ():
  # file=r'output.bin'
  # flowTHPath = Path(file)
  # if flowTHPath.is_file():            
  #   f = FortranFile(file, 'r')
  #   no_node = f.read_ints(dtype=np.int32)[0]
  #   no_ele = f.read_ints(dtype=np.int32)[0]
  #   xyz = f.read_reals(dtype=np.float64).reshape(-1,3)
  #   ele_ind = f.read_reals(dtype=np.int32).reshape(-1,3)-1
  #   labels = f.read_ints(dtype=np.int32)
  dz=0.3 #10.  #0.3
  tP=np.array([-336.26751709, 1440.24938965, -500.])

  points=np.zeros((424*2,3),dtype=np.float64)
  points[0]=[263.86373901, 1058.2220459 , -500.]
  points[1]=[255.20161438, 1063.21875,    -500.]

  points[2]=[ 501.88040161,  870.46972656, -500.]
  points[3]=[ 242.08292955, 1020.46381642, -500.]
  points[4]=[ 233.38092773, 1025.39139381, -500.]
  points[5]=[ -26.38717842, 1175.45788574, -500.]
  points[6]=[ 499.78442383,  866.83728027, -500.]
  points[7]=[ 239.98741216, 1016.83112193, -500.]
  points[8]=[ 231.28308105, 1021.76000977, -500.]
  points[9]=[ -28.48593903, 1171.82702637, -500.]

  points[10]=[ 481.89370728,  835.82098389, -500.] 
  # points[11]=[ 222.09596188,  985.8153102 , -500.] 
  # points[12]=[ 213.39392885,  990.74283588, -500.] 
  # points[13]=[ -46.37385559, 1140.8092041 , -500.] 
  # points[14]=[ 479.79776001,  832.1885376 , -500.] 
  # points[15]=[ 220.00044068,  982.18260911, -500.] 
  # points[16]=[ 211.29640198,  987.11132812, -500.] 
  # points[17]=[ -48.47261429, 1137.17834473, -500.]
  
  mtV=points[3]-points[0]
  dtV=points[10]-points[2]
  tV=tP-points[0]
  point10=points[10]

  for i in range(1,26):
    nb=2+i*8
    for j in range(0,8):
      # print(nb+j,nb-8+j)
      points[nb+j]=points[nb-8+j]+dtV

  points[10]=point10
  nb=2+26*8
  points[nb+0]=points[2+25*8+5]+mtV
  points[nb+1]=points[2+25*8+6]+mtV
  
  '''
  # zone 2
  '''
  Pnb=4+26*8
  points[0+Pnb]=[-336.26751709, 1440.24938965, -500.]
  points[1+Pnb]=[-344.92965698, 1445.24621582, -500.] 
  points[2+Pnb]=[-159.12539673, 1246.88867188, -500.] 
  points[3+Pnb]=[-375.59960938, 1371.94458008, -500.] 
  points[4+Pnb]=[-384.35381589, 1376.90226348, -500.] 
  points[5+Pnb]=[-600.79968262, 1501.86730957, -500.] 
  points[6+Pnb]=[-161.22416687, 1243.2578125 , -500.] 
  points[7+Pnb]=[-377.69836426, 1368.3137207 , -500.] 
  points[8+Pnb]=[-386.4493331 , 1373.2695689 , -500.] 
  points[9+Pnb]=[-602.89562988, 1498.23486328, -500.] 
  points[10+Pnb]=[-179.12342834, 1212.24658203, -500.]
  
  mtV=points[3+Pnb]-points[0+Pnb]
  dtV=points[10+Pnb]-points[2+Pnb]
  point10=points[10+Pnb]

  for i in range(1,26):
    nb=2+i*8
    for j in range(0,8):
      # print(nb+j,nb-8+j)
      points[nb+j+Pnb]=points[nb-8+j+Pnb]+dtV

  points[10+Pnb]=point10
  nb=2+26*8
  points[nb+0+Pnb]=points[2+25*8+5+Pnb]+mtV
  points[nb+1+Pnb]=points[2+25*8+6+Pnb]+mtV

  points[Pnb*2:Pnb*4,0:2]=points[0:Pnb*2,0:2]
  points[Pnb*2:Pnb*4,2:3]=-500-dz
  
  # for i in range(0,2*(4+26*8)):
  #   print(points[i])

  faces=[]
  ''' zone 1 Right Top'''
  faces.append([4,0,1,4,3])
  for i in range(0,26):
    nb=8*i
    faces.append([8,2+nb,3+nb,4+nb,5+nb,9+nb,8+nb,7+nb,6+nb])
  for i in range(0,25):
    nb=8*i
    faces.append([4,7+nb,8+nb,12+nb,11+nb])
  faces.append([4, 207, 208, 211, 210])  
  
  ''' zone 1 Lift Top'''
  # Pnb=4+26*8
  faces.append([4,0+Pnb,1+Pnb,4+Pnb,3+Pnb])
  for i in range(0,26):
    nb=8*i+Pnb
    faces.append([8,2+nb,3+nb,4+nb,5+nb,9+nb,8+nb,7+nb,6+nb])
  for i in range(0,25):
    nb=8*i+Pnb
    faces.append([4,7+nb,8+nb,12+nb,11+nb])
  faces.append([4, 207+Pnb, 208+Pnb, 211+Pnb, 210+Pnb]) 

  ''' zone 1 Right down'''
  Pnb2=(4+26*8)*2
  faces.append([4,0+Pnb2,3+Pnb2,4+Pnb2,1+Pnb2])
  for i in range(0,26):
    nb=8*i+Pnb2
    faces.append([8,2+nb,6+nb,7+nb,8+nb,9+nb,5+nb,4+nb,3+nb])
  for i in range(0,25):
    nb=8*i+Pnb2
    faces.append([4,7+nb,11+nb,12+nb,8+nb])
  faces.append([4,207+Pnb2,210+Pnb2, 211+Pnb2, 208+Pnb2])  
 
  ''' zone 1 Lift down'''
  # Pnb=4+26*8
  faces.append([4,0+Pnb+Pnb2,3+Pnb+Pnb2,4+Pnb+Pnb2,1+Pnb+Pnb2])
  for i in range(0,26):
    nb=8*i+Pnb+Pnb2
    faces.append([8,2+nb,6+nb,7+nb,8+nb,9+nb,5+nb,4+nb,3+nb])
  for i in range(0,25):
    nb=8*i+Pnb+Pnb2
    faces.append([4,7+nb,11+nb,12+nb,8+nb])
  faces.append([4,207+Pnb+Pnb2, 210+Pnb+Pnb2, 211+Pnb+Pnb2, 208+Pnb+Pnb2])  

  '''
  # Right profile
  '''
  faces.append([4, 4, 1, 1+Pnb2, 4+Pnb2])
  faces.append([4, 1, 0, 0+Pnb2, 1+Pnb2])
  faces.append([4, 0, 3, 3+Pnb2, 0+Pnb2])
  for i in range(0,26):
    nb=i*8
    faces.append([4, 3+nb, 2+nb, 2+nb+Pnb2, 3+nb+Pnb2])
    faces.append([4, 2+nb, 6+nb, 6+nb+Pnb2, 2+nb+Pnb2])
    faces.append([4, 6+nb, 7+nb, 7+nb+Pnb2, 6+nb+Pnb2])
    faces.append([4, 8+nb, 9+nb, 9+nb+Pnb2, 8+nb+Pnb2])
    faces.append([4, 9+nb, 5+nb, 5+nb+Pnb2, 9+nb+Pnb2])
    faces.append([4, 5+nb, 4+nb, 4+nb+Pnb2, 5+nb+Pnb2])
  for i in range(0,25):
    nb=i*8
    faces.append([4, 7+nb, 11+nb, 11+nb+Pnb2, 7+nb+Pnb2])
    faces.append([4, 12+nb, 8+nb, 8+nb+Pnb2, 12+nb+Pnb2])
  faces.append([4, 211, 208, 208+Pnb2, 211+Pnb2])
  faces.append([4, 210, 211, 211+Pnb2, 210+Pnb2])
  faces.append([4, 207, 210, 210+Pnb2, 207+Pnb2])

  '''
  # Lift profile
  '''
  faces.append([4, 4+Pnb, 1+Pnb, 1+Pnb2+Pnb, 4+Pnb2+Pnb])
  faces.append([4, 1+Pnb, 0+Pnb, 0+Pnb2+Pnb, 1+Pnb2+Pnb])
  faces.append([4, 0+Pnb, 3+Pnb, 3+Pnb2+Pnb, 0+Pnb2+Pnb])
  for i in range(0,26):
    nb=i*8+Pnb
    faces.append([4, 3+nb, 2+nb, 2+nb+Pnb2, 3+nb+Pnb2])
    faces.append([4, 2+nb, 6+nb, 6+nb+Pnb2, 2+nb+Pnb2])
    faces.append([4, 6+nb, 7+nb, 7+nb+Pnb2, 6+nb+Pnb2])
    faces.append([4, 8+nb, 9+nb, 9+nb+Pnb2, 8+nb+Pnb2])
    faces.append([4, 9+nb, 5+nb, 5+nb+Pnb2, 9+nb+Pnb2])
    faces.append([4, 5+nb, 4+nb, 4+nb+Pnb2, 5+nb+Pnb2])
  for i in range(0,25):
    nb=i*8+Pnb
    faces.append([4, 7+nb, 11+nb, 11+nb+Pnb2, 7+nb+Pnb2])
    faces.append([4, 12+nb, 8+nb, 8+nb+Pnb2, 12+nb+Pnb2])
  faces.append([4, 211+Pnb, 208+Pnb, 208+Pnb2+Pnb, 211+Pnb2+Pnb])
  faces.append([4, 210+Pnb, 211+Pnb, 211+Pnb2+Pnb, 210+Pnb2+Pnb])
  faces.append([4, 207+Pnb, 210+Pnb, 210+Pnb2+Pnb, 207+Pnb2+Pnb])

  edzTop=pv.PolyData(points,np.hstack(faces))
  # edzTop=edzTop.scale([1.0, 1.0, 1000.0], inplace=False)

  
  edzTop.save('edzNew.vtk')
  edzTop.save('edzNew.stl')

  # quit()


def fabRead(noTestFrac,delFracNb,TecFile,boolCheck,printInd=False):
  folder='/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes'
  FABResFileCheck = Path(folder+r'/no_frac.out')
  if FABResFileCheck.is_file():
    with open(folder+'/frac_XYZ.out', 'rb') as file:
      frac_XYZ = pickle.load(file)        
      file.close() 
    with open(folder+'/fracID.out', 'rb') as file:
      fracID = pickle.load(file)   
      file.close()     
    with open(folder+'/frac_ele_ind.out', 'rb') as file:
      frac_ele_ind = pickle.load(file) 
      file.close()           
    with open(folder+'/no_frac.out', 'rb') as file:
      ORno_frac = pickle.load(file)
      file.close()
    file=folder+'/frac_K_ap.bin'
    flowResFileCheck = Path(file)
    if (flowResFileCheck.is_file):
      f = FortranFile(file, 'r')
      frac_K = f.read_reals(dtype=np.float64)
      frac_ap = f.read_reals(dtype=np.float64)
      f.close()
    # print(np.max(fracID))
    # quit()

  no_frac=noTestFrac
  cells=[]
  frac_ele_k=[]
  frac_ele_ap=[]
  frac_local_ID=[]
  nodeCheck=np.zeros(len(frac_XYZ),dtype=np.int32)
  if (no_frac==None or no_frac<=1):
    no_frac=ORno_frac

  for i in range(0,len(fracID)):
    # if (fracID[i]<=no_frac and fracID[i] not in delFracNb):
    if (fracID[i]<=no_frac and int(fracID[i]) not in delFracNb):
      if (len(boolCheck)>0):
        addbool=boolCheck[i]
      else:
        addbool=True
      if addbool:
          # print("fracID[i]=",fracID[i])
          cells.append([3,frac_ele_ind[i][0],frac_ele_ind[i][1],frac_ele_ind[i][2]])
          for j in range(0,3):
            nodeCheck[frac_ele_ind[i][j]]=1
          frac_ele_k.append(np.float64(frac_K[fracID[i]]))
          frac_ele_ap.append(np.float64(frac_ap[fracID[i]]))
          frac_local_ID.append(np.int32(fracID[i]))

  no_points=np.sum(nodeCheck)
  points=np.zeros((no_points,3),dtype=np.float64)
  nb=-1
  for i in range(0,len(frac_XYZ)):
    if (nodeCheck[i]==1):
      nb=nb+1
      points[nb]=frac_XYZ[i]
      nodeCheck[i]=nb
  for i in range(0,len(cells)):
    for j in range(1,4):
      cells[i][j]=nodeCheck[cells[i][j]]

  no_cells=len(cells)
  cells=np.array(cells).ravel()             
  dfnOrUsg = pv.PolyData(points,cells) 
  
  dfnOrUsg=dfnOrUsg.compute_normals()
  frac_ele_k=np.array(frac_ele_k)
  frac_ele_ap=np.array(frac_ele_ap)
  dfnOrUsg['frac_ele_k']=frac_ele_k
  dfnOrUsg['frac_ele_ap']=frac_ele_ap    
  dfnOrUsg['fracID']=frac_local_ID  
 
  # plotter = pv.Plotter(off_screen=True)
  # plotter.add_mesh(dfnOrUsg, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=Truelighting=True,reset_camera=True,culling=False,metallic=1.0)   
  # plotter.show(screenshot='dfnOrUsg.png')
  # testPLOTfilename=r'dfnOrUsg.dat'
  # pyToTecplot.TriangleUSG_zCenter(testPLOTfilename,dfnOrUsg.points,dfnOrUsg.faces.reshape(-1,4),dfnOrUs['frac_ele_k']) 
  dfnOrUsg.save('dfnOrUsg.vtk')

  # '''
  if (printInd==True):
    dfn_ele_ind=copy.deepcopy(dfnOrUsg.faces.reshape(-1,4))
    with open(TecFile+'.dat','w',encoding = 'utf-8') as f:
      f.write('Title="XY2D_plot"\n')
      f.write('Variables="x(m)","y(m)","z(m)","label"\n')
      f.write('Zone N='+str(dfnOrUsg.n_points)+',E='+str(len(dfnOrUsg.faces.reshape(-1,4)))+',F=feblock,et=triangle\n')
      f.write('varlocation=([4]=cellcentered)\n')
      for i in range(0,dfnOrUsg.n_points):
        f.write(str(dfnOrUsg.points[i][0])+'\n')
      for i in range(0,dfnOrUsg.n_points):
        f.write(str(dfnOrUsg.points[i][1])+'\n')
      for i in range(0,dfnOrUsg.n_points):
        f.write(str(dfnOrUsg.points[i][2])+'\n')
      for i in range(0,len(dfn_ele_ind)):
        f.write(str(dfnOrUsg['fracID'][i])+'\n')
      for i in range(0,len(dfn_ele_ind)):
        f.write(str(dfn_ele_ind[i][1]+1)+' '+str(dfn_ele_ind[i][2]+1)+' '+str(dfn_ele_ind[i][3]+1)+'\n')
      if len(delFracNb)>0:
        nb=0
        for i in range(0,len(frac_ele_ind)):
          if (fracID[i] in delFracNb):  
            nb=nb+1
        # print("delFracNb=",delFracNb)
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","label"\n')
        f.write('Zone N='+str(len(frac_XYZ))+',E='+str(nb)+',F=feblock,et=triangle\n') 
        f.write('varlocation=([4]=cellcentered)\n')
        for i in range(0,len(frac_XYZ)):
          f.write(str(frac_XYZ[i][0])+'\n')
        for i in range(0,len(frac_XYZ)):
          f.write(str(frac_XYZ[i][1])+'\n')
        for i in range(0,len(frac_XYZ)):
          f.write(str(frac_XYZ[i][2])+'\n')
        for i in range(0,len(frac_ele_ind)):
          if (fracID[i] in delFracNb):  
            f.write(str(fracID[i])+'\n')
        for i in range(0,len(frac_ele_ind)):
          if (fracID[i] in delFracNb):
            f.write(str(frac_ele_ind[i][0]+1)+' '+str(frac_ele_ind[i][1]+1)+' '+str(frac_ele_ind[i][2]+1)+'\n')
      f.close() 
  # '''


  '''
  with open('dfnOrUsg.dat','w',encoding = 'utf-8') as f:
    f.write('Title="XY2D_plot"\n')
    f.write('Variables="x(m)","y(m)","z(m)"\n')
    f.write('Zone N='+str(dfnOrUsg.n_points)+',E='+str(dfnOrUsg.n_cells)+',F=fepoint,et=triangle\n')
    for i in range(0,dfnOrUsg.n_points):
      f.write(str(dfnOrUsg.points[i][0])+' '+str(dfnOrUsg.points[i][1])+' '+str(dfnOrUsg.points[i][2])+'\n')
    for i in range(0,dfnOrUsg.n_cells):
      f.write(str(frac_ele_ind[i][0]+1)+' '+str(frac_ele_ind[i][1]+1)+' '+str(frac_ele_ind[i][2]+1)+'\n')
    f.close()
  '''


  '''
  dfnOrUsg.save('dfnOrUsg.stl',binary=True)
  outputFileCheck = Path("output.obj")
  if outputFileCheck.is_file():
      os.remove(outputFileCheck) 
  os.system('./mesh_arrangement dfnOrUsg.stl')

  dfnPoly=pv.read("output.obj")
  '''
  # 計算mesh_arrangement之後,屬於DFN triangle
  '''
  centers=dfnPoly.cell_centers()
  labels=dfnOrUsg.find_closest_cell(centers.points)
  dfnPoly['labels']=labels
  # testPLOTfilename=r'dfnPoly.dat'
  # pyToTecplot.TriangleUSG_zCenter(testPLOTfilename,xyz,ele_ind,dfnPoly['labels'])  
  # dfnPoly.save('dfnPoly.vtk')
  # quit()
  '''
  return dfnOrUsg

def stlTetgenCenter(stl,sp):
  from mesh import Mesh
  stl.save('test.stl')
  my_mesh=Mesh(["test.stl"])
  my_mesh.solveIntersections()
  out_coords=my_mesh.get_out_coords().reshape(-1,3)
  out_tris=my_mesh.get_out_tris().reshape(-1,3)
  out_labels=np.int64(my_mesh.get_out_labels_bit())
  ele_ind_new=np.full((len(out_tris),4),3,dtype=np.int64)
  ele_ind_new[:,1:4]=out_tris[:,0:3]
  ele_ind_new=np.hstack(ele_ind_new)
  stlPoly=pv.PolyData(out_coords,ele_ind_new)

  mesh_info = MeshInfo()
  # '''
  tetgenF=r'test'
  with open(tetgenF+'.node','w',encoding = 'utf-8') as f:
    f.write(str(stlPoly.n_points)+'  3  1  1\n')
    nb=-1
    for i in range(0,stlPoly.n_points):
      nb=nb+1
      f.write(str(nb)+'  '+str(stlPoly.points[i][0])+'  '+str(stlPoly.points[i][1])+'  '+str(stlPoly.points[i][2])+'\n')
    f.close()
  with open(tetgenF+'.mtr','w',encoding = 'utf-8') as f:
    f.write(str(stlPoly.n_points)+' 1\n')
    for i in range(0,stlPoly.n_points):
      f.write(str(sp)+'\n')
    f.close()     

  cells=[]
  cellsMark=[]
  for i in range(0,stlPoly.n_cells):
    cells.append(out_tris[i,0:3].tolist())
    cellsMark.append(1)
  mesh_info = MeshInfo()
  mesh_info.load_node(tetgenF)
  mesh_info.load_mtr(tetgenF)
  mesh_info.set_facets(cells,cellsMark)
  tetgenSwitches='pqmAT1e-16'
  mesh = build(mesh_info, options=Options(switches=tetgenSwitches))
  # with open('merge2dElements.dat','w',encoding = 'utf-8') as f:
  #   f.write('Title="XY2D_plot"\n')
  #   f.write('Variables="x(m)","y(m)","z(m)","label"\n')
  #   f.write('Zone N='+str(len(mesh.points))+',E='+str(len(mesh.elements))+',F=feblock,et=tetrahedron\n')
  #   f.write('varlocation=([4]=cellcentered)\n')
  #   for i in range(0,len(mesh.points)):
  #     f.write(str(mesh.points[i][0])+'\n')
  #   for i in range(0,len(mesh.points)):
  #     f.write(str(mesh.points[i][1])+'\n')
  #   for i in range(0,len(mesh.points)):
  #     f.write(str(mesh.points[i][2])+'\n')
  #   for i in range(0,len(mesh.elements)):
  #     f.write(str(mesh.element_attributes[i])+'\n')
  #   for i in range(0,len(mesh.elements)):
  #     f.write(str(mesh.elements[i][0]+1)+' '+str(mesh.elements[i][1]+1)+' '+str(mesh.elements[i][2]+1)+' '+str(mesh.elements[i][3]+1)+'\n')
  #   f.close()

  meshTETRA,meshTETRAPoly=createPvUSG_TETRA(mesh)
  '''
  poly=createPvUSG_TRIANGLE(mesh)
  centers=poly.cell_centers()
  removeId=[]
  points=centers.points
  for i in range(0,centers.n_points):
    if i not in removeId:
      point=points[i]
      index=centers.find_closest_point(point=point, n=10)
      # print(index)
      for j in range(1,10):
        if index[j] not in removeId:
          dist = np.linalg.norm(point-centers.points[index[j]])
          if dist<=sp/2. :
            removeId.append(index[j])
          else:
            break
  removeId=np.unique(removeId)
  # print(removeId)
  # print(len(removeId))
  centers=centers.remove_cells(removeId)
  '''

  centers=meshTETRA.cell_centers()
  centers['mtr']=np.full(centers.n_points,sp,dtype=np.float64)
  # quit()

  return centers

def USGIntersection(usg,edz):
  # oneUsg=usg.extract_cells(10623501)
  # oneUsg=oneUsg.extract_surface()
  # print("oneUsg=",oneUsg)
  # oneUsg.save('oneUsg.vtk')
  # intersection, s1,s2 = oneUsg.intersection(edz, split_first=False, split_second=False)
  # print("intersection=",intersection)
  # quit()

  clipUsg =usg.clip_surface(edz, invert=True, value=50.0, compute_distance=True, progress_bar=False, crinkle=False)
  print(clipUsg)
  clipUsg.save('clipUsg.vtk')
  usgEleNb=clipUsg["eleNb"]
  # usgEleNb=np.linspace(start=0,stop=5,num=5,endpoint=False,dtype=np.int64)
  # usgEleNb=[0,1,2,3,4,5]
  

  return usgEleNb

def mesh_arrangement(dfnOrUsg,smallDomain,edz,smallsp,dfnsp,edzsp,max_angle,min_area):
  from mesh import Mesh

  case=1
  if (case==1):
    dfnOrUsg.save('dfnOrUsg.stl')
    smallDomain.save('smallDomain.stl')
    edz.save('edzNew.stl')   

    outputFileCheck = Path("output.obj") 
    if outputFileCheck.is_file():
      os.remove(outputFileCheck) 

    my_mesh=Mesh(["dfnOrUsg.stl","smallDomain.stl",'edzNew.stl'])
    
    my_mesh.solveIntersections()
    
    out_coords=my_mesh.get_out_coords().reshape(-1,3)
    out_tris=my_mesh.get_out_tris().reshape(-1,3)
    out_labels=np.int64(my_mesh.get_out_labels_bit())
    # print('out_coords=',out_coords)
    # print('out_tris=',out_tris)
    # print('out_labels=',out_labels)
    # quit()



  ele_ind_new=np.full((len(out_tris),4),3,dtype=np.int64)
  ele_ind_new[:,1:4]=out_tris[:,0:3]
  ele_ind_new=np.hstack(ele_ind_new)
  innerOBJ=pv.PolyData(out_coords,ele_ind_new)


  innerOBJ["labels"]=out_labels

  '''創造每個三角形編號'''
  innerOBJ["eleNb"]=np.linspace(
    start=0,stop=innerOBJ.n_cells,
    num=innerOBJ.n_cells,
    endpoint=False,dtype=np.int64)
  
  '''取得每個三角形中心(重心)'''
  innerOBJ_center=innerOBJ.cell_centers()

  '''利用原始edz, 切割innerOBJ 所有三角形中心'''
  innerOBJ_center_edz=innerOBJ_center.clip_surface(
    edz, 
    invert=True, value=0.0, 
    compute_distance=True, progress_bar=False, crinkle=False)
  # innerOBJ_center_edz.save("innerOBJ_center_edz.vtk")
  # quit()
  
  '''尋找裂隙在edz裡頭'''
  innerOBJBool=innerOBJ["eleNb"]==-1
  # print('innerOBJ["eleNb"][0:10]=',innerOBJ["eleNb"][0:10])
  # print("innerOBJBool=",innerOBJBool)
  # quit()
  for i in range(0,innerOBJ_center_edz.n_cells):
    if (
      innerOBJ["labels"][innerOBJ_center_edz["eleNb"][i]]==1):
      innerOBJBool[innerOBJ_center_edz["eleNb"][i]]=True
  
  '''去除edz內部的裂隙'''
  innerOBJ=innerOBJ.remove_cells(innerOBJBool,inplace=False)
  
  '''留下edz'''
  edzBool=innerOBJ['labels']!=100 #1號與10號True
  innerOBJ_edz=innerOBJ.remove_cells(edzBool,inplace=False) #留下100號=留下edz

  '''留下smallDomain'''
  smallDomainBool=innerOBJ['labels']!=10
  innerOBJ_smallDomain=innerOBJ.remove_cells(smallDomainBool,inplace=False)

  '''留下裂隙'''
  isdfn=innerOBJ['labels']>1
  innerOBJ_isdfn=innerOBJ.remove_cells(isdfn,inplace=False)
  # print(innerOBJ_notdfn)
  # print(innerOBJ_isdfn)
  
  '''計算網格品質，計算所有三角形最大角度'''
  '''最大角度>max_angle, 則會移除'''
  innerOBJ_angle=innerOBJ_isdfn.compute_cell_quality(quality_measure='max_angle')
  isGoodAngle=innerOBJ_angle['CellQuality']>max_angle
  innerOBJ_angle.remove_cells(isGoodAngle,inplace=True)
  
  '''計算網格品質，計算所有三角形面積'''
  
  innerOBJ_area=innerOBJ_angle.compute_cell_quality(quality_measure='area')
  # isGoodArea=innerOBJ_area['CellQuality']<min_area
  isGoodArea=np.logical_and(
    innerOBJ_area['CellQuality']<min_area,
    innerOBJ_area['CellQuality']>min_area-20.)
  
  '''面積<min_area, 則會移除'''
  innerOBJ_area.remove_cells(isGoodArea,inplace=True)

  '''設定網格密度，參數名稱=mtr'''
  innerOBJ_area['mtr']=np.full(innerOBJ_area.n_points,dfnsp,dtype=np.float64)
  innerOBJ_edz['mtr']=np.full(innerOBJ_edz.n_points,edzsp,dtype=np.float64)
  innerOBJ_smallDomain['mtr']=np.full(innerOBJ_smallDomain.n_points,smallsp,dtype=np.float64)

  '''將裂隙+edz+smallDomain vtk檔案合併'''
  innerOBJ_area=innerOBJ_area+innerOBJ_smallDomain +innerOBJ_edz
  innerOBJ_area.save('innerOBJ.vtk')
  return innerOBJ_area



def find_closestCell(innerOBJ,dfnOrUsg,dh,edz,dt,mt):


  # '''
  # cell_normals=innerOBJ.cell_normals
  centers=innerOBJ.cell_centers()
  case=1
  if case==1:
    innerOBJ['closest_cell']=np.zeros(innerOBJ.n_cells,dtype=np.int32)
    index=dfnOrUsg.find_closest_cell(centers.points)
    for i in range(0,len(index)):
      innerOBJ['closest_cell'][i]=dfnOrUsg['No'][index[i]]
    print("find_closest_cell OK")

    innerOBJ['containing_cell']=np.zeros(innerOBJ.n_cells,dtype=np.int32)
    index=dfnOrUsg.find_containing_cell(centers.points)
    for i in range(0,len(index)):
      innerOBJ['containing_cell'][i]=dfnOrUsg['No'][index[i]]  
    print("find_containing_cell OK")
    # print("np.max(innerOBJ['containing_cell'])=",np.max(innerOBJ['containing_cell']))
    innerOBJ.save('innerOBJ_closest.vtk')
  else:
    innerOBJ=pv.read('innerOBJ_closest.vtk')
  innerMegered=innerOBJ
  innerMegered.save('innerMegered.vtk')
  # '''

  '''
  # testPLOTfilename='innerMegered.dat'
  # innerMegered=innerMegered.triangulate()
  # xyz=innerMegered.points
  # ele_ind=innerMegered.faces.reshape(-1,4)
  # no_node = len(xyz)
  # no_ele = len(ele_ind)
  # with open(testPLOTfilename,'w',encoding = 'utf-8') as f:
  #   f.write('Title="XY2D_plot"\n')
  #   f.write('Variables="x(m)","y(m)","z(m)","containing_cell","labels"\n')
  #   f.write('Zone N='+str(no_node)+',E='+str(no_ele)+',F=FEBLOCK,ET=TRIANGLE\n') 
  #   f.write('varlocation=([1,2,3,4]=nodal,[5]=CELLCENTERED)\n')
  #   for i in range(0,no_node):
  #     f.write(str(xyz[i][0])+'\n')
  #   for i in range(0,no_node):
  #     f.write(str(xyz[i][1])+'\n')
  #   for i in range(0,no_node):
  #     f.write(str(xyz[i][2])+'\n')
  #   for i in range(0,no_ele):
  #     f.write(str(innerMegered['containing_cell'][i])+'\n')
  #   for i in range(0,no_ele):
  #     f.write(str(innerMegered['labels'][i])+'\n')
  #   for i in range(0,no_ele):
  #     f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+'\n')
  #   f.close()  
  # pyToTecplot.TriangleUSG_zCenter(testPLOTfilename,xyz,ele_ind,innerMegered['containing_cell']) 
  '''
  ''' 
  # 無法準確判斷cell 中心是否在dfnOrUsg內
  innerOBJ_isdfn_cells=innerOBJ.cell_centers()
  innerOBJ['closest_cell']=np.full(shape=innerOBJ.n_cells,fill_value=0,dtype=np.int64)
  copydfnOrUsg=dfnOrUsg.copy()
  copydfnOrUsg=copydfnOrUsg.triangulate()

  for i in range(0,copydfnOrUsg.n_cells):
    print(i,copydfnOrUsg.n_cells)
    isinside=copydfnOrUsg.point_is_inside_cell(i, innerOBJ_isdfn_cells.points)
    ind=np.where(isinside==True)
    # ind=list(zip(*np.where(isinside==True)))
    # print(ind)
    for j in range(0,len(ind[0])):
      # print(j,ind[0][j],dfnOrUsg['No'][i])
      if(innerOBJ['closest_cell'][ind[0][j]]==0):
        if (innerOBJ['labels'][ind[0][j]]==1):
          innerOBJ['closest_cell'][ind[0][j]]=copydfnOrUsg['No'][i]
  innerMegered=innerOBJ
  innerMegered.save('innerMegered.vtk')
  quit()
  '''
  return innerMegered

def triangleNormal(tri):
  U=tri[1]-tri[0]
  V=tri[2]-tri[0]
  # print("U=",U)
  # print("V=",V)
  N = np.array([
    U[1]*V[2] - U[2]*V[1],
    U[2]*V[0] - U[0]*V[2],
    U[0]*V[1] - U[1]*V[0]])
  # N=N/np.linalg.norm(N)
  # print("N=",N)
  return N

def point_in_triangle(tri,pt):
  one=1.0
  zero=1.0e-7
  Pru_zero=1.0e-11
  inout=False
  print((tri[0][0]+tri[1][0]+tri[2][0])/3-pt[0])
  area =determinant_value_new_2d(tri[0],tri[1],tri[2])
  area1=determinant_value_new_2d(pt,tri[0],tri[1])
  area2=determinant_value_new_2d(pt,tri[1],tri[2])
  area3=determinant_value_new_2d(pt,tri[2],tri[0])
  print(area,area1,area2,area3)
  print(abs(abs(area)-abs(area1)-abs(area2)-abs(area3)))

  if (abs(abs(area)-abs(area1)-abs(area2)-abs(area3))<zero):
    if (abs(area)>zero): test_area=area
    if (abs(area1)>zero): test_area=test_area*area1
    if (abs(area2)>zero): test_area=test_area*area2
    if (abs(area3)>zero): test_area=test_area*area3
    if (test_area>zero):
      inout=True
    else:
      if (area*area1>=0.0e0 and area*area2>=0.0e0 and area*area3>=0.0e0):
        inout=True
      else:
        disab=dis3d(tri[0],tri[1])
        disad=dis3d(tri[0],pt)
        disbd=dis3d(pt,tri[1])
        if (abs(disab-disad-disbd)<zero):
          inout=True
        else:
          disbc=dis3d(tri[1],tri[2])
          discd=dis3d(pt,tri[2])
          if (abs(disbc-disbd-discd)<zero):
            inout=True
          else:
            disac=dis3d(tri[2],tri[0])
            if (abs(disbc-disbd-discd)<zero):
              inout=True

  return inout

def dis3d(p1,p2):
  dis=np.linalg.norm(p1-p2)
  # dis2=((p1[0]-p2[0])**2.0+(p1[1]-p1[1])**2.0+(p1[2]-p2[2])**2.0)**0.5
  return dis

def determinant_value_new_2d(a,b,c):
  if (abs(a[2])<1.0e-12 and abs(b[2])<1.0e-12 and abs(c[2])<1.0e-12):
    a0=1.0
    b0=1.0
    c0=1.0
    v=(a[0]*b[1]*c0+b[0]*c[1]*a0+c[0]*a[1]*b0)-(c[0]*b[1]*a0+b[0]*a[1]*c0+a[0]*c[1]*b0)
  else:
   v=(a[0]*b[1]*c[2]+b[0]*c[1]*a[2]+c[0]*a[1]*b[2])-(c[0]*b[1]*a[2]+b[0]*a[1]*c[2]+a[0]*c[1]*b[2])
  return v

def writeNode(points,delPointNb,path_to_file):
  with open(path_to_file,'w',encoding = 'utf-8') as f:
    f.write(str(len(points))+'  3  0  0\n')
    nb=-1
    for i in range(0,len(points)):
      if (i not in delPointNb):
        nb=nb+1
        f.write(str(nb)+'  '+str(points[i][0])+'  '+str(points[i][1])+'  '+str(points[i][2])+'\n')
    f.close()

def runTetgen_coarsening(smallDomain,poly3d,fault12,edz_centers,edz,edzRegionTag,smallsp,dfnsp,edzsp):
  '''
  # 測試tetgen
  '''
  smallDomain=smallDomain.triangulate()
  xyz=smallDomain.points
  ele_ind=smallDomain.faces.reshape(-1,4)

  poly3d=poly3d.triangulate()
  poly3d_ele_ind=poly3d.faces.reshape(-1,4)
  # print('BF poly3d ele',poly3d_ele_ind[0:3,:],smallDomain.n_points)  
  poly3d_ele_ind[:,1:4]=poly3d_ele_ind[:,1:4]+smallDomain.n_points
  # print('AF poly3d ele',poly3d_ele_ind[0:3,:])
  # quit()

  # '''
  tetgenF=r'merge'
  # mergePoints=np.concatenate((smallDomain.points, poly3d.points), axis=0)
  # np.zeros((smallDomain.n_points+poly3d.n_points,3),dtype=np.float64)
  # mergePoints[0:smallDomain.n_points,0:3]=smallDomain.points[0:smallDomain.n_points,0:3]
  # mergePoints[smallDomain.n_points:smallDomain.n_points+poly3d.n_points,:]=poly3d.points[0:poly3d.n_points,0:3]
  # points=tuple(map(tuple, mergePoints))

  with open(tetgenF+'.node','w',encoding = 'utf-8') as f:
    f.write(str(smallDomain.n_points+poly3d.n_points)+'  3  1  1\n')
    nb=-1
    for i in range(0,smallDomain.n_points):
      nb=nb+1
      f.write(str(nb)+'  '+str(smallDomain.points[i][0])+'  '+str(smallDomain.points[i][1])+'  '+str(smallDomain.points[i][2])+'\n')
    for i in range(0,poly3d.n_points):
      nb=nb+1
      f.write(str(nb)+'  '+str(poly3d.points[i][0])+'  '+str(poly3d.points[i][1])+'  '+str(poly3d.points[i][2])+'\n')
    f.close()

  with open(tetgenF+'.mtr','w',encoding = 'utf-8') as f:
    f.write(str(smallDomain.n_points+poly3d.n_points)+' 1\n')
    for i in range(0,smallDomain.n_points):
      f.write(str(smallDomain['mtr'][i])+'\n')
    for i in range(0,poly3d.n_points):
      f.write(str(poly3d['mtr'][i])+'\n')
    f.close()
  # quit() 

  cells=[]
  cellsMark=[]
  sb=0
  # print(smallDomain.faces)
  for i in range(0,smallDomain.n_cells):
    # eb=smallDomain.faces[sb]+sb+1
    # print('BF sb,eb=',sb,eb,smallDomain.faces[sb:eb])
    cells.append(ele_ind[i,1:4].tolist())
    if (smallDomain['labels'][i]==1):
      cellsMark.append(smallDomain['containing_cell'][i])
    elif (smallDomain['labels'][i]==edzRegionTag):
      cellsMark.append(-edzRegionTag)
    else:
      cellsMark.append(0)
    # sb=sb+smallDomain.faces[sb]+1
    # print('AF sb,eb=',sb,eb)
    # if i==3: quit()
  for i in range(0,poly3d.n_cells):
    cells.append(poly3d_ele_ind[i,1:4].tolist())
    cellsMark.append(poly3d['boxFaceType'][i])
  print('len(cells)=',len(cells))
  print('len(cellsMark)=',len(cellsMark))

  with open(tetgenF+'_cells.dat','w',encoding='utf-8') as f:
    f.write('Title="XY2D_plot"\n')
    f.write('Variables="x(m)","y(m)","z(m)","label"\n')
    f.write('Zone N='+str(smallDomain.n_points+poly3d.n_points)+',E='+str(len(cells))+',F=feblock,et=triangle\n')
    f.write('varlocation=([4]=cellcentered)\n')
    for i in range(0,smallDomain.n_points):
      f.write(str(smallDomain.points[i][0])+'\n')
    for i in range(0,poly3d.n_points):
      f.write(str(poly3d.points[i][0])+'\n')
    for i in range(0,smallDomain.n_points):
      f.write(str(smallDomain.points[i][1])+'\n')
    for i in range(0,poly3d.n_points):
      f.write(str(poly3d.points[i][1])+'\n')
    for i in range(0,smallDomain.n_points):
      f.write(str(smallDomain.points[i][2])+'\n')
    for i in range(0,poly3d.n_points):
      f.write(str(poly3d.points[i][2])+'\n')
    for i in range(0,len(cells)):
      f.write(str(cellsMark[i])+'\n')
    for i in range(0,len(cells)):
      f.write(str(cells[i][0]+1)+' '+str(cells[i][1]+1)+' '+str(cells[i][2]+1)+'\n')
    f.close()
  # quit()
  # print(edz_centers)
  # for i in range(0,edz_centers.n_points):
  #   print(edz_centers.points[i])
  # quit()
  runTetgenCheck=0
  while runTetgenCheck==0:
    print("renew mesh_info")
    mesh_info = MeshInfo()
    mesh_info.load_node(tetgenF)
    # mesh_info.set_points(points)
    mesh_info.load_mtr(tetgenF)
    mesh_info.set_facets(cells,cellsMark)
    # mesh_info.regions.resize(int(edz_centers.n_points+3))
    # mesh_info.regions[0] = [900, 1800, -25,-1,1e+5,]
    # mesh_info.regions[1] = [900, 1800, -100,-2,1e+5,]
    # mesh_info.regions[2] = [800, 1700, -300,-3,1e+5,]     
    # for i in range(0,edz_centers.n_points):
    #   mesh_info.regions[i+2] = [edz_centers.points[i][0],edz_centers.points[i][1],edz_centers.points[i][2],-edzRegionTag,edzsp,]
    mesh_info.regions.resize(int(3))
    mesh_info.regions[0] = [900, 1800, -25,-1,1e+5,]
    mesh_info.regions[1] = [900, 1800, -100,-2,1e+5,]
    mesh_info.regions[2] = [800, 1700, -300,-3,1e+5,]     
    print("set regions OK")

    mesh_info.save_poly(tetgenF)
    path_to_file=tetgenF+'Out'
    file_exists = os.path.exists(path_to_file)
    if file_exists:
      os.remove(path_to_file)
    print('tetgen -dT1.0e-6 '+tetgenF+'.poly >'+path_to_file)
    os.system('tetgen -dT1.0e-6 '+tetgenF+'.poly >'+path_to_file)
    print("path_to_file=",path_to_file)
    file_exists = os.path.exists(path_to_file)
    print("file_exists=",file_exists)
    if file_exists:
      f = open(path_to_file)
      delNb=[]
      for line in f.readlines():      
        if 'No faces are intersecting.' in line :
          runTetgenCheck=1
          print("Self-intersection OK")
        if (' intersects facet #' in line) or (' duplicates facet #' in line):
          linelist =[int(s) for s in re.findall(r'-?\d+\.?\d*', line)][1]-1
          nb=(linelist)
          if (nb not in delNb):
            delNb.append(nb)
      f.close()  
      print("delNb=",delNb)
      if (len(delNb)>0):
        delNb=np.unique(delNb)
        for i in range(len(delNb)-1,-1,-1):
          del cells[delNb[i]] 
          del cellsMark[delNb[i]] 
        print("len(cells) AF",len(cells))
      else:
        mesh_info.save_poly(tetgenF)
        runTetgenCheck=1
        if file_exists:
          os.remove(path_to_file)
  print("tetgen start")
  # mesh = build(mesh_info, options=Options(switches='pmnAfT1e-16'))
  # tetgenSwitches='pqmMnAT1e-16' #必要 pMAT1e-16
  tetgenSwitches='pmMnAT1e-16'
  print('tetgenSwitches=',tetgenSwitches)
  mesh = build(mesh_info, options=Options(switches=tetgenSwitches))
  print("MeshCoarsening point=",len(mesh.points))
  print("MeshCoarsening elements=",len(mesh.elements))
  faces=[]
  eachNoFace=[]
  # print("OR cells=",cells)
  for i in range(0,len(cells)):
    faces.append(cells[i])
    if (len(cells[i])==3):
      eachNoFace.append(3)
    elif (len(cells[i])==4):
      eachNoFace.append(4)
  # print(faces)
  cells=np.hstack(faces)
  # '''

  points=np.array(mesh.points)
  mtr=np.array(mesh.point_metric_tensors)
  coarseningPoly=pv.PolyData(points)
  nodeNb=np.linspace(start=0,stop=coarseningPoly.n_points,num=coarseningPoly.n_points,endpoint=False,dtype=np.int64)
  coarseningPoly['mtr']=mtr
  coarseningPoly['nodeNb']=nodeNb

  clipCoarseningPoly_smallDomain=coarseningPoly.clip_surface(smallDomain, invert=True, value=smallsp, compute_distance=False, crinkle=False)
  for i in range(0,clipCoarseningPoly_smallDomain.n_points):
    if clipCoarseningPoly_smallDomain['mtr'][i]>smallsp:
      mesh.point_metric_tensors[clipCoarseningPoly_smallDomain['nodeNb'][i]]=smallsp


  clipCoarseningPoly_smallDomain=coarseningPoly.clip_surface(edz, invert=True, value=smallsp, compute_distance=False, crinkle=False)
  for i in range(0,clipCoarseningPoly_smallDomain.n_points):
    if clipCoarseningPoly_smallDomain['mtr'][i]>dfnsp:
      mesh.point_metric_tensors[clipCoarseningPoly_smallDomain['nodeNb'][i]]=dfnsp

  # clipcoarseningPoly=coarseningPoly.clip_surface(fault12, invert=True, value=0.0, compute_distance=False, crinkle=False) 
  # fault12.save('newfault12.vtk') 
  # clipcoarseningPoly.save('clipcoarseningPoly.vtk') 
  
  # quit()

  np.savez('meshUSG_coarsening', 
  points=np.array(mesh.points), 
  elements=np.array(mesh.elements), element_attributes=np.array(mesh.element_attributes),
  faces=np.array(mesh.faces),
  face_markers=np.array(mesh.face_markers),
  mtr=np.array(mesh.point_metric_tensors))
  print("save meshUSG_coarsening OK")

  
  # '''
  with open('merge2dFaces.dat','w',encoding = 'utf-8') as f:
    f.write('Title="XY2D_plot"\n')
    f.write('Variables="x(m)","y(m)","z(m)","label"\n')
    f.write('Zone N='+str(len(mesh.points))+',E='+str(len(mesh.faces))+',F=feblock,et=triangle\n')
    f.write('varlocation=([4]=cellcentered)\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][0])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][1])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][2])+'\n')
    for i in range(0,len(mesh.faces)):
      f.write(str(mesh.face_markers[i])+'\n')
    for i in range(0,len(mesh.faces)):
      f.write(str(mesh.faces[i][0]+1)+' '+str(mesh.faces[i][1]+1)+' '+str(mesh.faces[i][2]+1)+'\n')
    f.close()
  with open('merge2dElements.dat','w',encoding = 'utf-8') as f:
    f.write('Title="XY2D_plot"\n')
    f.write('Variables="x(m)","y(m)","z(m)","mtr","label"\n')
    f.write('Zone N='+str(len(mesh.points))+',E='+str(len(mesh.elements))+',F=feblock,et=tetrahedron\n')
    f.write('varlocation=([5]=cellcentered)\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][0])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][1])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][2])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.point_metric_tensors[i])+'\n')
    for i in range(0,len(mesh.elements)):
      f.write(str(mesh.element_attributes[i])+'\n')
    for i in range(0,len(mesh.elements)):
      f.write(str(mesh.elements[i][0]+1)+' '+str(mesh.elements[i][1]+1)+' '+str(mesh.elements[i][2]+1)+' '+str(mesh.elements[i][3]+1)+'\n')
    f.close()
  # '''
  
  return mesh

def createPvUSG_TETRA_byNpz(npz):
  points=npz["points"]
  elements=npz["elements"]
  nodeNb=np.linspace(start=0,stop=len(points)-1,num=len(points),endpoint=True,dtype=np.int64)
  eleNb=np.linspace(start=0,stop=len(elements)-1,num=len(elements),endpoint=True,dtype=np.int64)
  # element_attributes=npz["element_attributes"]
  meshfaces=npz["faces"]
  faceNb=np.linspace(start=0,stop=len(meshfaces)-1,num=len(meshfaces),endpoint=True,dtype=np.int64)
  faces=np.full((len(meshfaces),4),fill_value=3,dtype=np.int32)
  faces[:,1:]=meshfaces[:,:]
  faces=np.hstack(faces)
  # face_markers=npz["face_markers"]
  # mtr=npz["mtr"]

  cells=np.full((len(elements),5),4,dtype=np.int64) 
  cells[:,1:5]=elements[:,0:4]
  no_cells=len(cells)
  cells=np.array(cells).ravel()                
  celltypes = np.empty(no_cells, dtype=np.uint32)
  celltypes[:] = vtk.VTK_TETRA
  meshTETRA = pv.UnstructuredGrid(cells, celltypes, points)
  meshTETRAPoly=pv.PolyData(points,faces)

  meshTETRA["nodeNb"]=nodeNb
  meshTETRA["eleNb"]=eleNb
  meshTETRA["element_attributes"]=npz["element_attributes"]
  meshTETRA["mtr"]=npz["mtr"]
  meshTETRAPoly["faceNb"]=faceNb
  meshTETRAPoly["face_markers"]=npz["face_markers"]
  meshTETRAPoly["mtr"]=npz["mtr"]
  
  return meshTETRA,meshTETRAPoly

def createPvUSG_TETRA(mesh):
  points=np.array(mesh.points)
  elements=np.array(mesh.elements)
  meshfaces=np.array(mesh.faces)
  faces=np.full((len(meshfaces),4),fill_value=3,dtype=np.int32)
  faces[:,1:]=meshfaces[:,:]
  faces=np.hstack(faces)
  # print(faces)
  # for i in range(0,5):
  #   print(faces[i])
  #   print(meshfaces[i])
  # quit()

  cells=np.full((len(elements),5),4,dtype=np.int64)             
  # cells=[]
  # for i in range(0,len(elements)):
  #   cells.append([4,elements[i][0],elements[i][1],elements[i][2],elements[i][3]])
  cells[:,1:5]=elements[:,0:4]
  
    
  no_cells=len(cells)
  cells=np.array(cells).ravel()                
  celltypes = np.empty(no_cells, dtype=np.uint32)
  celltypes[:] = vtk.VTK_TETRA
  meshTETRA = pv.UnstructuredGrid(cells, celltypes, points)
  meshTETRAPoly=pv.PolyData(points,faces)
  return meshTETRA,meshTETRAPoly

def createPvUSG_TRIANGLE(mesh):
  points=np.array(mesh.points)
  faces=np.array(mesh.faces)
                     
  cells=np.full((len(faces),4),3,dtype=np.int32)
  cells[:,1:4]=faces[:,0:3]
    
  no_cells=len(cells)
  cells=np.array(cells).ravel()                
  celltypes = np.empty(no_cells, dtype=np.uint32)
  celltypes[:] = vtk.VTK_TRIANGLE
  meshTRIANGLE = pv.UnstructuredGrid(cells, celltypes, points)
  return meshTRIANGLE

def runTetgen_refines(smallDomain,edzRegionTag,meshUSG_coarsening,meshTetraCoarsening):
  '''
  # 測試產生refinement網格
  np.savez('meshUSG_coarsening', 
  points=np.array(mesh.points), 
  elements=np.array(mesh.elements), element_attributes=np.array(mesh.element_attributes),
  faces=np.array(mesh.faces),
  face_markers=np.array(mesh.face_markers),
  mtr=np.array(mesh.point_metric_tensors))
  print("save meshUSG_coarsening OK") 
  '''
  points=tuple(map(tuple, meshUSG_coarsening['points']))
  elements=meshUSG_coarsening['elements']
  faces=meshUSG_coarsening['faces'].tolist()
  face_markers=meshUSG_coarsening['face_markers'].tolist()
  mtr=meshUSG_coarsening['mtr']
  tetgenF='test'
  with open(tetgenF+'.mtr','w',encoding = 'utf-8') as f:
    f.write(str(len(mtr))+' 1\n')
    for i in range(0,len(mtr)):
      f.write(str(mtr[i])+'\n')
    f.close()

  '''
  runTetgenCheck=0
  while runTetgenCheck==0:
    print("renew mesh_info")
    mesh_info = MeshInfo()
    mesh_info.set_points(points)
    mesh_info.save_nodes(tetgenF)
    mesh_info.load_mtr(tetgenF)
    mesh_info.set_facets(faces,face_markers)  
    mesh_info.regions.resize(int(3))
    mesh_info.regions[0] = [900, 1800, -25,-1,1e+5,]
    mesh_info.regions[1] = [900, 1800, -100,-2,1e+5,]
    mesh_info.regions[2] = [800, 1700, -300,-3,1e+5,]  
    mesh_info.save_poly(tetgenF)
    path_to_file=tetgenF+'Out'
    file_exists = os.path.exists(path_to_file)    
    if file_exists:
      os.remove(path_to_file)
    print('tetgen -dT1.0e-6 '+tetgenF+'.poly >'+path_to_file)
    os.system('tetgen -dT1.0e-6 '+tetgenF+'.poly >'+path_to_file)
    print("path_to_file=",path_to_file)
    file_exists = os.path.exists(path_to_file)
    print("file_exists=",file_exists)  
    if file_exists:
      f = open(path_to_file)
      delNb=[]
      for line in f.readlines():      
        if 'No faces are intersecting.' in line :
          runTetgenCheck=1
          print("Self-intersection OK")
        if (' intersects facet #' in line) or (' duplicates facet #' in line):
          linelist =[int(s) for s in re.findall(r'-?\d+\.?\d*', line)][1]-1
          nb=(linelist)
          if (nb not in delNb):
            delNb.append(nb)
      f.close()           
      print("delNb=",delNb)
      if (len(delNb)>0):
        delNb=np.unique(delNb)
        for i in range(len(delNb)-1,-1,-1):
          del faces[delNb[i]] 
          del face_markers[delNb[i]] 
        print("len(cells) AF",len(faces))
      else:
        mesh_info.save_poly(tetgenF)
        runTetgenCheck=1
        if file_exists:
          os.remove(path_to_file)
  print("tetgen start")      
  '''  

  # edzBool = np.argwhere(meshTetraCoarsening["element_attributes"] !=-edzRegionTag)
  # ezdmeshByTag=meshTetraCoarsening.remove_cells(edzBool,inplace=False)
  # ezdmeshByTagCenter=ezdmeshByTag.cell_centers()


  # print(faces)
  # print(face_markers)
  mesh_info = MeshInfo()
  mesh_info.set_points(points)
  mesh_info.set_facets(faces,face_markers)
  mesh_info.regions.resize(int(3))
  mesh_info.regions[0] = [900, 1800, -25,-1,1e+5,]
  mesh_info.regions[1] = [900, 1800, -100,-2,1e+5,]
  mesh_info.regions[2] = [800, 1700, -300,-3,1e+5,]  
  mesh_info.load_mtr(tetgenF) 
  # switches='pqAmnf'  
  switches='pqAmnfMT1e-16'
  print('refinement switches=',switches)
  mesh = build(mesh_info, options=Options(switches=switches))
  # print("len(mesh.points)=",len(mesh.points))
  # print("len(mesh.elements)=",len(mesh.elements))

  
  # meshUSG_refinement_Tetra,meshUSG_refinement_TetraFace=createPvUSG_TETRA(mesh)
  # meshUSG_refinement_Tetra.save('meshUSG_refinement.vtk')
  np.savez('meshUSG_refinement', 
  points=np.array(mesh.points), 
  elements=np.array(mesh.elements), 
  element_attributes=np.array(mesh.element_attributes),
  faces=np.array(mesh.faces),
  face_markers=np.array(mesh.face_markers),
  mtr=np.array(mesh.point_metric_tensors))
  print("save meshUSG_refinement OK")  
  
  '''
  with open('mergeRefinementFaces.dat','w',encoding = 'utf-8') as f:
    f.write('Title="XY2D_plot"\n')
    f.write('Variables="x(m)","y(m)","z(m)","label"\n')
    f.write('Zone N='+str(len(mesh.points))+',E='+str(len(mesh.faces))+',F=feblock,et=triangle\n')
    f.write('varlocation=([4]=cellcentered)\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][0])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][1])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][2])+'\n')
    for i in range(0,len(mesh.faces)):
      f.write(str(mesh.face_markers[i])+'\n')
    for i in range(0,len(mesh.faces)):
      f.write(str(mesh.faces[i][0]+1)+' '+str(mesh.faces[i][1]+1)+' '+str(mesh.faces[i][2]+1)+'\n')
    f.close()
  '''
  # '''
  with open('mergeRefinementElements.dat','w',encoding = 'utf-8') as f:
    f.write('Title="XY2D_plot"\n')
    f.write('Variables="x(m)","y(m)","z(m)","mtr","label"\n')
    f.write('Zone N='+str(len(mesh.points))+',E='+str(len(mesh.elements))+',F=feblock,et=tetrahedron\n')
    f.write('varlocation=([5]=cellcentered)\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][0])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][1])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][2])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.point_metric_tensors[i])+'\n')
    for i in range(0,len(mesh.elements)):
      f.write(str(mesh.element_attributes[i])+'\n')
    for i in range(0,len(mesh.elements)):
      f.write(str(mesh.elements[i][0]+1)+' '+str(mesh.elements[i][1]+1)+' '+str(mesh.elements[i][2]+1)+' '+str(mesh.elements[i][3]+1)+'\n')
    f.close()
  # '''  
   
  
def createMeshRefines(meshUSG_coarsening,meshUSG_coarseningVTK,edz,regionTag,addRegion):
# def createMeshRefines(OR_mesh_info,meshUSG_coarsening,meshUSG_coarseningVTK,edz,regionTag,addRegion):
  # print('len(OR_mesh_info.points)=',len(OR_mesh_info.points))
  # print('len(OR_mesh_info.faces)=',len(OR_mesh_info.faces))
  # print("len(meshUSG_coarsening['ini_cells'])=",len(meshUSG_coarsening['ini_cells']))
  # print("len(meshUSG_coarsening['ini_mtr'])=",len(meshUSG_coarsening['ini_mtr']))

  co=meshUSG_coarseningVTK.cell_centers()
  meshUSG_coarsening_clippedEdz=co.clip_surface(edz)
  meshUSG_coarsening_clippedEdz.save('meshUSG_coarsening_clippedEdz.vtk')
  bcCener=co.points[(meshUSG_coarseningVTK.find_closest_cell(np.max(meshUSG_coarseningVTK.points,axis=0)))]
  # print("bcCener=",bcCener)

  points=meshUSG_coarsening['points']
  elements=meshUSG_coarsening['elements']
  faces=meshUSG_coarsening['faces']
  face_markers=meshUSG_coarsening['face_markers']
  ini_points=tuple(map(tuple, meshUSG_coarsening['ini_points']))   
  ini_cells=meshUSG_coarsening['ini_cells']
  ini_eachNoFace=meshUSG_coarsening['ini_eachNoFace']
  ini_cellsMark=meshUSG_coarsening['ini_cellsMark'].tolist()
  ini_mtr=meshUSG_coarsening['ini_mtr'].tolist()

  # print(ini_points[0:3])
  # print(ini_cells[0:10])
  # print(ini_cellsMark[0:3])
  # print(ini_mtr[0:3])
  tetgenF='merge'
  with open(tetgenF+'.mtr','w',encoding = 'utf-8') as f:
    f.write(str(len(ini_mtr))+' 1\n')
    for i in range(0,len(ini_mtr)):
      f.write(str(ini_mtr[i])+'\n')
    f.close()
  
  '''
  # set mesh regions
  '''
  # print("edz no_cells=",meshUSG_clipped.n_points)
  
  cells=get_cellbyEachNo(ini_cells,ini_eachNoFace)
  mesh_info = MeshInfo()
  # mesh_info.load_node(tetgenF)
  mesh_info.set_points(ini_points)
  mesh_info.set_facets(cells,ini_cellsMark)
  # mesh_info.load_poly(tetgenF)
  mesh_info.load_mtr(tetgenF)

  # regionTag,addRegion
  mesh_info.regions.resize(int(meshUSG_coarsening_clippedEdz.n_points+1))
  mesh_info.regions[0]=[bcCener[0],bcCener[1],bcCener[2],regionTag[0],1e+5]
  for i in range(0,meshUSG_coarsening_clippedEdz.n_points):
    mesh_info.regions[i+1] = [meshUSG_coarsening_clippedEdz.points[i][0],meshUSG_coarsening_clippedEdz.points[i][1],meshUSG_coarsening_clippedEdz.points[i][2],regionTag[1],1e+5,]
  print("set regions OK")

  
  mesh_info.save_poly(tetgenF)
  # quit()
  # mesh = build(mesh_info, options=Options(switches='pqmAO3o/150'))
  mesh = build(mesh_info, options=Options(switches='pqAmnf'))
  # mesh = build(mesh_info, options=Options(switches='pAa'))
  # mesh = build(mesh_info, options=Options(switches='pA'))
  mesh.save_neighbors(tetgenF)
  print("refine mesh OK")
  # quit()  ### refine mesh quitPoint
  
  print("len(mesh.points)=",len(mesh.points))
  print("len(mesh.elements)=",len(mesh.elements))
  with open('mergeRefinesTETRA.dat','w',encoding = 'utf-8') as f:
    f.write('Title="XY2D_plot"\n')
    f.write('Variables="x(m)","y(m)","z(m)","label3D"\n')
    f.write('Zone N='+str(len(mesh.points))+',E='+str(len(mesh.elements))+',F=feblock,et=tetrahedron\n')
    f.write('varlocation=([4]=cellcentered)\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][0])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][1])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][2])+'\n')
    for i in range(0,len(mesh.elements)):
      f.write(str(mesh.element_attributes[i])+'\n')
    for i in range(0,len(mesh.elements)):
      f.write(str(mesh.elements[i][0]+1)+' '+str(mesh.elements[i][1]+1)+' '+str(mesh.elements[i][2]+1)+' '+str(mesh.elements[i][3]+1)+'\n')
    f.close()
  with open('mergeRefinesTriangle.dat','w',encoding = 'utf-8') as f:
    f.write('Title="XY2D_plot"\n')
    f.write('Variables="x(m)","y(m)","z(m)","label2D"\n')
    f.write('Zone N='+str(len(mesh.points))+',E='+str(len(mesh.faces))+',F=feblock,et=triangle\n')
    f.write('varlocation=([4]=cellcentered)\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][0])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][1])+'\n')
    for i in range(0,len(mesh.points)):
      f.write(str(mesh.points[i][2])+'\n')
    for i in range(0,len(mesh.faces)):
      f.write(str(mesh.face_markers[i])+'\n')
    for i in range(0,len(mesh.faces)):
      f.write(str(mesh.faces[i][0]+1)+' '+str(mesh.faces[i][1]+1)+' '+str(mesh.faces[i][2]+1)+'\n')
    f.close()
  return mesh

def get_cellbyEachNo(cells,eachNoFace):
  faces=[]
  loc=0
  for i in range(0,len(eachNoFace)):
    faces.append(cells[loc:loc+eachNoFace[i]].tolist())
    loc=loc+eachNoFace[i]
  # print("faces=",faces)
  return faces