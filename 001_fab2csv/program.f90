program fab2csv
integer*4 i,j,k,Reason
character(256) word,fabFile
integer*4 no_face
integer*4 fracNB,no_node,temp
real*8 Transmissivity,Storativity,Aperture

open(20,file='fab.sp')
read(20,"(A256)") fabFile
close(20)

open(30,file='dfnData.csv')
write(30,"(A)") 'no_node,Transmissivity,Storativity,aperture'


!!!!!!!!!!!!!!!!!!!!!!!!!
open(10,file=fabFile)
do while(.true.)
  read(10,"(A256)",IOSTAT=Reason) word
  if (Reason==-1) goto 101
  if (word(1:12)=='BEGIN FORMAT') then
    do while(.true.)
      read(10,"(A256)",IOSTAT=Reason) word
      if (Reason==-1) goto 101
      if (word(1:18)=='    No_Fractures =') then
        ! write(*,"(A)") trim(word(19:256))
        read(word(19:256),*) no_face
      end if
    end do
  end if
end do
101 close(10)
write(*,"(A,I0)") 'no_face=',no_face
!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!
open(10,file=fabFile)
do while(.true.)
  read(10,"(A256)",IOSTAT=Reason) word
  if (Reason==-1) goto 102
  if (word(1:14)=='BEGIN FRACTURE') then
    do while(.true.)
      read(10,"(A256)",IOSTAT=Reason) word
      if (Reason==-1) goto 102
      if (word(1:12)=='END FRACTURE') goto 102
      read(word(1:256),*) fracNB,no_node,temp,Transmissivity,Storativity,Aperture
      ! write(*,"(A,I0)") 'fracNB=',fracNB
      write(30,"(I0,A,1x,G0,A,1x,G0,A,1x,G0)") no_node,',',Transmissivity,',',Storativity,',',Aperture
      do i=1,no_node
        read(10,"(A256)") word
        write(30,"(A)") trim(word)
      end do
      read(10,"(A256)") word
    end do
  end if
end do
102 close(10)
close(30)
!!!!!!!!!!!!!!!!!!!!!!!!!
end program fab2csv