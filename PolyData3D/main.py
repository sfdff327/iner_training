import numpy as np
import pandas as pd
import time
import dem
import pyvista as pv
import plotPyvista

if __name__ == "__main__":
  start = time.time()
  
  OriginPointsF=r'../../INER_DATA/Domain2D/OriginPoints.txt'
  path=r'../../HybridTW/000_fabToMesh/testWriteMeshALL.poly'
  q123Folder=r'../../HybridTW/001_stlToMesh'
  FABResfolder=r'../../HybridTW/000_fabToMesh/FABRes'

  EpsgIn=3825
  EpsgOut=4326

  '''
  # 模擬底部高程
  '''
  STLminZ=-2000.0

  '''
  # 表土層深度
  '''
  depthSurfaceSoil=-70.0  

  # OriginPointsF=r'/root/ihsienlee/CGAL/CGAL_LEE/INER_DATA/Domain2D/OriginPoints.txt'
  orPointCsv = pd.read_csv(OriginPointsF,sep='\\s+')
  orPointCsv = orPointCsv.fillna('')
  orPointCsv.columns=["x", "y"]
  delta_x=np.float64(orPointCsv['x'].values[0])
  delta_y=np.float64(orPointCsv['y'].values[0])
  # print(orPointCsv)
  print("delta_x,delta_y=",delta_x,delta_y)
  # quit()  

  CsvFile=r'../../INER_DATA/Domain2D/DEM_120m_resolution500m_201x_139y.dat'
  domainFile=r'../../INER_DATA/Domain2D/domainboundary_large.xy'
  nx=201
  ny=139
  # CsvFile=r'../../INER_DATA/Domain2D/DTM_largescale_263x_263y.dat'
  # nx=263
  # ny=263  

  case=1
  if (case==1):
    '''
    # 讀取500m DEM
    '''    
    xyzCsv = pd.read_csv(CsvFile,sep='\\s+',header=None)
    xyzCsv = xyzCsv.fillna('')
    xyzCsv.columns=["x", "y", "z"]   
    # print(xyzCsv) 
    # quit()
    '''csv points to vtk'''
    points=np.zeros((len(xyzCsv),3),dtype=np.float64)
    points[:,0]=xyzCsv['x'].values[:]
    points[:,1]=xyzCsv['y'].values[:]
    xyzPoints = pv.PolyData(points)
    xyzPoints.save("xyzPoints.vtk")


    ''' # 讀取模擬範圍,並且依據dem計算表面高程 '''    
    xyzDomain = pd.read_csv(domainFile,sep='\\s+',header=None)
    xyzDomain = xyzDomain.fillna('')
    xyzDomain.columns=["x", "y"]
    

    domain2d,demClip2d,largeDem= dem.demToMesh(nx,ny,xyzCsv,xyzDomain)
    print("read dem and doamin files")
    domain2d,demClip2d,largeDem=plotPyvista.dem2dClip(delta_x,delta_y,EpsgIn,EpsgOut,domain2d,demClip2d,largeDem)
  # quit()
  domain2d=pv.read('domain2d.vtk')
  demClip2d=pv.read('demClip2d.vtk')
  largeDem=pv.read('largeDem.vtk')  

  case=1
  if (case==1):   
    STLminZ=np.float64(STLminZ)
    '''
    use demClip2d create 3d dem
    '''
    demClip3dsurf=plotPyvista.dem2dTo3d(demClip2d)
    StlFile=r'../../INER_DATA/STL/GeoModel_SKB_AModify_fault_1.stl'
    fault_1=pv.read(StlFile)
    StlFile=r'../../INER_DATA/STL/GeoModel_SKB_AModify_fracture.stl'
    fault_2=pv.read(StlFile)
    fault_1Mtr=100.0
    fault_2Mtr=20.0
    fault12=plotPyvista.faultStl(demClip2d,delta_x,delta_y,fault_1,fault_2,fault_1Mtr,fault_2Mtr)
    boxMtr=500.0
    poly3d=plotPyvista.mergeOutsideBox(depthSurfaceSoil,demClip3dsurf,STLminZ,boxMtr)    
  else:
    demClip3dsurf=pv.read('demClip3dsurf.vtk')
    poly3d=pv.read('poly3d.vtk')
    fault12=pv.read('fault12.vtk')
  # print(fault12.array_names)
  # print(poly3d.array_names)
  # print('poly3d, demclip3d and fault12')
  quit()