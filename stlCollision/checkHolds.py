import pyvista as pv
import numpy as np
import vtk
import os
import pandas as pd
from numpy.core.numeric import NaN
from pathlib import Path
from mesh import Mesh

def EulerFormula(strname1,strname2,dhNB,fracNB,single_DH,single_FRAC):
  col, n_contacts = single_FRAC.collision(single_DH,contact_mode=2)
  # print("n_contacts=",n_contacts)
  holeInd=np.int32(-1)
  if (n_contacts>0):
    print("i,j=",dhNB,fracNB)
    file = Path("single_DH.stl")
    if file.is_file():
      os.system('rm single_DH.stl')
    file = Path("single_FRAC.stl")
    if file.is_file():
      os.system('rm single_FRAC.stl')
    file = Path("output.obj")
    if file.is_file():
      os.system('rm output.obj')
    file = Path("out_labels.txt")
    if file.is_file():
      os.system('rm out_labels.txt')

    single_DH.save('single_DH.stl')
    single_FRAC.save('single_FRAC.stl')
    my_mesh=Mesh(["single_DH.stl","single_FRAC.stl"])
    my_mesh.solveIntersections()
    
    out_coords=my_mesh.get_out_coords().reshape(-1,3)
    out_tris=my_mesh.get_out_tris().reshape(-1,3)
    out_labels=np.int64(my_mesh.get_out_labels_bit())
    ele_ind_new=np.full((len(out_tris),4),3,dtype=np.int64)
    ele_ind_new[:,1:4]=out_tris[:,0:3]
    ele_ind_new=np.hstack(ele_ind_new)
    obj=pv.PolyData(out_coords,ele_ind_new)
    # print(obj)
    # quit()

    # command = './mesh_arrangement single_DH.stl single_FRAC.stl'
    # os.system(command)
    # obj=pv.read("output.obj")
            
    # file1 = open('out_labels.txt', 'r')
    # df_t = pd.read_csv(file1,header=None)

    # onjscalars = np.array(df_t.values.tolist())
    obj['celltype']=out_labels
    objFrac=obj.extract_cells(obj["celltype"] == 10)
    objFrac=objFrac.extract_surface()

    center=objFrac.cell_centers()
    selected = center.select_enclosed_points(single_DH,check_surface=False)
    # print(selected['SelectedPoints'])
    outPts=objFrac.extract_cells(selected["SelectedPoints"] == 0)
    inPts=objFrac.extract_cells(selected["SelectedPoints"] == 1)
    # pts = center.extract_points(selected['SelectedPoints'].view(bool),adjacent_cells=False)
    V=outPts.n_points
    F=outPts.n_cells
    E=outPts.extract_all_edges().n_cells
    # print("clipFrac.n_points=",V) 
    # print("clipFrac.n_faces=",F)
    # print("clipFrac.n_edges=",E)
    # print("V - E + F=",V - E + F)
    if ((V - E + F)==1):
      holeInd=np.int32(0)
    else:
      holeInd=np.int32(1)  

    '''
    # holeInd==0 代表部份貫穿
    # holeInd==1 代表完全貫穿
    未來計算最大速度差值，僅需要找 inPts.points
    '''

    binfile=strname1+str(dhNB)+strname2+str(fracNB)+'.vtk'
    inPts.save(binfile)
    binfile='out'+strname1+str(dhNB)+strname2+str(fracNB)+'.vtk'
    outPts.save(binfile)
    binfile='OBJ_'+strname1+str(dhNB)+strname2+str(fracNB)+'.vtk'
    obj.save(binfile)
    # print("holeInd=",holeInd)
    # quit()
  return holeInd
