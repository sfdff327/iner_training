import readSTL
import pyvista as pv
import numpy as np
from pathlib import Path
import pickle
import pandas as pd

if __name__ == "__main__":

  OriginPointsF=r'../../INER_DATA/Domain2D/OriginPoints.txt'
  orPointCsv = pd.read_csv(OriginPointsF,sep='\\s+')
  orPointCsv = orPointCsv.fillna('')
  orPointCsv.columns=["x", "y"]
  delta_x=np.float64(orPointCsv['x'].values[0])
  delta_y=np.float64(orPointCsv['y'].values[0])
  # print(orPointCsv)
  print("delta_x,delta_y=",delta_x,delta_y)
  # quit()

  case=0
  if (case==1):
    ''' #讀取DFN參數與座標'''
    dfndataF=r'dfnData.csv'
    dfndata = pd.read_csv(dfndataF,sep='\\s+')
    # print(dfndata)
    
    no_node=dfndata['no_node']
    Transmissivity=dfndata['Transmissivity']
    Storativity=dfndata['Storativity']
    aperture=dfndata['aperture']
    
    ''' # 計算K=T/b'''
    fractureK=Transmissivity/aperture

    ''' #fracNb=裂隙編號; 初始化'''
    fracNb=-1

    ''' #lineNb=dfnData.csv檔案行數編號; 初始化'''
    lineNb=0

    ''' # len_dfndata=dfnData.csv檔案[總]行數'''
    len_dfndata=len(no_node)

    ''' # 創立空的vtk'''
    dfnmesh = pv.PolyData()
  
    ''' # 當lineNb小於len_dfndata,代表並未讀完dfnData.csv'''
    while(lineNb<len_dfndata):
      ''' # 裂隙編號+1'''    
      fracNb=fracNb+1

      ''' # 讀取裂隙節點個數 = no_each_frac_node'''
      no_each_frac_node=np.int64(no_node[lineNb])

      ''' # 計算單一個別裂隙K'''
      fractureK=np.float64(Transmissivity[lineNb])/np.float64(aperture[lineNb])
      fractureAperture=np.float64(aperture[lineNb])

      ''' # 組裝單一裂隙的PolyData'''
      xyz=np.zeros((no_each_frac_node,3),dtype=np.float64)
      faces=np.linspace(start=-1,stop=no_each_frac_node,
                        num=no_each_frac_node+1,
                        endpoint=False,
                        dtype=np.int64)
      faces[0]=no_each_frac_node
      xyz[:,0]=np.float64(Transmissivity[lineNb+1:lineNb+1+no_each_frac_node])
      xyz[:,1]=np.float64(Storativity[lineNb+1:lineNb+1+no_each_frac_node])
      xyz[:,2]=np.float64(aperture[lineNb+1:lineNb+1+no_each_frac_node])

      ''' 計算下一個裂隙組在dfnData.csv的行數'''
      lineNb=lineNb+1+no_each_frac_node

      ''' # 創立單一裂隙為PolyData'''
      dfnpoly=pv.PolyData(xyz,faces)

      ''' # 移動座標'''
      dfnpoly.translate([delta_x,delta_y,0],inplace=True)

      ''' 將dfnData.csv的水文參數加入PolyData'''
      dfnpoly['fractureK']=[fractureK]
      dfnpoly['aperture']=[fractureAperture]
      dfnpoly['RegionId']=[fracNb]

      ''' 累加裂隙PolyData'''
      dfnmesh=dfnmesh+dfnpoly

    dfnmesh.save('dfnPoly.vtk') 
    dfnPoly=dfnmesh
    # print('fracNb=',fracNb)
  elif (case==0):
    dfnPoly=pv.read('dfnPoly.vtk')
  # dfnPoly=dfnPoly.triangulate() 
  # print("dfnPoly",dfnPoly)
  # print("dfnPoly.array_names=",dfnPoly.array_names)
  # print("noTestFrac=",dfnPoly.n_cells)
  # print((dfnPoly['RegionId']))
  # print(np.max(dfnPoly['RegionId']))
  # print(np.min(dfnPoly['RegionId']))

  
  '''
  # 利用np.unique判斷具有多少個不連通MT群組
  # np.unique: 列出不重複元素,
  # 在 Python 中我們可以使用 numpy 模組的 unique 函數列出所有不重複的元素
  '''
  noConnfrac = len(np.unique(dfnPoly['RegionId']))
  print('noConnfrac=',noConnfrac)

  case=1
  if (case==1):
    '''
    # 設定MT STL檔案位置
    # 由於個人置放INER_DATA資料夾位置不一定相同,需要確認後才可以執行
    '''
    mtFile=r'../../INER_DATA/SNFD2021Reposiroty/MT_o.stl'

    '''
    # 讀取MT STL檔案
    # 利用paraview展示MT_o.stl,確認stl的參數
    '''
    mtSTL=pv.read(mtFile)

    '''
    # 判斷MT所有元素連通性
    # 執行完成後,將mt儲存為vtk,再與STL比較
    '''
    mt = mtSTL.connectivity(largest=False)

    '''
    # 利用np.unique判斷具有多少個不連通MT群組
    # np.unique: 列出不重複元素,
    # 在 Python 中我們可以使用 numpy 模組的 unique 函數列出所有不重複的元素
    '''
    noConnDH=len(np.unique(mt['RegionId']))
    
    '''# 設定MT的z值作為參數'''
    mt['z']=mt.points[:,2:3]

    '''# 儲存vtk檔案'''
    mt.save('mt.vtk')        

    
  case=1
  if (case==1):
    '''
    # Q1
    '''
    print("Run Q1")
    dhFile=r'../../INER_DATA/SNFD2021Reposiroty/DH_o.stl'
    dhSTL=pv.read(dhFile)
    dh = dhSTL.connectivity(largest=False)
    strname1=r'dh'
    strname2=r'frac'
    noConnDH=len(np.unique(dh['RegionId']))
    dh['z']=dh.points[:,2:3]
    dh.save('dh.vtk')
    folder=r"Q1vtk"

    '''
    # 計算Q1路徑的截切關係
    '''
    readSTL.comm(strname1,strname2,dh,dfnPoly,"Q1vtk") 

    dhfrac = np.fromfile(
      folder+'/'+strname1+'_'+strname2+'.bin', 
      dtype=np.int32).reshape(noConnDH, noConnfrac)
    print(dhfrac)
    print(dhfrac.shape)
    print("Finish Q1")
  else:
    folder=r"Q1vtk"

  dh=pv.read(folder+'/'+'dh.vtk')
  noConnDH=len(np.unique(dh['RegionId']))
  # quit()

  case=1
  if (case==1):
    '''
    # Q2
    '''
    print("Run Q2")
    folder=r"Q2vtk"
    edzFile=r'../../INER_DATA/SNFD2021Reposiroty/EDZ_vol_o.stl'
    edzSTL=pv.read(edzFile)
    edz = edzSTL.connectivity(largest=False)

    
    noConnedz=len(np.unique(edz['RegionId']))
    # print("noConnDH,noConnedz=",noConnDH,noConnedz)
    # holeInd=np.zeros((noConnDH,noConnFRAC),dtype=np.int32)

    strname1=r'dh'
    strname2=r'edz'
    readSTL.comm(strname1,strname2,dh,edz,"Q2vtk")
    # dhedz = np.fromfile('/data/CGAL/CGAL_LEE/HybridTW/001_stlToMesh/Q2vtk/dh_edz.bin', dtype=np.int32).reshape(noConnDH, noConnedz)
    dhedz = np.fromfile(folder+'/'+strname1+'_'+strname2+'.bin', dtype=np.int32).reshape(noConnDH, noConnedz)
    edz['z']=edz.points[:,2:3]
    edz.save('edz.vtk')
    print(dhedz)
    print(dhedz.shape)
    print("Finish Q2")

  '''
  # Q3
  '''
  folder=r"Q3vtk"
  dtFile=r'../../INER_DATA/SNFD2021Reposiroty/DT_o.stl'
  dtSTL=pv.read(dtFile)
  dt = dtSTL.connectivity(largest=False)
  noConnDt=len(np.unique(dt['RegionId']))    
  strname1=r'dt'
  strname2=r'frac'
  readSTL.comm(strname1,strname2,dt,dfnPoly,"Q3vtk") 
  dt['z']=dt.points[:,2:3]
  dt.save('dt.vtk')
  dtfrac = np.fromfile(folder+'/'+strname1+'_'+strname2+'.bin', dtype=np.int32).reshape(noConnDt, noConnfrac)
  print(dtfrac)
  print(dtfrac.shape)
  print("Finish Q3")


  print("=====")