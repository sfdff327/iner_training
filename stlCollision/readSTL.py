import pyvista as pv
import numpy as np
import vtk
import os,shutil
from pathlib import Path
import pandas as pd
from numpy.core.numeric import NaN

import checkHolds

'''
# 確認是否有holes # V - E + F = 2 for 3d volue
# V - E + F = 1 for 2d
'''
def comm(strname1,strname2,connDH,connFRAC,vtkFolder):
  noConnDH=len(np.unique(connDH['RegionId']))
  noConnFRAC=len(np.unique(connFRAC['RegionId']))
  holeInd=np.zeros((noConnDH,noConnFRAC),dtype=np.int32)
  print("noConnDH,noConnFRAC=",noConnDH,noConnFRAC)
  # quit()

  # for i in range (0,noConnDH): #np.unique(connDH['RegionId']):
  for i in range (5,6): #np.unique(connDH['RegionId']):
  # for i in range (0,1): #np.unique(connDH['RegionId']):
    # print("i=",i)
    single_DH=connDH.extract_cells(connDH["RegionId"] == i)
    single_DH=single_DH.extract_surface()
    single_DH=single_DH.triangulate()
    single_DH.save('single_DH.vtk')
    # for j in range (0,noConnFRAC): #np.unique(connFRAC['RegionId']):
    for j in range (4407,4408): #np.unique(connFRAC['RegionId']):
    # for j in range (51,52): #np.unique(connFRAC['RegionId']):
      # print("i,j=",i,j)
      single_FRAC=connFRAC.extract_cells(connFRAC["RegionId"] == j)
      single_FRAC=single_FRAC.extract_surface()
      single_FRAC=single_FRAC.triangulate()
      single_FRAC.save('single_Frac.vtk')
      holeInd[i][j]=checkHolds.EulerFormula(strname1,strname2,i,j,single_DH,single_FRAC)

  '''
  不可隨便執行下列程式，否則會刪除截切資料
  '''
  binfile=strname1+'_'+strname2+'.bin'
  holeInd.tofile(binfile)
  if not os.path.exists(vtkFolder):
    os.mkdir(vtkFolder)                
  for move_file in Path('./').glob(strname1+'*.vtk'):
    shutil.move(move_file, os.path.join(vtkFolder, move_file))   
  for move_file in Path('./').glob(binfile):
    shutil.move(move_file, os.path.join(vtkFolder, move_file))   
  # quit()          
            
