import io
import numpy as np
import os
import pyvista as pv
import readGeoJSON
from pathlib import Path
import pickle
from scipy.io import FortranFile
import pyToTecplot
import vector
import math
import copy
import pandas as pd
import dem
import vtk

def extract_node(mesh,key,node):
  idx = mesh[key] == node
  return idx
def Circumcenter_new(x1,y1,z1,x2,y2,z2,x3,y3,z3):
    print(x1,y1,z1)
    print(x2,y2,z2)
    print(x3,y3,z3)
    x12=x1-x2
    x21=x2-x1
    x13=x1-x3
    x31=x3-x1
    x23=x2-x3
    x32=x3-x2
    y12=y1-y2
    y21=y2-y1
    y13=y1-y3
    y31=y3-y1
    y23=y2-y3
    y32=y3-y2
    z12=z1-z2
    z21=z2-z1
    z13=z1-z3
    z31=z3-z1
    z23=z2-z3
    z32=z3-z2
    a=np.float64(0.0)
    b=np.float64(0.0)
    c=np.float64(0.0)

    crx,cry,crz=cross_V_new(x12,y12,z12,x23,y23,z23)  
    dis=(crx*crx+cry*cry+crz*crz)**0.5
    print(dis)
    dis=np.float64(2.0)*dis*dis
    print(dis)
    a=(x23*x23+y23*y23+z23*z23)*(x12*x13+y12*y13+z12*z13)/dis
    b=(x13*x13+y13*y13+z13*z13)*(x21*x23+y21*y23+z21*z23)/dis
    c=(x12*x12+y12*y12+z12*z12)*(x31*x32+y31*y32+z31*z32)/dis

    cx=a*x1+b*x2+c*x3
    cy=a*y1+b*y2+c*y3
    cz=a*z1+b*z2+c*z3

    return cx,cy,cz

def cross_V_new(x1,y1,z1,x2,y2,z2):
    x3=y1*z2-z1*y2
    y3=-x1*z2+z1*x2
    z3=x1*y2-y1*x2
    return x3,y3,z3

def pnt2line(A, B, C):
    d = (C - B) / np.sqrt(np.sum(np.power((C - B), 2.0)))
    v = A - B
    t = np.dot(v,d)
    P = B + t * d
    return np.sqrt(np.sum(np.power((P - A), 2.0))),P
    # line_vec = vector.vector(start, end)
    # pnt_vec = vector.vector(start, pnt)
    # line_len = vector.length(line_vec)
    # line_unitvec = vector.unit(line_vec)
    # pnt_vec_scaled = vector.scale(pnt_vec, 1.0/line_len)
    # t = vector.dot(line_unitvec, pnt_vec_scaled)    
    # if t < 0.0:
    #     t = 0.0
    # elif t > 1.0:
    #     t = 1.0
    # nearest = vector.scale(line_vec, t)
    # dist = vector.distance(nearest, pnt_vec)
    # nearest = vector.add(nearest, start)
    # return (dist, nearest)
def threeCoors2TwoD(p,c, oxyz,a, b,xDir,yDir):
    # p=inPartEdge.points[i]
    # print("inPartEdge.points[i]=",inPartEdge.points[i])
    '''new x'''
    xdist,nearest=pnt2line(p, c, oxyz)
    # print("i,xdist",i,xdist)
    # print("nearest=",nearest)
    # print("pnx=",)
    if(np.dot(xDir,nearest-p)<0.0):
        xdist=-xdist
    '''new y'''
    ydist,nearest=pnt2line(p, a, b)
    # print("i,ydist",i,ydist)
    # print("nearest=",nearest)
    # print("pny=",nearest-p)    
    if(np.dot(yDir,nearest-p)<0.0):
        ydist=-ydist
    xy=np.array([xdist,ydist,0.0])
    return xy


def DfnEqFlux(outfile_name,inPart,aperture,poro_fracture):

    center=inPart.center
    # print(center)
    size=inPart.compute_cell_sizes() 
    # print(size.array_names)
    # print(size['Area'])
    maxArea=np.max(size['Area'])
    maxAreaNB=np.where(size['Area']==maxArea)[0][0]


    inPart_xyz=inPart.points
    inPart_ele_ind=inPart.cells.reshape(-1,4)
    inPolyData=pv.PolyData(inPart_xyz,inPart_ele_ind)
    meanNormal=(np.mean(inPolyData.cell_normals,axis=0))

    '''
    起始參考點與直線方程
    '''
    a=np.array(inPart_xyz[inPart_ele_ind[maxAreaNB][1]])
    b=np.array(inPart_xyz[inPart_ele_ind[maxAreaNB][2]])
    c=np.array(inPart_xyz[inPart_ele_ind[maxAreaNB][3]])
    # print(a)
    # print(b)
    # print(c)
    dist,oxyz=pnt2line(c, a, b)
    # print("dist=",dist)
    # print("nearest=",oxyz)
    xDir=b-a
    yDir=np.array(oxyz-c,dtype=np.float64)

    '''
    # xDir與yDir 分別為3D完全截切體最大三角形的局部xy軸單位向量
    '''
    xDir=xDir/(np.sqrt(np.sum(np.power(xDir, 2.0))))
    yDir=yDir/(np.sqrt(np.sum(np.power(yDir, 2.0))))
    # print("ab=",xDir)
    # print("co=",yDir)


    center=inPart.center
    inPartEdge=inPart.extract_feature_edges(feature_angle=30, boundary_edges=True, non_manifold_edges=True, feature_edges=True, manifold_edges=True)

    '''    
    cxy=threeCoors2TwoD(inPartEdge.center,c, oxyz,a, b,xDir,yDir)
    print(inPartEdge.center)
    xy=np.zeros((len(inPartEdge.points),3),dtype=np.float64)
    for i in range(0,len(inPartEdge.points)):
        xy[i]=threeCoors2TwoD(inPartEdge.points[i],c, oxyz,a, b,xDir,yDir)
    print(xy)
    '''

    xyz=inPartEdge.points
    ele_ind=inPartEdge.lines.reshape(-1,3)
    vXYZ=inPartEdge['vectors']
    vol=inPartEdge['velocityLen']
    Tdis=inPartEdge.compute_cell_sizes() #計算每個線段長度
    # print(Tdis['Length'])

    # aperture=0.1
    Ur=np.zeros(len(ele_ind),dtype=np.float64)
    UrVector=np.zeros((len(ele_ind),3),dtype=np.float64)
    EqFlux=np.float64(0.0)
    for i in range(0,len(ele_ind)):
        # print("i=",i)
        e1=ele_ind[i][1]
        e2=ele_ind[i][2]
        a=xyz[e1]
        b=xyz[e2]
        p2Ldist,nearest=pnt2line(center, a, b)
        dir=nearest-center
        dir=dir/(np.sqrt(np.sum(np.power(dir, 2.0))))
        # print("dir Len",np.sqrt(np.sum(np.power(dir, 2.0))))
        # quit()

        # temp_maxV=np.float64(vol[e1]+vol[e2])/np.float64(2.0)
        temp_maxV_XYZ=[
            np.float64(vXYZ[e1][0]+vXYZ[e2][0])/np.float64(2.0),
            np.float64(vXYZ[e1][1]+vXYZ[e2][1])/np.float64(2.0),
            np.float64(vXYZ[e1][2]+vXYZ[e2][2])/np.float64(2.0)
        ]
        # tempVelLen=np.sum(np.power(temp_maxV_XYZ, 2.0))
        Ur[i]=np.dot(dir,temp_maxV_XYZ)/poro_fracture
        UrVector[i]=dir*Ur[i]
        # print("i,Ur=",i,Ur[i])
        EqFlux=EqFlux+abs(Ur[i]*aperture*Tdis['Length'][i])
        # print("Tdis=",Tdis['Length'][i])
        # print("i,EqFlux",i,Ur[i]*aperture*Tdis['Length'][i])

    # print(np.sum(Tdis['Length']))
    # print("aperture=",aperture)
    # print("EqFlux=",EqFlux)
    # inPartEdge=inPartEdge.cell_centers()
    inPartEdge['Ur']=Ur
    inPartEdge['UrVector']=UrVector
    inPartEdge.set_active_scalars('UrVector',preference='cell')
    # xyz=inPartEdge.points
    # ele_ind=inPartEdge.lines.reshape(-1,3)
    # with open(outfile_name,'w',encoding = 'utf-8') as f:
    #     f.write('Title="XY2D_plot"\n')
    #     f.write('Variables="x(m)","y(m)","z(m)","vx(m/s)","vy(m/s)","vz(m/s)"'+'\n')
    #     f.write('Zone N='+str(len(xyz))+',E='+str(len(ele_ind))+',F=FEBLOCK,ET=LINESEG\n') 
    #     f.write('varlocation=([1,2,3]=nodal,[4,5,6]=CELLCENTERED)\n')
    #     for i in range(0, len(xyz)):
    #         f.write(str(xyz[i][0])+'\n')
    #     for i in range(0, len(xyz)):
    #         f.write(str(xyz[i][1])+'\n')
    #     for i in range(0, len(xyz)):
    #         f.write(str(xyz[i][2])+'\n')
    #     for i in range(0, len(UrVector)):
    #         f.write(str(UrVector[i][0])+'\n')
    #     for i in range(0, len(UrVector)):
    #         f.write(str(UrVector[i][1])+'\n')
    #     for i in range(0, len(UrVector)):
    #         f.write(str(UrVector[i][2])+'\n')
    #     for i in range(0,len(ele_ind)):
    #         f.write(str(ele_ind[i][1]+1)+','+str(ele_ind[i][2]+1)+'\n')
    #     f.close()

    return inPartEdge,EqFlux

def cal_thrD_Determinant(v0,v1,v2,v3):
    v=+v0[0]*(v1[1]*v2[2]+v2[1]*v3[2]+v3[1]*v1[2]-v1[1]*v3[2]-v2[1]*v1[2]-v3[1]*v2[2])-v0[1]*(v1[0]*v2[2]+v2[0]*v3[2]+v3[0]*v1[2]-v1[0]*v3[2]-v2[0]*v1[2]-v3[0]*v2[2])+v0[2]*(v1[0]*v2[1]+v2[0]*v3[1]+v3[0]*v1[1]-v1[0]*v3[1]-v2[0]*v1[1]-v3[0]*v2[1])-(v1[0]*v2[1]*v3[2]+v2[0]*v3[1]*v1[2]+v3[0]*v1[1]*v2[2]-v1[0]*v3[1]*v2[2]-v2[0]*v1[1]*v3[2]-v3[0]*v2[1]*v1[2])
    return v
def point_in_TETRAHEDRON(p,v0,v1,v2,v3):
    vol0=cal_thrD_Determinant(v0,v1,v2,v3)
    vol1=cal_thrD_Determinant(p,v1,v2,v3)
    vol2=cal_thrD_Determinant(v0,p,v2,v3)
    vol3=cal_thrD_Determinant(v0,v1,p,v3)
    vol4=cal_thrD_Determinant(v0,v1,v2,p)
    INOUT=0
    if (abs(vol0)>1.0e-12 and abs(vol1)>1.0e-12 and abs(vol2)>1.0e-12 and abs(vol3)>1.0e-12 and abs(vol4)>1.0e-12):
        if (vol0*vol1>-1.0e-12 and vol0*vol2>-1.0e-12 and vol0*vol3>-1.0e-12 and vol0*vol4>-1.0e-12):
            INOUT=1

    # print('vol0=',vol0)
    # print('vol1=',vol1)
    # print('vol2=',vol2)
    # print('vol3=',vol3)
    # print('vol4=',vol4)
    # quit()
    return INOUT

def interpolation(face_id,v0,v1,v2,v3,vel0,vel1,vel2,vel3,p):
    Jacobian=(v1[0]-v0[0])*((v1[1]-v2[1])*(v2[2]-v3[2])-(v2[1]-v3[1])*(v1[2]-v2[2]))+(v2[0]-v1[0])*((v2[1]-v3[1])*(v0[2]-v1[2])-(v0[1]-v1[1])*(v2[2]-v3[2]))+(v3[0]-v2[0])*((v0[1]-v1[1])*(v1[2]-v2[2])-(v1[1]-v2[1])*(v0[2]-v1[2])) 
    Vol=Jacobian/6.0
    vol01=(v1[0]*(v2[1]*v3[2]-v3[1]*v2[2])+v2[0]*(v3[1]*v1[2]-v1[1]*v3[2])+v3[0]*(v1[1]*v2[2]-v2[1]*v1[2]))
    vol02=(v0[0]*(v3[1]*v2[2]-v2[1]*v3[2])+v2[0]*(v0[1]*v3[2]-v3[1]*v0[2])+v3[0]*(v2[1]*v0[2]-v0[1]*v2[2]))
    vol03=(v0[0]*(v1[1]*v3[2]-v3[1]*v1[2])+v1[0]*(v3[1]*v0[2]-v0[1]*v3[2])+v3[0]*(v0[1]*v1[2]-v1[1]*v0[2]))
    vol04=(v0[0]*(v2[1]*v1[2]-v1[1]*v2[2])+v1[0]*(v0[1]*v2[2]-v2[1]*v0[2])+v2[0]*(v1[1]*v0[2]-v0[1]*v1[2]))
    a1=(v3[1]-v1[1])*(v2[2]-v1[2])-(v2[1]-v1[1])*(v3[2]-v1[2])
    a2=(v2[1]-v0[1])*(v3[2]-v2[2])-(v2[1]-v3[1])*(v0[2]-v2[2])
    a3=(v1[1]-v3[1])*(v0[2]-v3[2])-(v0[1]-v3[1])*(v1[2]-v3[2])
    a4=(v0[1]-v2[1])*(v1[2]-v0[2])-(v0[1]-v1[1])*(v2[2]-v0[2])

    b1=(v2[0]-v1[0])*(v3[2]-v1[2])-(v3[0]-v1[0])*(v2[2]-v1[2])
    b2=(v3[0]-v2[0])*(v2[2]-v0[2])-(v0[0]-v2[0])*(v2[2]-v3[2])
    b3=(v0[0]-v3[0])*(v1[2]-v3[2])-(v1[0]-v3[0])*(v0[2]-v3[2])
    b4=(v1[0]-v0[0])*(v0[2]-v2[2])-(v2[0]-v0[0])*(v0[2]-v1[2])

    c1=(v3[0]-v1[0])*(v2[1]-v1[1])-(v2[0]-v1[0])*(v3[1]-v1[1])
    c2=(v2[0]-v0[0])*(v3[1]-v2[1])-(v2[0]-v3[0])*(v0[1]-v2[1])
    c3=(v1[0]-v3[0])*(v0[1]-v3[1])-(v0[0]-v3[0])*(v1[1]-v3[1])
    c4=(v0[0]-v2[0])*(v1[1]-v0[1])-(v0[0]-v1[0])*(v2[1]-v0[1])
    pvel=np.zeros(3,dtype=np.float64)
    for i in range(0,3):
        pvel[i]=((vol01+a1*p[0]+b1*p[1]+c1*p[2])*vel0[i]+(vol02+a2*p[0]+b2*p[1]+c2*p[2])*vel1[i]+(vol03+a3*p[0]+b3*p[1]+c3*p[2])*vel2[i]+(vol04+a4*p[0]+b4*p[1]+c4*p[2])*vel3[i])/Jacobian
    # print('pvel=',pvel)
    return pvel
def PtOne(p,meger3dTH,meshRefine_TETRAPoly,poro_rock,totalTime):
    orP=copy.deepcopy(p)
    ptPoints=[]
    ptVel=[]
    ptFaceId=[]
    ptTime=[]
    # totalTime=0
    eleNbDict={}
    # max(stats, key=stats.get)
    ptPoints.append(orP)
    ptFaceId.append(0)
    xyz=meger3dTH.points
    vel=meger3dTH['vectors']
    ele_ind=meger3dTH.cells.reshape(-1,5)
    maxxyz=np.max(xyz,axis=0)
    minxyz=np.min(xyz,axis=0)
    new_teatLen=((maxxyz[0]-minxyz[0])**2.0+(maxxyz[1]-minxyz[1])**2.0+(maxxyz[2]-minxyz[2])**2.0)**0.5/10.0
    # print('len(xyz)=',len(xyz))
    # print('len(ele_ind)=',len(ele_ind))
    i=meger3dTH.find_closest_cell(orP)
    # INOUT=point_in_TETRAHEDRON(orP,xyz[ele_ind[i][1]],xyz[ele_ind[i][2]],xyz[ele_ind[i][3]],xyz[ele_ind[i][4]])
    INOUT=1
    # print('INOUT=',INOUT)
    if INOUT!=1:
        i=0
        while (i<len(ele_ind) or INOUT==1 ):
            # print('ele_ind',ele_ind[i])
            # print('v0',xyz[ele_ind[i][1]]) 
            INOUT=point_in_TETRAHEDRON(orP,xyz[ele_ind[i][1]],xyz[ele_ind[i][2]],xyz[ele_ind[i][3]],xyz[ele_ind[i][4]])
            if (INOUT==1):
                # print('p in ',i)
                PointInELE_NB=i
                break
            else:
                i=i+1
                # print(i,len(ele_ind))
    else:
        face_id=0
        PointInELE_NB=i
        # print('PointInELE_NB=',PointInELE_NB)
    
    face_id=0
    nb=0
    if INOUT==1:
        # print(orP)
        runPt=True
        i=PointInELE_NB
        while(runPt):
            # nb=nb+1            
            pvel=interpolation(
                face_id,
                xyz[ele_ind[i][1]],xyz[ele_ind[i][2]],xyz[ele_ind[i][3]],xyz[ele_ind[i][4]],
                vel[ele_ind[i][1]],vel[ele_ind[i][2]],vel[ele_ind[i][3]],vel[ele_ind[i][4]],
                orP)
            # print(nb,pvel,i)            
            unitV=((pvel[0])**2.0+(pvel[1])**2.0+(pvel[2])**2.0)**0.5
            new_p=np.zeros(3,dtype=np.float64)
            for j in range(0,3):
                new_p[j]=orP[j]+pvel[j]/unitV*new_teatLen
            # ray = pv.Line(p, new_p)        
            # ray.save('ray.vtk')
            # print(meshRefine_TETRAPoly)
            points=[]
            points, ind = meshRefine_TETRAPoly.ray_trace(orP, new_p)
            # print('i,ind=',i,ind)
            if len(points)>0:
                for j in range(0,len(points)):
                    if (orP[0]==points[j][0] and orP[1]==points[j][1] and orP[2]==points[j][2]):
                        k=0
                    else:
                        k=1
                        # print('j=',j)
                        ptPoints.append(points[j])
                        ptVel.append(pvel)                        
                        ptFaceId.append(meshRefine_TETRAPoly['face_markers'][ind[j]])
                        lenth=((orP[0]-points[j][0])**2.0+(orP[1]-points[j][1])**2.0+(orP[2]-points[j][2])**2.0)**0.5
                        orP=np.array(points[j])
                        break
                if k==0 :
                    print("check orP and points")
                    break
                totalTime=totalTime+lenth/(unitV/poro_rock[0])/(86400.0*365.0)
                ptTime.append(totalTime)
                i=meger3dTH.find_closest_cell(orP)
                print(orP[0],orP[1],orP[2],totalTime)
                # if nb>5 : quit()
            else:
                runPt=False
            if str(i) in eleNbDict.keys():
                eleNbDict[str(i)]=eleNbDict[str(i)]+1
            else:
                eleNbDict[str(i)]=1
            # 
            maxKeyId=max(eleNbDict, key=eleNbDict.get)
            if (eleNbDict[str(maxKeyId)]>50):
                runPt=False
                print("eleNbDict[str(maxKeyId)]>50")
                break

        ptVel.append(pvel)
        nb=int(len(ptTime)-1)
        totalTime=totalTime+ptTime[nb]
        ptTime.append(totalTime)
        print('Finish =====')
        # for i in range(0,len(ptPoints)):
        #     print(ptPoints[i][0],ptPoints[i][1],ptPoints[i][2],ptTime[i])
        ptPoints=np.array(ptPoints)
        ptVel=np.array(ptVel)
        ptTime=np.array(ptTime)
        ptPoly=pv.PolyData(ptPoints)
        ptPoly['ptVel']=ptVel
        ptPoly['time']=ptTime
        # ptPoly.save('ptVel.vtk')
        # quit()

  
    
    return ptPoly

def readConnSTL(q123Folder):

    '''讀取原始FAB與STL轉換後的vtk'''
    dfnFABQ123=pv.read(q123Folder+r'/dfnFAB.vtk')
    dhQ123=pv.read(q123Folder+r'/dh.vtk')
    edzQ123=pv.read(q123Folder+r'/edz.vtk')
    dtQ123=pv.read(q123Folder+r'/dt.vtk')
    mtQ123=pv.read(q123Folder+r'/mt.vtk')



    '''#計算每個vtk物件,具有多少組不連接的元件'''
    noConnfrac=len(np.unique(dfnFABQ123['RegionId']))
    noConnDH=len(np.unique(dhQ123['RegionId']))
    noConnedz=len(np.unique(edzQ123['RegionId']))
    noConnDt=len(np.unique(dtQ123['RegionId']))
    noConnMt=len(np.unique(mtQ123['RegionId']))
    # print("noConnDH=",noConnDH)
    # print("noConnfrac=",noConnfrac)
    # print("noConnedz=",noConnedz)
    # print("noConnMt=",noConnMt)
    # quit()

    '''
    # 讀取Q123，每個物件交互關係
    holeInd==-1, 完全沒有交結
    # holeInd==0 代表部份貫穿
    # holeInd==1 代表完全貫穿
    # q123Folder=r'../../HybridTW/001_stlToMesh'
    '''
    dhfrac = np.fromfile(q123Folder+'/Q1vtk/dh_frac.bin', dtype=np.int32).reshape(noConnDH, noConnfrac)
    dhedz = np.fromfile(q123Folder+'/Q2vtk/dh_edz.bin', dtype=np.int32).reshape(noConnDH, noConnedz)
    dtfrac = np.fromfile(q123Folder+'/Q3vtk/dt_frac.bin', dtype=np.int32).reshape(noConnDt, noConnfrac)
    # print("dhfrac=",dhfrac)
    # quit()

    
    return noConnfrac,noConnDH,noConnedz,noConnDt,noConnMt,dhfrac,dhedz,dtfrac,dfnFABQ123,dhQ123,edzQ123,dtQ123,mtQ123

def FindMaxFluxNode(\
      dfnOrUsg,q123Folder,FABResfolder,\
        noConnfrac,noConnDH,noConnedz,noConnDt,noConnMt,\
          dhfrac,dhedz,dtfrac,dfnFABQ123,\
            dhQ123,edzQ123,dtQ123,mtQ123,\
              meger3dTH,poro_fracture,edzProfile,poro_edz):

  # fullConnNBforDhFrac [0]=STL編號, [1]=裂隙編號,從0開始編號
  '''# terminal_speed代表最低速度'''
  # terminal_speed=np.min(meger3dTH['velocityLen']) 
  
  print(dfnOrUsg['No']) # 裂隙編號,從1開始編號

  Q1PtPoint=[]
  Q1PtUr=[]
  Q1STL=[]
  Q1FAB=[]

  case=1
  if (case==1):
    '''#讀取Q1部份貫穿'''
    titleA=r'DH'
    titleB=r'Fracture'  
    print("titleA,titleB=",titleA,titleB) 

    '''# partConnNBforDhFrac代表dfn與Dh部份貫穿編號'''
    partConnNBforDhFrac = np.where((dhfrac== 0)) # dhfrac==0 代表部份貫穿
    # print("partConnNBforDhFrac=",partConnNBforDhFrac)
    '''
    (array([   5,   75,   92,  261,  290,  464,  530,  624,  782,  788,  845,
       1010, 1084, 1143, 1156, 1171, 1179, 1180, 1451, 1509, 1842, 2107,
       2133, 2138, 2139, 2267, 2290, 2445, 2450, 2485, 2496, 2566, 2640]), 
    array([6481, 7170, 1609, 7357, 8180, 4534, 4424, 6456, 6288, 4557, 4422,
       1477, 3621,   27, 1497, 1493, 1491, 1493, 6495,   19, 6478, 5667,
       3426, 4409, 6470, 4423, 6615, 1490, 1478, 3345,    8, 1475, 6468]))
    '''

    partConnNBforDhFrac=np.array(partConnNBforDhFrac).swapaxes(0,1) 
    # print("partConnNBforDhFrac=")
    # print(partConnNBforDhFrac)
    '''
    # partConnNBforDhFrac=[[   5 6481] [  75 7170] [  92 1609]...[2496    8] [2566 1475] [2640 6468]]
    '''

    '''
    # 確認查詢部份截切裂隙，是否參與
    # 若無則透過np.delete刪除
    '''
    delInd=[]
    for i in range(0,len(partConnNBforDhFrac)):
      if (partConnNBforDhFrac[i,1]+1 not in dfnOrUsg["No"]):
        delInd.append(i)
    partConnNBforDhFrac=np.delete(partConnNBforDhFrac, delInd, 0)

    '''
    # 將確認過部份截切資料寫入csv檔案
    '''
    filename=r'Q1Part.csv'
    ToWriteConnNB(titleA,titleB,filename,partConnNBforDhFrac)

    '''#讀取Q1完全貫穿'''
    fullConnNBforDhFrac = np.where((dhfrac== 1))
    fullConnNBforDhFrac=np.array(fullConnNBforDhFrac).swapaxes(0,1)
    # print(fullConnNBforDhFrac)
    delInd=[]
    for i in range(0,len(fullConnNBforDhFrac)):
      if (fullConnNBforDhFrac[i,1]+1 not in dfnOrUsg["No"]):
        delInd.append(i)
    fullConnNBforDhFrac=np.delete(fullConnNBforDhFrac, delInd, 0)    
    filename=r'Q1Full.csv'
    ToWriteConnNB(titleA,titleB,filename,fullConnNBforDhFrac)
    # quit()

    '''
    # 讀取FAB裂隙水力傳導係數與裂隙寬
    # 將FAB檔案儲存為binary檔案
    '''
    file=FABResfolder+'/frac_K_ap.bin'
    flowResFileCheck = Path(file)
    if (flowResFileCheck.is_file):
      f = FortranFile(file, 'r')
      frac_K = f.read_reals(dtype=np.float64)
      frac_ap = f.read_reals(dtype=np.float64)
      f.close()   

    Q1FullConnNb=copy.deepcopy(fullConnNBforDhFrac)
    Q1EqFlux=np.zeros(len(fullConnNBforDhFrac),dtype=np.float64)

    '''#測試單dh完全截切PT'''
    # for i in range(0,len(fullConnNBforDhFrac)):
    for i in range(0,10):
    # for i in range(0,5):
      # if(fullConnNBforDhFrac[i][1]==4407):
      #   testNB=i
      #   break
      testNB=i
      # print("testNB=",testNB)
      # quit()
            
      if (fullConnNBforDhFrac[testNB][1]+1 in dfnOrUsg["No"]):
        print('dh #',fullConnNBforDhFrac[testNB][0],',fracture #',fullConnNBforDhFrac[testNB][1])

        '''# 創造讀取完全截切的vtk檔案名稱'''
        sigleDhFracVtkF = \
          q123Folder + r'/Q1vtk/'+\
            'dh'+str(fullConnNBforDhFrac[testNB][0]) + \
            'frac'+str(fullConnNBforDhFrac[testNB][1])+'.vtk'
        
        '''#紀錄完全截切dh與fracture編號 '''
        Q1STL.append(fullConnNBforDhFrac[testNB][0])
        Q1FAB.append(fullConnNBforDhFrac[testNB][1])
        
        '''#讀取完全截切創造節點,並完成速度插值'''
        inPart=pv.read(sigleDhFracVtkF)
        inPart=inPart.interpolate(\
           meger3dTH,\
            sharpness=2, radius=5000.0, strategy='null_value', null_value=0.0, \
              n_points=50, pass_point_data=True, progress_bar=False)

        outfile_name=r'Q1UrVector.dat'
        aperture=frac_ap[fullConnNBforDhFrac[testNB][1]] #紀錄單一裂隙開口寬

        '''# 計算完全截切所有節點線段位置與垂直線段流速'''
        inPartEdge,Q1EqFlux[testNB]=DfnEqFlux(outfile_name,inPart,aperture,poro_fracture)

        '''# 確保節點上存有流速'''
        inPartEdge=inPartEdge.cell_data_to_point_data()

        '''
        # 單一截切，判斷最大速度
        '''
        maxVelL=np.max(inPartEdge['velocityLen'])
        nb=np.max(np.where(inPartEdge['velocityLen']==maxVelL))
        Q1PtPoint.append([\
           np.float64(inPartEdge.points[nb][0]),\
            np.float64(inPartEdge.points[nb][1]),\
              np.float64(inPartEdge.points[nb][2])])
        Q1PtUr.append(np.float64(inPartEdge["UrVector"][nb]))

    '''# 將所有最大速度釋放點座標,創造PolyData'''
    Q1PT=pv.PolyData(Q1PtPoint)

    '''# PolyData 加上 Ur, dh與fracture編號'''
    Q1PT["Ur"]=Q1PtUr
    Q1PT["STL"]=Q1STL
    Q1PT["FAB"]=Q1FAB

  '''#教育訓練關閉Q2'''
  case=0
  Q2PtPoint=[]
  Q2PtUr=[]
  Q2STL=[]
  Q2FAB=[]
  if (case==1):
    '''#讀取Q2部份貫穿'''
    titleA=r'DH'
    titleB=r'Edz'  
    print("titleA,titleB=",titleA,titleB)              
    partConnNBforDhEdz = np.where((dhedz== 0))
    partConnNBforDhEdz=np.array(partConnNBforDhEdz).swapaxes(0,1)
    filename=r'Q2Part.csv'
    ToWriteConnNB(titleA,titleB,filename,partConnNBforDhEdz)
    '''#讀取Q2完全貫穿'''
    fullConnNBforDhEdz = np.where((dhedz== 1))
    fullConnNBforDhEdz=np.array(fullConnNBforDhEdz).swapaxes(0,1)
    filename=r'Q2Full.csv'
    ToWriteConnNB(titleA,titleB,filename,fullConnNBforDhEdz)
    Q2FullConnNb=copy.deepcopy(fullConnNBforDhEdz)
    Q2EqFlux=np.zeros(len(fullConnNBforDhEdz),dtype=np.float64)

    '''#測試單dh完全截切PT'''
    for i in range(0,len(fullConnNBforDhEdz)):
      testNB=i      
      sigleDhFracVtkF=q123Folder+r'/Q2vtk/'+'dh'+str(fullConnNBforDhEdz[testNB][0])+'edz'+str(fullConnNBforDhEdz[testNB][1])+'.vtk'
      Q2STL.append(fullConnNBforDhEdz[testNB][0])
      Q2FAB.append(fullConnNBforDhEdz[testNB][1])
      inPart=pv.read(sigleDhFracVtkF)
      inPart=inPart.interpolate(meger3dTH, sharpness=2, radius=5000.0, strategy='null_value', null_value=0.0, n_points=50, pass_point_data=True, progress_bar=False)
      outfile_name=r'Q2UrVector.dat'
      aperture=edzProfile
      inPartEdge,Q2EqFlux[testNB]=DfnEqFlux(outfile_name,inPart,aperture,poro_edz)
      inPartEdge=inPartEdge.cell_data_to_point_data()
      maxVelL=np.max(inPartEdge['velocityLen'])
      nb=np.max(np.where(inPartEdge['velocityLen']==maxVelL))
      Q2PtPoint.append([np.float64(inPartEdge.points[nb][0]),np.float64(inPartEdge.points[nb][1]),np.float64(inPartEdge.points[nb][2])])
      Q2PtUr.append(np.float64(inPartEdge["UrVector"][nb]))
    Q2PT=pv.PolyData(Q2PtPoint)
    Q2PT["Ur"]=Q2PtUr
    Q2PT["STL"]=Q2STL
    Q2PT["FAB"]=Q2FAB    

  Q3PtPoint=[]
  Q3PtUr=[]
  Q3STL=[]
  Q3FAB=[]  
  case=1
  if (case==1):
    '''#讀取Q3部份貫穿'''
    titleA=r'DT'
    titleB=r'Fracture'  
    print("titleA,titleB=",titleA,titleB)              
    partConnNBforDtFrac = np.where((dtfrac== 0))
    partConnNBforDtFrac=np.array(partConnNBforDtFrac).swapaxes(0,1)
    delInd=[]
    for i in range(0,len(partConnNBforDtFrac)):
      if (partConnNBforDtFrac[i,1]+1 not in dfnOrUsg["No"]):
        delInd.append(i)
    partConnNBforDtFrac=np.delete(partConnNBforDtFrac, delInd, 0)
    filename=r'Q3Part.csv'
    ToWriteConnNB(titleA,titleB,filename,partConnNBforDtFrac)
    '''#讀取Q3完全貫穿'''
    fullConnNBforDtFrac = np.where((dtfrac== 1))
    fullConnNBforDtFrac=np.array(fullConnNBforDtFrac).swapaxes(0,1)
    delInd=[]
    for i in range(0,len(fullConnNBforDtFrac)):
      if (fullConnNBforDtFrac[i,1]+1 not in dfnOrUsg["No"]):
        delInd.append(i)
    fullConnNBforDtFrac=np.delete(fullConnNBforDtFrac, delInd, 0)
    filename=r'Q3Full.csv'
    ToWriteConnNB(titleA,titleB,filename,fullConnNBforDtFrac) 
    Q3FullConnNb=copy.deepcopy(fullConnNBforDtFrac)
    Q3EqFlux=np.zeros(len(fullConnNBforDtFrac),dtype=np.float64)    

    '''#測試單dh完全截切PT'''
    for i in range(0,len(fullConnNBforDtFrac)):  
      testNB=i 
      if (fullConnNBforDtFrac[testNB][1]+1 in dfnOrUsg["No"]):
        print('No',fullConnNBforDtFrac[testNB][1])
        sigleDhFracVtkF=q123Folder+r'/Q3vtk/'+'dt'+str(fullConnNBforDtFrac[testNB][0])+'frac'+str(fullConnNBforDtFrac[testNB][1])+'.vtk'
        Q3STL.append(fullConnNBforDtFrac[testNB][0])
        Q3FAB.append(fullConnNBforDtFrac[testNB][1])      
        inPart=pv.read(sigleDhFracVtkF)
        inPart=inPart.interpolate(meger3dTH, sharpness=2, radius=5000.0, strategy='null_value', null_value=0.0, n_points=50, pass_point_data=True, progress_bar=False)
        outfile_name=r'Q3UrVector.dat'
        aperture=frac_ap[fullConnNBforDtFrac[testNB][1]]
        inPartEdge,Q3EqFlux[testNB]=DfnEqFlux(outfile_name,inPart,aperture,poro_fracture)
        inPartEdge=inPartEdge.cell_data_to_point_data()
        maxVelL=np.max(inPartEdge['velocityLen'])
        nb=np.max(np.where(inPartEdge['velocityLen']==maxVelL))
        Q3PtPoint.append([np.float64(inPartEdge.points[nb][0]),np.float64(inPartEdge.points[nb][1]),np.float64(inPartEdge.points[nb][2])])
        Q3PtUr.append(np.float64(inPartEdge["UrVector"][nb]))
    Q3PT=pv.PolyData(Q3PtPoint)
    Q3PT["Ur"]=Q3PtUr
    Q3PT["STL"]=Q3STL
    Q3PT["FAB"]=Q3FAB 

  '''#教育訓練關閉Q2Q3'''
  Q2PT=Q1PT
  Q2FullConnNb=Q1FullConnNb
  Q2EqFlux=Q1EqFlux
  Q3PT=Q1PT
  Q3FullConnNb=Q1FullConnNb
  Q3EqFlux=Q1EqFlux

  return Q1PT,Q2PT,Q3PT,Q1FullConnNb,Q2FullConnNb,Q3FullConnNb,Q1EqFlux,Q2EqFlux,Q3EqFlux

def streamlines_from_source(\
      meshFlow,PtStart,initial_step_length,\
        min_step_length,max_step_length,max_steps,terminal_speed,max_error):
  
  streamlines=meshFlow.streamlines_from_source(\
     source=PtStart, \
      vectors='vectors', \
      integrator_type=45, \
      integration_direction='forward', \
      surface_streamlines=False, \
      initial_step_length=initial_step_length, \
      step_unit='l', \
      min_step_length=min_step_length, \
      max_step_length=max_step_length, \
      max_steps=max_steps, \
      terminal_speed=terminal_speed, \
      max_error=max_error, \
      max_time=None, compute_vorticity=True, rotation_scale=1.0, interpolator_type='point', progress_bar=False)
  return streamlines

def isPtInSTL(edz,scalarName,streamlines):
  #deepcopy streamlines,避免影響原始streamlines
  streamlines_edz=streamlines.copy()

  #創造質點傳輸節點編號
  streamlines_edz["lineNb"]=np.linspace(\
    start=0,stop=streamlines_edz.n_points,\
    num=streamlines_edz.n_points,endpoint=False,dtype=np.int64)
  
  #預設所有節點不在3D物件內
  streamlines_edz[scalarName]=np.full(shape=streamlines_edz.n_points,fill_value=False)

  # 運用3D物件封閉性,去除不在3D物件內的質點
  streamlines_edzClip=streamlines_edz.clip_surface(\
    edz, invert=True, value=0.0, compute_distance=True, \
      progress_bar=False, crinkle=False)

  # clip留下的質點,設定為True
  streamlines_edz[scalarName][streamlines_edzClip["lineNb"]]=True
  # for i in range(len(streamlines_edzClip["lineNb"])):
  #   streamlines_edz[scalarName][streamlines_edzClip["lineNb"][i]]=True
  
  return streamlines_edz

def isPtInDFN(dfnOrUsg,streamlines):
  # streamlinesFalse=streamlines.copy()
  streamlinesFalse=streamlines.clip_surface(dfnOrUsg, invert=False, value=0.0, compute_distance=True, progress_bar=False, crinkle=True)
  streamlinesFalse["implicit_distance"]=np.abs(streamlinesFalse["implicit_distance"])

  # 創造質點最接近裂隙編號矩陣
  streamlinesFalse['closestDfn']=np.zeros(streamlinesFalse.n_points,dtype=np.int64)
  
  # 判斷質點位於哪片裂隙編號
  index=dfnOrUsg.find_closest_cell(streamlinesFalse.points)
  streamlinesFalse['closestDfn'][0:]=dfnOrUsg['No'][index[0:]]
  # for i in range(0,len(index)):
  #   streamlinesFalse['closestDfn'][i]=dfnOrUsg['No'][index[i]]
  
  return streamlinesFalse



def performanceMeasurement(\
    dfnOrUsg,streamlines,dfnsp,\
    poro_fracture,poro_surface,poro_rock,poro_F1F2K,\
    fracture_alpha_w,F1F2K_alpha_w,surface_alpha_w,matrix_alpha_w):
  
  streamlines["lineNb"]=np.linspace(\
    start=0,stop=streamlines.n_points,\
    num=streamlines.n_points,endpoint=False,dtype=np.int64)
  streamlines["travelTime"]=np.zeros(streamlines.n_points,dtype=np.float64)
  streamlines["PathLength"]=np.zeros(streamlines.n_points,dtype=np.float64)
  streamlines["Fr"]=np.zeros(streamlines.n_points,dtype=np.float64)

  # streamlines['SeedIds']代表質點編號
  for SeedId in streamlines['SeedIds']:

    # 取得單獨質點編號vtk--方法1
    streamLineBool=streamlines['SeedIds']!=SeedId
    streamLine_seedId=streamlines.remove_cells(streamLineBool,inplace=False)

    # 取得單獨質點編號vtk--方法2
    # streamLineBool=streamlines['SeedIds']==SeedId
    # streamLine_seedId=streamlines.extract_cells(streamLineBool)
    
    # print("=============")
    # print("SeedId=",SeedId,len(streamlines['SeedIds']))

    # 初始化
    TimeR=np.float64(0.0)
    pathLen=np.float64(0.0)
    Fr=np.float64(0.0)

    for i in range(0,streamLine_seedId.n_points-1):
      if streamLine_seedId["implicit_distance"][i]<dfnsp:
        poro=poro_fracture
        alpha_w=fracture_alpha_w
      else:
        if streamLine_seedId["IsEdz"][i]:
          poro=poro_rock[1]
          alpha_w=fracture_alpha_w
        else:
          if streamLine_seedId["IsFault"][i]:
            poro=poro_F1F2K
            alpha_w=F1F2K_alpha_w
          else:
            if streamLine_seedId["IsSurface"][i]:
              poro=poro_surface
              alpha_w=surface_alpha_w
            else:
              poro=poro_rock[0]
              alpha_w=matrix_alpha_w
      # if streamLine_seedId["implicit_distance"][i]<dfnsp:
      #   alpha_w=fracture_alpha_w
      # else:
      #   if streamLine_seedId["IsFault"][i]:
      #     alpha_w=F1F2K_alpha_w
      #   else:
      #     if streamLine_seedId["IsSurface"][i]:
      #       alpha_w=surface_alpha_w
      #     else:
      #       alpha_w=matrix_alpha_w
      # print("poro=",poro)

      # 計算質點傳輸路徑長度
      dis=np.linalg.norm(streamLine_seedId.points[i] - streamLine_seedId.points[i+1])
      # 計算質點速度長度
      vLen=np.linalg.norm(streamLine_seedId['vectors'][i])
      # 計算傳輸時間
      PtTime=dis/(vLen/poro)
      # 累加傳輸時間
      TimeR=TimeR+PtTime
      # 累加傳輸距離
      pathLen=pathLen+dis
      # 計算與累加Fr
      Fr=Fr+alpha_w*PtTime

      streamlines["travelTime"][streamLine_seedId["lineNb"][i+1]]=TimeR
      streamlines["PathLength"][streamLine_seedId["lineNb"][i+1]]=pathLen
      streamlines["Fr"][streamLine_seedId["lineNb"][i+1]]=Fr

  return streamlines


def Density2Con(meger3dVtkF,meger3dTH,meger3dTH_nonDensity,alpha,largeDem,radius):

    # profileBcNodeInterpolate(bcProfileFace,largeDem,'z',radius)
    points=np.zeros((len(meger3dTH.points),3),dtype=np.float64)
    points[:,0:2]=meger3dTH.points[:,0:2]
    poly=pv.PolyData(points)

    Topz= poly.interpolate(largeDem, radius=radius)
    Botz=meger3dTH.points[:,2:3]
    # print("Topz['z']")
    # print(Topz['z'][:])
    # print(Topz['z'].shape)
    # print("Botz")
    # print(Botz[:])
    # print(Botz.shape)
    
    detaz=np.zeros(len(meger3dTH['totalHead']),dtype=np.float64)
    detaTh=np.zeros(len(meger3dTH['totalHead']),dtype=np.float64)
    sa=np.zeros(len(meger3dTH['totalHead']),dtype=np.float64)
    nb=0
    for i in range(0,len(meger3dTH['totalHead'])):
        detaz[i]=Topz['z'][i]-Botz[i]        
        detaTh[i]=meger3dTH['totalHead'][i]-meger3dTH_nonDensity['totalHead'][i]-Topz['z'][i]+meger3dTH.points[i][2]
        if (detaTh[i]>0.0):
            nb=nb+1
            sa[i]=detaTh[i]/detaz[i]/alpha
            # print("i,dz,dTh,TH,nTH,Tz,sa[i]=",i,' ',detaz[i],' ',detaTh[i],meger3dTH['totalHead'][i],' ',meger3dTH_nonDensity['totalHead'][i],' ',Topz['z'][i],' ',sa[i])

    meger3dTH['detaz']=detaz
    meger3dTH['detaTh']=detaTh
    meger3dTH['sa']= sa
    print("meger3dTH['sa'] OK")
    meger3dTH.save('meger3dCon.vtk')

    xyz=meger3dTH.points
    ele_ind=meger3dTH.cells.reshape(-1,5)
    testPLOTfilename=r'meger3dCon.dat'
    no_node = len(xyz)
    no_ele = len(ele_ind)
    with open(testPLOTfilename,'w',encoding = 'utf-8') as f:
        f.write('Title="XY2D_plot"\n')
        f.write('Variables="x(m)","y(m)","z(m)","Th(m)","vx","vy","vz","dTh(m)","dz(m)","sa"\n')
        f.write('Zone N='+str(no_node)+',E='+str(no_ele)+',F=FEPOINT,ET=TETRAHEDRON\n')
        for i in range(0,no_node):
            f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+' '+str(meger3dTH['totalHead'][i])+' '+str(meger3dTH['vectors'][i][0])+' '+str(meger3dTH['vectors'][i][1])+' '+str(meger3dTH['vectors'][i][2])+' '+str(meger3dTH['detaTh'][i])+' '+str(meger3dTH['detaz'][i])+' '+str(meger3dTH['sa'][i])+'\n')
        for i in range(0,no_ele):
            f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+' '+str(ele_ind[i][4]+1)+'\n')
        f.close()

    return 0

def ToWriteConnNB(titleA,titleB,filename,partConnNBforDhFrac):
    import pandas as pd
    # partConnNBforDhFrac=np.array(partConnNBforDhFrac).swapaxes(0,1)
    # print(type(partConnNBforDhFrac))
    # print(partConnNBforDhFrac.shape)
    # print(partConnNBforDhFrac[0])
    df = pd.DataFrame(partConnNBforDhFrac, columns=[titleA,titleB])
    df.to_csv(filename, columns=[titleA,titleB], index = False)
    return 0

def readFlowTH(meshTetraRefinement):
    # print(meger3d.array_names)
    # quit()
    file=r'flowTH.bin'    
    flowTHPath = Path(file)
    if flowTHPath.is_file():            
        f = FortranFile(file, 'r')
        no_node = f.read_reals(dtype=np.int32)
        no_ele = f.read_reals(dtype=np.int32)
        x = f.read_reals(dtype=np.float64)
        y = f.read_reals(dtype=np.float64)
        z = f.read_reals(dtype=np.float64)
        ele_ind = f.read_reals(dtype=np.int32)
        TH = f.read_reals(dtype=np.float64)
        vx = f.read_reals(dtype=np.float64)
        vy = f.read_reals(dtype=np.float64)
        vz = f.read_reals(dtype=np.float64)
        f.close()
        vectors=np.column_stack((vx,vy,vz))
        meshTetraRefinement['vectors']=vectors
        meshTetraRefinement['velocityLen']=np.linalg.norm(vectors,axis=1)
        meshTetraRefinement['totalHead']=TH
        # print(meger3d.array_names)
        # meger3d=meger3d.cell_data_to_point_data(pass_cell_data = True)
        
        # plotter = pv.Plotter(off_screen=True)
        # plotter.add_mesh(meger3d, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=False,lighting=True,reset_camera=True,culling=False,metallic=1.0)   
        # plotter.show(screenshot='meger3dTH.png')         
        # min=np.min(meger3d.points,axis=0)
        # max=np.max(meger3d.points,axis=0)
        # x=(min[0]+max[0])/2.0-delta_x
        # y=(min[1]+max[1])/2.0-delta_y
        # x84,y84=readGeoJSON.transGeoCoor(x,y,EpsgIn,EpsgOut)
        # print('meger3d cxcy=', x84,y84)


    return meshTetraRefinement

def localMeshByNb(meshTetraRefinementHybridFolw,EleNb):
  ind=np.full(shape=meshTetraRefinementHybridFolw.n_cells,fill_value=True,dtype=np.bool_)
  for i in range(0,len(EleNb)):
    ind[EleNb[i]]=False
  mesh=meshTetraRefinementHybridFolw.remove_cells(ind,inplace=False)
  return mesh

def tecplotHybridFlow(outputfile,mesh):
  xyz=mesh.points
  ele_ind=mesh.cells.reshape(-1,5)
  with open(outputfile,'w',encoding = 'utf-8') as f:
    f.write('Title="XY2D_plot"\n')
    f.write('Variables="x(m)","y(m)","z(m)","HT(m)","vx(m/day)","vy(m/day)","vz(m/day)","v(m/day)"\n')
    f.write('Zone N='+str(mesh.n_points)+',E='+str(mesh.n_cells)+',F=FEPOINT,ET=TETRAHEDRON\n')
    for i in range(0,mesh.n_points):
      f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+' '+str(mesh["totalHead"][i])+' '+str(mesh["vectors"][i][0])+' '+str(mesh["vectors"][i][0])+' '+str(mesh["vectors"][i][0])+' '+str(mesh["velocityLen"][i])+'\n')
    for i in range(0,mesh.n_cells):
      f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+' '+str(ele_ind[i][4]+1)+'\n')
    f.close()


def runDualDomain_flowFortanProgram(programName):
    os.system(r'./'+programName)

def writeInputDensity(density,meshTetraRefinement,meshTetraPolyRefinement,edzMeshEleNb,faultMeshEleNb,dfnOrUsg,dfnPolyNp,Kfracture,poro_fracture,Krock,poro_rock,Ksurface,poro_surface,F1F2K,poro_F1F2K,demClip2d):

  xyz=meshTetraRefinement.points
  ele_ind=meshTetraRefinement.cells.reshape(-1,5)
  no_node=len(xyz)
  no_ele=len(ele_ind)
  ind=meshTetraRefinement["element_attributes"]!=-1
  surface=meshTetraRefinement.remove_cells(ind,inplace=False)
  meshK=np.full(fill_value=Krock[0],shape=no_ele,dtype=np.float64)
  meshporo=np.full(fill_value=poro_rock[0],shape=no_ele,dtype=np.float64)

  '''
  # print Rock HK
  '''
  meshK[surface["eleNb"][:]] = Ksurface
  meshporo[surface["eleNb"][:]] = poro_surface
  meshK[faultMeshEleNb[:]] = F1F2K
  meshporo[faultMeshEleNb[:]] = poro_F1F2K
  meshK[edzMeshEleNb[:]] = Krock[1]
  meshporo[edzMeshEleNb[:]] = poro_rock[1]
  # for i in range(0,surface.n_cells):
  #   meshK[surface["eleNb"]]=Ksurface
  #   meshporo[surface["eleNb"]]=poro_surface
  # for i in range(0,len(faultMeshEleNb)):
  #   meshK[faultMeshEleNb[i]]=F1F2K
  #   meshporo[faultMeshEleNb[i]]=poro_F1F2K
  # for i in range(0,len(edzMeshEleNb)):
  #   meshK[edzMeshEleNb[i]]=Krock[1]
  #   meshporo[edzMeshEleNb[i]]=poro_rock[1] 
  print('start FEM_AGMG_HK')
  with open('FEM_AGMG_HK.dat','w',encoding = 'utf-8') as f:
    np.savetxt(f, np.column_stack((meshK,meshK,meshK,meshporo)),delimiter=' ',)
    f.close()
  # quit()        

  '''
  # print MESH
  '''
  print('start FEM_AGMG_MESH')
  ele_indP1=copy.deepcopy(ele_ind)
  ele_indP1=ele_indP1+1
  with open('FEM_AGMG_MESH.dat','w',encoding = 'utf-8') as f:
    f.write(str(no_node)+' '+str(no_ele)+'\n')
    np.savetxt(f, np.column_stack((xyz[:,0],xyz[:,1],xyz[:,2])))
    np.savetxt(f, np.column_stack((ele_indP1[:,1],ele_indP1[:,2],ele_indP1[:,3],ele_indP1[:,4])),fmt=["%u","%u","%u","%u"])
    f.close()
  # quit()


  '''
  # print fracture HK
  '''
  print('start fracture HK')
  ind = np.argwhere(meshTetraPolyRefinement["face_markers"] <=0)
  fractureFace=meshTetraPolyRefinement.remove_cells(ind,inplace=False)
  fractureFace_ele_ind=meshTetraPolyRefinement.faces.reshape(-1,4)
  no_face_type=6
  Dualface_markers=np.linspace(start=1,stop=fractureFace.n_cells,num=fractureFace.n_cells,endpoint=True,dtype=np.int64)
  with open('FEM_fracture_ele_HK.dat','w',encoding = 'utf-8') as f:
    f.write(str(fractureFace.n_cells)+' '+str(no_face_type)+'\n')
    np.savetxt(f, Dualface_markers,fmt="%u")
    # np.savetxt(f, fractureFace["face_markers"],fmt="%u")
    f.write(str(fractureFace.n_cells)+'\n')
    for i in range(0,fractureFace.n_cells):
      e0=fractureFace_ele_ind[fractureFace["faceNb"][i]][1]+1
      e1=fractureFace_ele_ind[fractureFace["faceNb"][i]][2]+1
      e2=fractureFace_ele_ind[fractureFace["faceNb"][i]][3]+1
      # f.write(str(e0)+' '+str(e1)+' '+str(e2)+' '+str(dfnPolyNp["fractureK"][fractureFace["face_markers"][i]-1])+' '+str(fractureFace["face_markers"][i])+' '+str(poro_fracture)+'\n')
      f.write(str(e0)+' '+str(e1)+' '+str(e2)+' '+str(dfnPolyNp["fractureK"][fractureFace["face_markers"][i]-1])+' '+str(int(i+1))+' '+str(poro_fracture)+'\n')
    f.close()

  

  '''
  # print boundary
  '''  
  print('start FEM_AGMG_BC1_inf')
  print("density=",density)
  meshTetraPolyRefinement["nodeNb"]=np.linspace(start=0,stop=meshTetraPolyRefinement.n_points,num=meshTetraPolyRefinement.n_points,dtype=np.int64)
  topind = np.argwhere(meshTetraPolyRefinement["face_markers"] !=-1)
  profileind = np.argwhere(meshTetraPolyRefinement["face_markers"] !=-2)
  topMesh=meshTetraPolyRefinement.remove_cells(topind,inplace=False)
  profileMesh=meshTetraPolyRefinement.remove_cells(profileind,inplace=False)

  points=np.zeros((profileMesh.n_points,3),dtype=np.float64)
  points[:,0:2]=profileMesh.points[:,0:2]
  profilePoint=pv.PolyData(points)
  bounds=demClip2d.bounds
  dist = np.linalg.norm(np.array([bounds[0],bounds[2],bounds[4]]) - np.array([bounds[1],bounds[3],bounds[5]]))

  profilePoint=profilePoint.interpolate(demClip2d, sharpness=2, radius=dist, strategy='null_value', null_value=0.0, n_points=5, pass_cell_data=True, pass_point_data=True, progress_bar=False)
  # print(demClip2d["z"],np.max(demClip2d["z"]),np.min(demClip2d["z"]))
  # print(profilePoint["z"],np.max(profilePoint["z"]),np.min(profilePoint["z"]))
  profileMesh["z"]=profilePoint["z"]
  profileMesh.save("profileMesh.vtk")
  
  with open('FEM_AGMG_BC1_inf.dat','w',encoding = 'utf-8') as f:
    for i in range(0,len(topMesh['nodeNb'])):
      f.write(str(topMesh['nodeNb'][i]+1)+' '+str(topMesh.points[i][2])+'\n')
    for i in range(0,profileMesh.n_points):
      if (abs(profileMesh['z'][i]-profileMesh.points[i][2])>1e-5):
        Th=density*(profileMesh['z'][i]-profileMesh.points[i][2])+profileMesh.points[i][2]
        f.write(str(profileMesh['nodeNb'][i]+1)+' '+str(Th)+'\n')       

  return 0


def V2writeInputDensity(density,meger3d,megerFace,edzMeshEleNb,faultMeshEleNb,merge3dClipTop,bcProfileFace,Kfracture,poro_fracture,Krock,poro_rock,Ksurface,poro_surface,F1F2K,poro_F1F2K):

    # print("poro_fracture")
    # print(poro_fracture)
    # quit()
    xyz=meger3d.points
    ele_ind=meger3d.cells.reshape(-1,5)
    no_node=len(xyz)
    no_ele=len(ele_ind)
    print('start FEM_AGMG_HK')
    with open('FEM_AGMG_HK.dat','w',encoding = 'utf-8') as f:
        for i in range(0,no_ele):
            region=meger3d['region'][i]
            if (region==1):
                f.write(str(Krock)+' '+str(Krock)+' '+str(Krock)+' '+str(poro_rock)+'\n')
            elif (region==2):
                f.write(str(Ksurface)+' '+str(Ksurface)+' '+str(Ksurface)+' '+str(poro_surface)+'\n')
            elif (region==3 or region==4 or region==5 or region==6):
                f.write(str(F1F2K)+' '+str(F1F2K)+' '+str(F1F2K)+' '+str(poro_F1F2K)+'\n')
            else:
                f.write(str(Krock)+' '+str(Krock)+' '+str(Krock)+' '+str(poro_rock)+'\n')
        f.close()
    # quit()        

    '''
    # print MESH
    '''
    print('start FEM_AGMG_MESH')
    with open('FEM_AGMG_MESH.dat','w',encoding = 'utf-8') as f:
        f.write(str(no_node)+' '+str(no_ele)+'\n')
        for i in range(0,no_node):
            f.write(str(xyz[i][0])+' '+str(xyz[i][1])+' '+str(xyz[i][2])+'\n')
        for i in range(0,no_ele):
            f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+' '+str(ele_ind[i][4]+1)+'\n')
        f.close()




    print('start FEM_fracture_ele_HK')
    profileRegion=np.max(megerFace['region'])
    nodeNB=np.linspace(0,len(megerFace.points),num=len(megerFace.points),endpoint=False,dtype=np.int32)
    megerFace['nodeNB']=nodeNB

    def extract_node(mesh,key,node):
        idx = mesh[key] == node
        return idx
    
    idx=extract_node(megerFace,'region',profileRegion)
    dfnFace=megerFace.extract_cells(idx)    
    plotter = pv.Plotter(off_screen=True)
    plotter.add_mesh(dfnFace, style='surface',cmap='jet', smooth_shading=True,show_scalar_bar=True,show_edges=False)   
    plotter.show(screenshot='dfnFace.png')  
    # quit()   

    dfn_ele_ind=dfnFace.cells.reshape(-1,4)
    print("dfn_ele_ind[0]")
    print(dfn_ele_ind[0])
    no_fracture=len(dfn_ele_ind)
    no_fracture_ele=no_fracture
    no_face_type=6
    with open('FEM_fracture_ele_HK.dat','w',encoding = 'utf-8') as f:
        f.write(str(no_fracture)+' '+str(no_face_type)+'\n')
        for i in range(0,no_fracture):
            f.write(str(i+1)+'\n')
        f.write(str(no_fracture_ele)+'\n')
        for i in range(0,no_fracture_ele):
            e0=dfnFace['nodeNB'][dfn_ele_ind[i][1]]+1
            e1=dfnFace['nodeNB'][dfn_ele_ind[i][2]]+1
            e2=dfnFace['nodeNB'][dfn_ele_ind[i][3]]+1
            f.write(str(e0)+' '+str(e1)+' '+str(e2)+' '+str(Kfracture)+' '+str(i+1)+' '+str(poro_fracture)+'\n')
        f.close()
    # quit()

    print('start FEM_AGMG_BC1_inf')
    print("density=",density)
    ni=0
    with open('FEM_AGMG_BC1_inf.dat','w',encoding = 'utf-8') as f:
        for i in range(0,len(merge3dClipTop['nodeNB'])):
            f.write(str(merge3dClipTop['nodeNB'][i]+1)+' '+str(merge3dClipTop.points[i][2])+'\n')
        for i in range(0,len(bcProfileFace['nodeNB'])):
            if (abs(bcProfileFace['z'][i]-bcProfileFace.points[i][2])>1e-5):
                Th=density*(bcProfileFace['z'][i]-bcProfileFace.points[i][2])+bcProfileFace.points[i][2]
                f.write(str(bcProfileFace['nodeNB'][i]+1)+' '+str(Th)+'\n')            
        f.close()
    with open('FEM_AGMG_BC1_inf_nonDensity.dat','w',encoding = 'utf-8') as f:
        for i in range(0,len(merge3dClipTop['nodeNB'])):
            f.write(str(merge3dClipTop['nodeNB'][i]+1)+' '+str(merge3dClipTop.points[i][2])+'\n')
        for i in range(0,len(bcProfileFace['nodeNB'])):
            if (abs(bcProfileFace['z'][i]-bcProfileFace.points[i][2])>1e-5):
                Th=1.0*(bcProfileFace['z'][i]-bcProfileFace.points[i][2])+bcProfileFace.points[i][2]
                f.write(str(bcProfileFace['nodeNB'][i]+1)+' '+str(Th)+'\n')
                # ni=ni+1
                # if (ni>5): quit()            
        f.close()        
    # quit()

    return 0

def writeHybridFlowIn(meshRefine_TETRA,meshRefine_triangle,topBcNodeNb,botBcNodeNb,addRegion,regionTag,Krock,poro_rock,poro_fracture):
  '''
  # 設定邊界node定水頭
  '''
  # topBcNodeNb=[]
  # botBcNodeNb=[]
  print('start FEM_AGMG_BC1_inf')
  # print("density=",density)
  ni=0
  # surfaceElevation=0.0
  minZ=np.min(meshRefine_TETRA.points,axis=0)[2]
  print("minZ=",minZ)
  with open('FEM_AGMG_BC1_inf.dat','w',encoding = 'utf-8') as f:
    for i in range(0,len(topBcNodeNb)):
      f.write(str(topBcNodeNb[i]+1)+' '+str(meshRefine_TETRA.points[topBcNodeNb[i]][2]-minZ)+'\n')
    for i in range(0,len(botBcNodeNb)):
      f.write(str(botBcNodeNb[i]+1)+' '+str(meshRefine_TETRA.points[botBcNodeNb[i]][2]-minZ)+'\n')      
    f.close()

  print('start FEM_AGMG_IC')
  with open('FEM_AGMG_IC.dat','w',encoding = 'utf-8') as f:
    for i in range(0,meshRefine_TETRA.n_points):
      f.write(str(i+1)+' '+str(meshRefine_TETRA.points[i][2]-minZ)+'\n')     
    f.close()

  '''
  # 搜尋dfn三角形編號
  '''
  # meshRefine_triangle['face_markers']
  partMesh=copy.deepcopy(meshRefine_triangle['face_markers']>=addRegion)
  meshRefinFracCellsNb=np.unique(np.array(list(zip(*np.where(partMesh==True)))).tolist())
  # print('meshRefinFracCellsNb=',meshRefinFracCellsNb)
  unqiueFrac=np.unique(meshRefine_triangle['face_markers'][partMesh])-addRegion
  # print("unqiueFrac=",unqiueFrac)
  no_unqiueFrac=len(unqiueFrac)
  maxFracNb=np.max(unqiueFrac)
  FracMaplinspace=np.zeros(maxFracNb+1,dtype=np.int32)
  # print("FracMaplinspace=",FracMaplinspace)
  for i in range(0,len(unqiueFrac)):
    FracMaplinspace[unqiueFrac[i]]=1
  nb=0
  for i in range(0,len(FracMaplinspace)):
    if (FracMaplinspace[i]==1):
      nb=nb+1
      FracMaplinspace[i]=nb
    # print(i,FracMaplinspace[i])
  # quit()

  folder='/data/CGAL/CGAL_LEE/HybridTW/000_fabToMesh/FABRes'
  FABResFileCheck = Path(folder+r'/no_frac.out')
  if FABResFileCheck.is_file():
    file=folder+'/frac_K_ap.bin'
    flowResFileCheck = Path(file)
    if (flowResFileCheck.is_file):
      f = FortranFile(file, 'r')
      frac_K = f.read_reals(dtype=np.float64)
      frac_ap = f.read_reals(dtype=np.float64)
  # print("frac_K=",frac_K)
  # print("len(frac_K)=",len(frac_K))

  print("start FEM_fracture_ele_HK.dat")
  faces=meshRefine_triangle.cells.reshape(-1,4)
  no_face_type=6
  with open('FEM_fracture_ele_HK.dat','w',encoding = 'utf-8') as f:
    f.write(str(no_unqiueFrac)+' '+str(no_face_type)+'\n')
    for i in range(0,no_unqiueFrac):
      # f.write(str(unqiueFrac[i]+1)+'\n')
      f.write(str(FracMaplinspace[i])+'\n')
    f.write(str(len(meshRefinFracCellsNb))+'\n')
    for i in range(0,len(meshRefinFracCellsNb)):
      e0=faces[meshRefinFracCellsNb[i]][1]+1
      e1=faces[meshRefinFracCellsNb[i]][2]+1
      e2=faces[meshRefinFracCellsNb[i]][3]+1
      # print("meshRefinFracCellsNb[i]=",meshRefinFracCellsNb[i])
      fracID=FracMaplinspace[meshRefine_triangle['face_markers'][meshRefinFracCellsNb[i]]-addRegion]
      f.write(str(e0)+' '+str(e1)+' '+str(e2)+' '+str(frac_K[fracID])+' '+str(fracID)+' '+str(poro_fracture)+'\n')
      ''' #fracID 從1開始,連續編號'''
    f.close()


  '''
  # 搜尋每個網格屬於那個區域,排除dfn
  '''   
  Hk=np.zeros(meshRefine_TETRA.n_cells,dtype=np.float64)
  porosity=np.zeros(meshRefine_TETRA.n_cells,dtype=np.float64)
  meshRefine_elementNbByRegion=[]
  for i in range(0,addRegion):      
    partMesh=meshRefine_TETRA['element_attributes']==regionTag[i]
    ind=list(zip(*np.where(partMesh==True)))
    meshRefine_elementNbByRegion.append(np.unique(np.array(ind)).tolist())
    for j in range(0,len(meshRefine_elementNbByRegion[i])):
      Hk[meshRefine_elementNbByRegion[i][j]]=Krock[i]
      porosity[meshRefine_elementNbByRegion[i][j]]=poro_rock[i]



  '''
  # 產生複合域水流輸入檔
  '''
  print('start FEM_AGMG_HK')
  with open('FEM_AGMG_HK.dat','w',encoding = 'utf-8') as f:
    for i in range(0,meshRefine_TETRA.n_cells):
      f.write(str(Hk[i])+' '+str(Hk[i])+' '+str(Hk[i])+' '+str(porosity[i])+'\n')
    f.close()

  '''
  # print MESH
  '''
  print('start FEM_AGMG_MESH')
  ele_ind=meshRefine_TETRA.cells.reshape(-1,5)
  with open('FEM_AGMG_MESH.dat','w',encoding = 'utf-8') as f:
    f.write(str(meshRefine_TETRA.n_points)+' '+str(meshRefine_TETRA.n_cells)+'\n')
    for i in range(0,meshRefine_TETRA.n_points):
      f.write(str(meshRefine_TETRA.points[i][0])+' '+str(meshRefine_TETRA.points[i][1])+' '+str(meshRefine_TETRA.points[i][2])+'\n')
    for i in range(0,meshRefine_TETRA.n_cells):
      f.write(str(ele_ind[i][1]+1)+' '+str(ele_ind[i][2]+1)+' '+str(ele_ind[i][3]+1)+' '+str(ele_ind[i][4]+1)+'\n')
    f.close()
    
    