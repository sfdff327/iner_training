import numpy as np
import pyvista as pv
import json
import matplotlib.pyplot as plt
import pandas as pd
import copy

def profileBcNodeInterpolate(bcProfileFace,Grid,key,radius):
  points=np.zeros((len(bcProfileFace.points),3),dtype=np.float64)
  for i in range(0,len(bcProfileFace.points)):
      points[i]=[bcProfileFace.points[i][0],bcProfileFace.points[i][1],0.0]
  # face=bcProfileFace.faces
  
  poly=pv.PolyData(points)
  # polyInterpolated = poly.interpolate(Grid)
  polyInterpolated = poly.interpolate(Grid, radius=radius)
  # print(polyInterpolated[key])

  return polyInterpolated[key]

def streamlinesToList(streamlinesPolyVtk):
  vertices=streamlinesPolyVtk.points
  lines=streamlinesPolyVtk.lines
  nb=0
  # print("===================")
  # print(len(lines))
  # for i in range(0,len(lines)):
  #   print(i,',',lines[i])
  # quit()
  lineNb={}
  cal=-1
  while(nb<len(lines)):
    # print(nb,lines[nb])    
    # print('start',nb+1,streamlinesPolyVtk.points[nb+1])
    # print('end',nb-1,streamlinesPolyVtk.points[lines[nb+lines[nb]]])
    cal=cal+1
    lineNb[str(cal)]=lines[nb+1:nb+lines[nb]]
    nb=nb+lines[nb]+1
    

    
    # print(lineNb)
    # if nb>5000: 
    #   print(len(lineNb))
    #   print(lineNb['0'][0:10])
    #   print(lineNb['1'][0:10])
    #   print(lineNb['2'][0:10])
    #   quit()
    

  # print(lines[0]+1,lines[lines[0]+1])
  # print(lines[0]+1+lines[lines[0]+1]+1,lines[lines[0]+1+lines[lines[0]+1]+1])

  # print("===================")
  return vertices,lineNb

def ptTravelTime(streamlinesPolyVtk,vertices,lineNbs,innerMegered,poro_fracture,poro_rock,Qnb):

  ptTime={}
  ptDist={}
  for lineNb in lineNbs:
    # print(lineNbs[lineNb])    
    time=[]
    distance=[]
    timeAll=0.0
    distAll=0.0
    for i in range(0,len(lineNbs[lineNb])):
      start = streamlinesPolyVtk.points[i]
      stop = streamlinesPolyVtk.points[i+1]
      dist = np.linalg.norm(start-stop)
      distAll=distAll+dist
      distance.append(distAll)
      # print('start=',start)
      # print('stop=',stop)
      # print('dist=',dist)
      # quit()
      # print(">>>>>>>>>>>>>")
      points, ind = innerMegered.ray_trace(start, stop) 
        
      if len(points)>0:
        # print("==============")
        # print("==============")
        # print('start=',start)
        # print('stop=',stop)
        # print('points=',points)
        # print('ind=',ind)
        # print("innerMegered['labels'][ind]=",innerMegered['labels'][ind])
        # print("innerMegered['containing_cell'][ind]=",innerMegered['containing_cell'][ind])
        # print(streamlinesPolyVtk['velocityLen'][i])
        # print(streamlinesPolyVtk['velocityLen'][i+1])        
        '''
        # labels ==1 代表裂隙, containing_cell代表裂隙編號 
        # mask01 = innerMegered['containing_cell'] !=-1
        # mask02 = innerMegered['labels'] ==1 
        '''
        # quit()
        poro=poro_fracture
      else:
        # print(streamlinesPolyVtk['velocityLen'][i])
        # print(streamlinesPolyVtk['velocityLen'][i+1])
        poro= poro_rock 
      q=streamlinesPolyVtk['velocityLen'][i]/poro
      timeAll=timeAll+dist/q/86400.0/365.0
      time.append(timeAll)
    ptTime[lineNb]=time
    ptDist[lineNb]=distance
  with open(Qnb+"ptTime.json", "w") as outfile:
    json.dump(ptTime, outfile)
  with open(Qnb+"ptDist.json", "w") as outfile:
    json.dump(ptDist, outfile)  



  return ptTime,ptDist

def ptCDF(outBound,streamlines,ptTime,ptDist,Qnb):
  vertices,lineNb=streamlinesToList(streamlines)
  ptY=[]
  ptD=[]
  ptX=[]

  deadEnd=[]
  cal=0  
  for key in ptTime:
    # print(lineNb[key])
    nb=lineNb[key][len(lineNb[key])-1]
    # print('len,nb=',len(lineNb[key]),nb)
    # print(lineNb[key])
    # print(lineNb[key][nb])
    # quit()
    if (
      # vertices[nb][0]>=outBound[0][0] and 
      # vertices[nb][0]<=outBound[0][1] and
      # vertices[nb][1]>=outBound[1][0] and 
      # vertices[nb][1]<=outBound[1][1] and
      vertices[nb][2]>=outBound[2][0] and 
      vertices[nb][2]<=outBound[2][1] ):
      # print(ptTime[key])
      # print(ptDist[key])
      maxT=max(ptTime[key])
      maxD=max(ptDist[key])
      # print('maxT=',maxT)
      # print('maxD=',maxD)
      # print(maxT/maxD)
      ptX.append(np.float64(maxT/maxD))
      ptY.append(np.float64(maxT))
      ptD.append(np.float64(maxD))
      # quit()
      # print(vertices[nb],'in')
    else:
      cal=cal+1
      # print(vertices[nb],'not in')
      deadEnd.append(key)
    # quit()
  # print('not in',cal)
    
  '''
  colNB CDF切割個數
  '''
  colNB=16
  npPT=np.zeros((colNB,6),dtype=np.float64)
  

  ptX=np.array(ptX)
  hist,bin_edges = np.histogram(ptX,bins = colNB)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  npPT[:,0]=bin_edges[1:]
  npPT[:,1]=cdf[:]
  # plt.plot(bin_edges[1:],cdf,marker="^",color='#ED7D31',label="Q1")
  plt.plot(bin_edges[1:],cdf,color='#ED7D31',label=Qnb)
  plt.xlabel("F(year/m)")
  plt.ylabel("Fraction(-)")
  plt.title("CDF for discrete distribution")
  plt.legend()
  plt.savefig(Qnb+'_ptYm.png')
  plt.close()
  ptY=np.array(ptY)
  hist,bin_edges = np.histogram(ptY,bins = colNB)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  npPT[:,2]=bin_edges[1:]
  npPT[:,3]=cdf[:]
  # plt.plot(bin_edges[1:],cdf,marker="^",color='#ED7D31',label="Q1")
  plt.plot(bin_edges[1:],cdf,color='#ED7D31',label=Qnb)
  plt.xlabel("F(year)")
  plt.ylabel("Fraction(-)")
  plt.title("CDF for discrete distribution")
  plt.legend()
  plt.savefig(Qnb+'_ptY.png')
  plt.close()
  ptD=np.array(ptD)
  hist,bin_edges = np.histogram(ptD,bins = colNB)
  cdf = np.cumsum(hist/sum(hist),dtype=np.float64)
  npPT[:,4]=bin_edges[1:]
  npPT[:,5]=cdf[:]  
  # plt.plot(bin_edges[1:],cdf,marker="^",color='#ED7D31',label="Q1")
  plt.plot(bin_edges[1:],cdf,color='#ED7D31',label=Qnb)
  plt.xlabel("F(m)")
  plt.ylabel("Fraction(-)")
  plt.title("CDF for discrete distribution")
  plt.legend()
  plt.savefig(Qnb+'_ptD.png')
  plt.close()

  # df = pd.DataFrame(npPT, columns = ['ym','ymCDF','y','yCDF','m','mCDF'])
  # df.to_csv(Qnb+'.csv', index=False)
  
  # plotdeadEnd=Qnb+'pathDeadEnd.dat'
  # plotpath=Qnb+'path.dat'
  # fe = open(plotdeadEnd, "w")
  # fp = open(plotpath, "w")
  # for key in ptTime:
  #   if (key in deadEnd):
  #     fe.write('Title="XY2D_plot"\n')
  #     fe.write('Variables="x(m)","y(m)","z(m)","time(year)","distance(m)"\n')
  #     fe.write('Zone T="'+str(key)+'",i='+str(len(lineNb[key]))+',F=point\n') 
  #     for i in range(0,len(lineNb[key])):          
  #       fe.write(str(vertices[lineNb[key][i]][0])+' '+str(vertices[lineNb[key][i]][1])+' '+str(vertices[lineNb[key][i]][2])+' '+str(ptTime[key][i])+' '+str(ptDist[key][i])+'\n')
  #   else:
  #     fp.write('Title="XY2D_plot"\n')
  #     fp.write('Variables="x(m)","y(m)","z(m)","time(year)","distance(m)"\n')
  #     fp.write('Zone T="'+str(key)+'",i='+str(len(lineNb[key]))+',F=point\n') 
  #     for i in range(0,len(lineNb[key])):          
  #       fp.write(str(vertices[lineNb[key][i]][0])+' '+str(vertices[lineNb[key][i]][1])+' '+str(vertices[lineNb[key][i]][2])+' '+str(ptTime[key][i])+' '+str(ptDist[key][i])+'\n')      
  # fe.close()
  # fp.close()

  return npPT,vertices,lineNb,ptTime,ptDist,deadEnd

def plotAllCDF(Q1numpyPT,Q2numpyPT,Q3numpyPT):
  fig, axes = plt.subplots(3,figsize=(8,8))
  axes[0].plot(Q1numpyPT[:,0],Q1numpyPT[:,1],color='blue',linestyle='-',label='Q1')
  axes[0].plot(Q2numpyPT[:,0],Q2numpyPT[:,1],color='green',linestyle='-.',label='Q2')
  axes[0].plot(Q3numpyPT[:,0],Q3numpyPT[:,1],color='red',linestyle='-.',label='Q3')  
  axes[0].set_xlabel("year/m")
  axes[0].set_ylabel("Fraction(-)")
  axes[0].set_title("CDF for discrete distribution")

  axes[1].plot(Q1numpyPT[:,2],Q1numpyPT[:,3],color='blue',linestyle='-',label='Q1')
  axes[1].plot(Q2numpyPT[:,2],Q2numpyPT[:,3],color='green',linestyle='-.',label='Q2')
  axes[1].plot(Q3numpyPT[:,2],Q3numpyPT[:,3],color='red',linestyle='-.',label='Q3')
  axes[1].set_xlabel("year")
  axes[1].set_ylabel("Fraction(-)")

  axes[2].plot(Q1numpyPT[:,4],Q1numpyPT[:,5],color='blue',linestyle='-',label='Q1')
  axes[2].plot(Q2numpyPT[:,4],Q2numpyPT[:,5],color='green',linestyle='-.',label='Q2')
  axes[2].plot(Q3numpyPT[:,4],Q3numpyPT[:,5],color='red',linestyle='-.',label='Q3')
  axes[2].set_xlabel("m")
  axes[2].set_ylabel("Fraction(-)")
  axes[2].legend() 
  # axes[2].text(0.1, 0.65,'(c)', ha='center', va='center',transform=plt.gcf().transFigure)
  plt.gcf().text(0.015, 0.95, '(a)', fontsize=12)
  plt.gcf().text(0.015, 0.63, '(b)', fontsize=12)
  plt.gcf().text(0.015, 0.32, '(c)', fontsize=12)

  fig.tight_layout()
  plt.savefig('QptYM.png')
  plt.close()
  print("plotAllCDF OK")

def plotTecplotByCDF(nyPT,vertices,lineNb,ptTime,ptDist,deadEnd,Qnb,parts):
  ym=np.zeros(len(parts),dtype=np.float64)
  for j in range(len(parts)):
    for i in range(0,len(nyPT)):
      if (nyPT[i,1]<=parts[j]):
        if (ym[j]<nyPT[i,0]):
          ym[j]=nyPT[i,0]
  # print('nyPT=')
  # print(nyPT)
  # print('ym=',ym)

  plotdeadEnd=Qnb+'pathDeadEnd.dat'
  plotpath= []
  fp=[]
  for i in range(0,int(len(ym)+1)):
    plotpath.append(Qnb+'_'+str(i)+'_path.dat')
    fp.append(open(plotpath[i], "w"))
    # print('set fp')
  fe = open(plotdeadEnd, "w")
  # fp = open(plotpath, "w")

  for key in ptTime:
    maxT=max(ptTime[key])
    maxD=max(ptDist[key])
    maxTD = np.float64(maxT/maxD)
    if (key in deadEnd):
      fe.write('Title="XY2D_plot"\n')
      fe.write('Variables="x(m)","y(m)","z(m)","time(year)","distance(m)"\n')
      fe.write('Zone T="'+str(key)+'",i='+str(len(lineNb[key]))+',F=point\n') 
      for i in range(0,len(lineNb[key])):          
        fe.write(str(vertices[lineNb[key][i]][0])+' '+str(vertices[lineNb[key][i]][1])+' '+str(vertices[lineNb[key][i]][2])+' '+str(ptTime[key][i])+' '+str(ptDist[key][i])+'\n')
    else:
      for i in range(0,int(len(ym)+1)):      
        if i==0 :
          # print('0','maxTD=',maxTD,'ym[i]=',ym[i])
          if maxTD<=ym[i] :
            writeNb=i
            break
        elif (i==int(len(ym))):
          # print('2','maxTD=',maxTD)
          if ((maxTD>ym[i-1])):
            writeNb=i
            break            
        elif (i>0 and len(ym)>1 and i<int(len(ym))):
          # print('1','maxTD=',maxTD,'ym[i]=',ym[i])
          if ((maxTD>ym[i-1]) and (maxTD<=ym[i])):
            writeNb=i
            break

      fp[writeNb].write('Title="XY2D_plot"\n')
      fp[writeNb].write('Variables="x(m)","y(m)","z(m)","time(year)","distance(m)"\n')
      fp[writeNb].write('Zone T="'+str(key)+'",i='+str(len(lineNb[key]))+',F=point\n') 
      for i in range(0,len(lineNb[key])):          
        fp[writeNb].write(str(vertices[lineNb[key][i]][0])+' '+str(vertices[lineNb[key][i]][1])+' '+str(vertices[lineNb[key][i]][2])+' '+str(ptTime[key][i])+' '+str(ptDist[key][i])+'\n')      
  fe.close()
  for i in range(0,int(len(ym)+1)):
    fp[i].close()  