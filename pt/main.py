import numpy as np
import pandas as pd
import time
import dem
import pyvista as pv
import plotPyvista

import os
import pyFabSTL
import hybridFlow


def createSmallDomain(domainCenter,dx,dy,dz):
  xyz=np.zeros((26,3),dtype=np.float64)
  xyz[0]=domainCenter-np.array([dx/2,dy/2,dz/2])
  
  xyz[1]=xyz[0]+np.array([dx,0.,0.])
  xyz[2]=xyz[0]+np.array([dx,dy,0.])
  xyz[3]=xyz[0]+np.array([0.,dy,0.])
  xyz[4]=xyz[0]+np.array([0.,0.,dz])
  xyz[5]=xyz[1]+np.array([0.,0.,dz])
  xyz[6]=xyz[2]+np.array([0.,0.,dz])
  xyz[7]=xyz[3]+np.array([0.,0.,dz])

  xyz[8] =xyz[0]+np.array([dx/2,0.,0.])
  xyz[9] =xyz[0]+np.array([dx,dy/2,0.])
  xyz[10]=xyz[0]+np.array([dx/2,dy,0.])
  xyz[11]=xyz[0]+np.array([0.,dy/2,0.])

  xyz[12]=xyz[4]+np.array([dx/2,0.,0.])
  xyz[13]=xyz[4]+np.array([dx,dy/2,0.])
  xyz[14]=xyz[4]+np.array([dx/2,dy,0.])
  xyz[15]=xyz[4]+np.array([0.,dy/2,0.])

  xyz[16]=xyz[0]+np.array([0.,0.,250.])
  xyz[17]=xyz[16]+np.array([dx/2.,0.,0.])
  xyz[18]=xyz[16]+np.array([dx,0.,0.])
  xyz[19]=xyz[16]+np.array([dx,dy/2,0.])
  xyz[20]=xyz[16]+np.array([dx,dy,0.])
  xyz[21]=xyz[16]+np.array([dx/2.,dy,0.])
  xyz[22]=xyz[16]+np.array([0.,dy,0.])
  xyz[23]=xyz[16]+np.array([0.,dy/2,0.])

  xyz[24]=xyz[0]+np.array([dx/2.,dy/2,0.])
  xyz[25]=xyz[4]+np.array([dx/2.,dy/2,0.])

  faces=np.hstack([
[4, 0,11,24,8],[4,11,3,10,24],[4,8,24,9,1],[4, 24,10,2,9],[4,4,15,23,16],[4,16,23,11,0],
[4,15,7,22,23],[4,23,22,3,11],[4,5,12,17,18],[4, 18,17,8,1],[4,12,4,16,17],[4, 17,16,0,8],
[4,6,13,19,20],[4, 20,19,9,2],[4,13,5,18,19],[4, 19,18,1,9],[4,7,14,21,22],[4,22,21,10,3],
[4,14,6,20,21],[4,21,20,2,10],[4,4,12,25,15],[4,15,25,14,7],[4,12,5,13,25],[4,25,13,6,14],])
  smallDomain=pv.PolyData(xyz,faces)

  ''' 利用等差數列函式, 創造面的編號'''
  nb=-np.linspace(0,24,num=24,endpoint=False,dtype=np.int64)

  ''' 將每個面的編號存入vtk檔案'''
  smallDomain['faceNb']=nb

  smallDomain.save('smallDomain.vtk')
  # print(smallDomain.points)
  # print(smallDomain.faces)
  
  return smallDomain


if __name__ == "__main__":
  start = time.time()
  
  OriginPointsF=r'../../INER_DATA/Domain2D/OriginPoints.txt'
  # path=r'../../HybridTW/000_fabToMesh/testWriteMeshALL.poly'
  # q123Folder=r'../../HybridTW/001_stlToMesh'
  # FABResfolder=r'../../HybridTW/000_fabToMesh/FABRes'

  EpsgIn=3825
  EpsgOut=4326

  '''
  # 模擬底部高程
  '''
  STLminZ=-2000.0

  '''
  # 表土層深度
  '''
  depthSurfaceSoil=-70.0  

  # OriginPointsF=r'/root/ihsienlee/CGAL/CGAL_LEE/INER_DATA/Domain2D/OriginPoints.txt'
  orPointCsv = pd.read_csv(OriginPointsF,sep='\\s+')
  orPointCsv = orPointCsv.fillna('')
  orPointCsv.columns=["x", "y"]
  delta_x=np.float64(orPointCsv['x'].values[0])
  delta_y=np.float64(orPointCsv['y'].values[0])
  # print(orPointCsv)
  print("delta_x,delta_y=",delta_x,delta_y)
  # quit()  

  CsvFile=r'../../INER_DATA/Domain2D/DEM_120m_resolution500m_201x_139y.dat'
  domainFile=r'../../INER_DATA/Domain2D/domainboundary_large.xy'
  q123Folder=r'../001_stlToMesh'
  FABResfolder=r'./'

  nx=201
  ny=139
  # CsvFile=r'../../INER_DATA/Domain2D/DTM_largescale_263x_263y.dat'
  # nx=263
  # ny=263  

  case=0
  if (case==1):
    '''
    # 讀取500m DEM
    '''    
    xyzCsv = pd.read_csv(CsvFile,sep='\\s+',header=None)
    xyzCsv = xyzCsv.fillna('')
    xyzCsv.columns=["x", "y", "z"]   
    # print(xyzCsv) 
    # quit()
    '''csv points to vtk'''
    points=np.zeros((len(xyzCsv),3),dtype=np.float64)
    points[:,0]=xyzCsv['x'].values[:]
    points[:,1]=xyzCsv['y'].values[:]
    xyzPoints = pv.PolyData(points)
    xyzPoints.save("xyzPoints.vtk")


    ''' # 讀取模擬範圍,並且依據dem計算表面高程 '''    
    xyzDomain = pd.read_csv(domainFile,sep='\\s+',header=None)
    xyzDomain = xyzDomain.fillna('')
    xyzDomain.columns=["x", "y"]
    

    domain2d,demClip2d,largeDem= dem.demToMesh(nx,ny,xyzCsv,xyzDomain)
    print("read dem and doamin files")
    domain2d,demClip2d,largeDem=plotPyvista.dem2dClip(delta_x,delta_y,EpsgIn,EpsgOut,domain2d,demClip2d,largeDem)
  # quit()
  domain2d=pv.read('domain2d.vtk')
  demClip2d=pv.read('demClip2d.vtk')
  largeDem=pv.read('largeDem.vtk') 
  # print("largeDem") 
  # print(largeDem)

  case=0
  if (case==1):   
    STLminZ=np.float64(STLminZ)
    '''
    use demClip2d create 3d dem
    '''
    demClip3dsurf=plotPyvista.dem2dTo3d(demClip2d)
    StlFile=r'../../INER_DATA/STL/GeoModel_SKB_AModify_fault_1.stl'
    fault_1=pv.read(StlFile)
    StlFile=r'../../INER_DATA/STL/GeoModel_SKB_AModify_fracture.stl'
    fault_2=pv.read(StlFile)
    fault_1Mtr=100.0
    fault_2Mtr=20.0
    fault12=plotPyvista.faultStl(demClip2d,delta_x,delta_y,fault_1,fault_2,fault_1Mtr,fault_2Mtr)
    boxMtr=500.0
    poly3d=plotPyvista.mergeOutsideBox(depthSurfaceSoil,demClip3dsurf,STLminZ,boxMtr)    
  else:
    demClip3dsurf=pv.read('demClip3dsurf.vtk')
    poly3d=pv.read('poly3d.vtk')
    fault12=pv.read('fault12.vtk')
  # print(fault12.array_names)
  # print(poly3d.array_names)
  # print('poly3d, demclip3d and fault12')

  case=0
  if (case==1):
    dfndataF=r'dfnData.csv'    
    dfndata = pd.read_csv(dfndataF,sep='\\s+')
    # dfndata.columns=["no_node", "Transmissivity", "Storativity", "aperture"]
    no_node=dfndata['no_node']
    Transmissivity=dfndata['Transmissivity']
    Storativity=dfndata['Storativity']
    aperture=dfndata['aperture']
    fractureK=Transmissivity/aperture
    fracNb=0
    lineNb=0
    len_dfndata=len(no_node)
    dfnmesh = pv.PolyData()
    while(lineNb<len_dfndata):      
      fracNb=fracNb+1
      no_each_frac_node=np.int64(no_node[lineNb])
      fractureK=np.float64(Transmissivity[lineNb])/np.float64(aperture[lineNb])
      fractureAperture=np.float64(aperture[lineNb])
      xyz=np.zeros((no_each_frac_node,3),dtype=np.float64)
      faces=np.linspace(start=-1,stop=no_each_frac_node,num=no_each_frac_node+1,endpoint=False,dtype=np.int64)
      faces[0]=no_each_frac_node
      xyz[:,0]=np.float64(Transmissivity[lineNb+1:lineNb+1+no_each_frac_node])
      xyz[:,1]=np.float64(Storativity[lineNb+1:lineNb+1+no_each_frac_node])
      xyz[:,2]=np.float64(aperture[lineNb+1:lineNb+1+no_each_frac_node])
      lineNb=lineNb+1+no_each_frac_node
      dfnpoly=pv.PolyData(xyz,faces)
      dfnpoly.translate([delta_x,delta_y,0],inplace=True)
      dfnpoly['fractureK']=[fractureK]
      dfnpoly['aperture']=[fractureAperture]
      dfnpoly['No']=[fracNb]
      dfnmesh=dfnmesh+dfnpoly
    dfnmesh.save('dfnPoly.vtk') 
    dfnPoly=dfnmesh
    print('fracNb=',fracNb)
  elif (case==0):
    dfnPoly=pv.read('dfnPoly.vtk') 
  dfnOrUsg=dfnPoly
  # print("dfnPoly",dfnPoly)
  # print("dfnPoly.array_names=",dfnPoly.array_names)

  case=0
  if (case==1):
    noface=np.max(dfnPoly["No"])
    ind=np.zeros(noface,dtype=np.int64)
    fractureK=np.zeros(noface,dtype=np.float64)
    aperture=np.zeros(noface,dtype=np.float64)
    for i in range(0,dfnPoly.n_cells):
      # if (ind[dfnPoly["No"][i]-1]==0):
      #  fractureK[dfnPoly["No"][i]-1]=dfnPoly["fractureK"][i]
      #  aperture[dfnPoly["No"][i]-1]=dfnPoly["aperture"][i]
      if (ind[i]==0):
       fractureK[i]=dfnPoly["fractureK"][i]
       aperture[i]=dfnPoly["aperture"][i]
    np.savez('dfnPolyNp', 
    fractureK=fractureK, 
    aperture=aperture)
    print("save dfnPolyNp OK")  
  dfnPolyNp = np.load('dfnPolyNp.npz')
  print("dfnPolyNp OK")    

  '''網格參數'''
  sp= 50.       # 模擬範圍(處置區外)網格密度
  smallsp=50.   # 處置區周圍網格密度
  dfnsp =20.    # DFN網格密度
  edzsp=20.     # EDZ網格密度
  max_angle=181.0  # max三角形網格角度
  min_area=0.      # min三角形面積

  '''水文參數設定'''
  fracture_alpha_w=1.0 
  poro_fracture=1.5e-2 #F2's poro
  alpha=0.008
  sa=3.2                      # 海水密度參數
  density=1.0*(1.0+alpha*sa)  # 地下水密度

  matrix_alpha_w=4.8
  Ksurface=1.0e-5             # 表土層K
  poro_surface=1.e-3          # 表土層孔隙率
  surface_alpha_w=4.8

  F1F2K=5.0e-6 # 基準案例
  # F1F2K=1.0e-04 # 3號案例 改斷層
  # F1F2K=1.0e-08 # 4號案例 改斷層
  poro_F1F2K=0.4
  F1F2K_alpha_w=1.0

  Krock=[1.0e-10,3.3e-8] # 基準案例'matrix', 'edz'
  # Krock=[4.1e-12,3.3e-8] # 1號案例只有改母岩 'matrix', 'edz'
  # Krock=[1.0e-09,3.3e-8] # 2號案例只有改母岩 'matrix', 'edz'
  poro_rock=[1.0e-5,1.0e-4] # 'matrix', 'edz'

  ''' EDZ厚度'''
  edzThickness=0.3

  ''' 
  # edz重建
  '''
  edz=pv.read(r'edzNew.vtk')

  '''
  # 計算dfn與edz座標範圍
  '''
  dfncoorMax=np.max(dfnPoly.points,axis=0)
  dfncoorMin=np.min(dfnPoly.points,axis=0)
  edzcoorMax=np.max(edz.points,axis=0)
  edzcoorMin=np.min(edz.points,axis=0)

  ''' # 計算處置區範圍'''
  coormax=np.zeros(3,dtype=np.float64)
  coormin=np.zeros(3,dtype=np.float64)
  for i in range(0,3):
    if dfncoorMax[i]>=edzcoorMax[i]:
      coormax[i]=dfncoorMax[i]
    else:
      coormax[i]=edzcoorMax[i]
    if dfncoorMin[i]<=edzcoorMin[i]:
      coormin[i]=dfncoorMin[i]
    else:
      coormin[i]=edzcoorMin[i]

  ''' # 計算處置區xyz方向長度'''
  lencoor=coormax-coormin
  print('lencoor=',lencoor)

  ''' 計算處置區中心座標'''
  domainCenter=(coormax+coormin)/2

  ''' 固定處置區中心z座標=-500'''
  domainCenter[2]=np.float64(-500.0)
  # quit()

  ''' create local small domain vtk'''
  smallDomain=createSmallDomain(
    domainCenter,
    lencoor[0]+250.0,
    lencoor[1]+250.0,
    500)
  # quit()

  ''' 計算模擬邊界內處置設施與dfn等三角形交錯關係'''
  case=0
  if (case==1):    
    innerOBJ=pyFabSTL.mesh_arrangement(
      dfnPoly,
      smallDomain,
      edz,
      smallsp,dfnsp,edzsp,
      max_angle,min_area) 
  else:
    innerOBJ=pv.read("innerOBJ.vtk")

  ''' # 讀取混合域穩態地下水流場'''
  meshTetraRefinementHybridFolw=pv.read("meshTetraRefinementHybridFolw.vtk")
  print(meshTetraRefinementHybridFolw.array_names)
  print(np.unique(meshTetraRefinementHybridFolw['element_attributes']))

  '''
  # 讀取完全截切節點
  # 各節點差值獲得速度，最大速度位置釋放質點
  '''
  case=0
  if (case==1):
    '''# 確認處置設施檔案位置'''
    print("q123Folder=",q123Folder)

    '''
    # 讀取處置設施並計算物件連通性 noConnfrac,noConnDH,noConnedz,noConnDt,noConnMt
    # 讀取不同釋放途徑完全截切關係 dhfrac,dhedz,dtfrac
    '''
    noConnfrac,noConnDH,noConnedz,noConnDt,noConnMt,dhfrac,dhedz,dtfrac,dfnFABQ123,dhQ123,edzQ123,dtQ123,mtQ123=hybridFlow.readConnSTL(q123Folder)

    '''# 依據完全截切紀錄vtk,分析最大流速位置,作為質點釋放位置'''
    Q1PT,Q2PT,Q3PT,\
      Q1FullConnNb,Q2FullConnNb,Q3FullConnNb,\
        Q1EqFlux,Q2EqFlux,Q3EqFlux=\
          hybridFlow.FindMaxFluxNode(\
      dfnOrUsg,q123Folder,FABResfolder,\
        noConnfrac,noConnDH,noConnedz,noConnDt,noConnMt,\
          dhfrac,dhedz,dtfrac,dfnFABQ123,dhQ123,edzQ123,dtQ123,mtQ123,\
            meshTetraRefinementHybridFolw,poro_fracture,edzThickness,poro_rock[1])
    
    Q1PT.save('Q1PT.vtk')
    Q2PT.save('Q2PT.vtk')
    Q3PT.save('Q3PT.vtk')
    np.savez('Q123FullConnEqFlux', 
    Q1FullConnNb=np.array(Q1FullConnNb),
    Q2FullConnNb=np.array(Q2FullConnNb),
    Q3FullConnNb=np.array(Q3FullConnNb),
    Q1EqFlux=np.array(Q1EqFlux),
    Q2EqFlux=np.array(Q2EqFlux),
    Q3EqFlux=np.array(Q3EqFlux))    
  else:
    Q1PT=pv.read('Q1PT.vtk')
    Q2PT=pv.read('Q2PT.vtk')
    Q3PT=pv.read('Q3PT.vtk')
  Q123FullConnEqFlux = np.load('Q123FullConnEqFlux.npz')
  print("read Q123FullConnEqFlux.npz OK")
  # quit()


  case=0
  if (case==1):
    '''
    # 設定質點傳輸重要共通性參數
    '''
    #質點傳輸最大迭代次數
    max_steps=500000 
    #穩態地下水流速最小值，作為停止質點傳輸模擬基準
    terminal_speed=np.min(meshTetraRefinementHybridFolw["velocityLen"])
    print("terminal_speed=",terminal_speed)
    # 初始質點傳輸長度
    initial_step_length=dfnsp/100
    # 最小質點傳輸長度
    min_step_length=dfnsp/2
    # 最大質點傳輸長度
    max_step_length=dfnsp    

    # 將Q1質點釋放點,放入穩態地下水流場,執行質點傳輸
    Q1_streamlines=hybridFlow.streamlines_from_source(\
      meshTetraRefinementHybridFolw,\
        Q1PT,initial_step_length,min_step_length,max_step_length,max_steps,terminal_speed,Krock[0])
    
    # 將質點傳輸模擬結果所有點座標確定格式為浮點數
    Q1_streamlines.points_to_double()
    Q1_streamlines.save('Q1_streamlines.vtk')
    print('streamlines Q1 OK')

    Q2_streamlines=hybridFlow.streamlines_from_source(meshTetraRefinementHybridFolw,Q2PT,initial_step_length,min_step_length,max_step_length,max_steps,terminal_speed,Krock[0])  
    Q2_streamlines.points_to_double()
    Q2_streamlines.save('Q2_streamlines.vtk')
    print('streamlines Q2 OK')
    Q3_streamlines=hybridFlow.streamlines_from_source(meshTetraRefinementHybridFolw,Q3PT,initial_step_length,min_step_length,max_step_length,max_steps,terminal_speed,Krock[0])
    Q3_streamlines.points_to_double()
    Q3_streamlines.save('Q3_streamlines.vtk')
    print('streamlines Q3 OK')
  else:
    Q1_streamlines=pv.read('Q1_streamlines.vtk')
    Q2_streamlines=pv.read('Q2_streamlines.vtk')
    Q3_streamlines=pv.read('Q3_streamlines.vtk') 

  '''
  # 判斷streamline是否在edz
  '''
  case=1
  if (case==1):
    Q1_streamlines_edz=hybridFlow.isPtInSTL(edz,"IsEdz",Q1_streamlines)
    Q2_streamlines_edz=hybridFlow.isPtInSTL(edz,"IsEdz",Q2_streamlines)
    Q3_streamlines_edz=hybridFlow.isPtInSTL(edz,"IsEdz",Q3_streamlines)
    Q1_streamlines_edz.save("Q1_streamlines_edz.vtk")
    Q2_streamlines_edz.save("Q2_streamlines_edz.vtk")
    Q3_streamlines_edz.save("Q3_streamlines_edz.vtk")
  else:
    Q1_streamlines_edz=pv.read("Q1_streamlines_edz.vtk")
    Q2_streamlines_edz=pv.read("Q2_streamlines_edz.vtk")
    Q3_streamlines_edz=pv.read("Q3_streamlines_edz.vtk")
  print("isPtInEdz OK")

  '''
  # 判斷streamline是否在F1F2
  '''
  case=1
  if (case==1):
    Q1_streamlines_edz_fault=hybridFlow.isPtInSTL(fault12,"IsFault",Q1_streamlines_edz)
    Q2_streamlines_edz_fault=hybridFlow.isPtInSTL(fault12,"IsFault",Q2_streamlines_edz)
    Q3_streamlines_edz_fault=hybridFlow.isPtInSTL(fault12,"IsFault",Q3_streamlines_edz)
    Q1_streamlines_edz_fault.save("Q1_streamlines_edz_fault.vtk")
    Q2_streamlines_edz_fault.save("Q2_streamlines_edz_fault.vtk")
    Q3_streamlines_edz_fault.save("Q3_streamlines_edz_fault.vtk")
  else:
    Q1_streamlines_edz_fault=pv.read("Q1_streamlines_edz_fault.vtk")
    Q2_streamlines_edz_fault=pv.read("Q2_streamlines_edz_fault.vtk")
    Q3_streamlines_edz_fault=pv.read("Q3_streamlines_edz_fault.vtk")
  print("isPtInFault12 OK")  

  '''
  # 判斷streamline是否在表土層
  '''
  case=1
  if (case==1):
    #另存表土層vtk檔案
    surfaceBool=meshTetraRefinementHybridFolw['element_attributes']!=-1
    surfaceMesh=meshTetraRefinementHybridFolw.remove_cells(surfaceBool,inplace=False)
    # 利用表土層3D物件,判斷質點是否於其內
    Q1_streamlines_edz_fault_surface=hybridFlow.isPtInSTL(surfaceMesh,"IsSurface",Q1_streamlines_edz_fault)
    Q2_streamlines_edz_fault_surface=hybridFlow.isPtInSTL(surfaceMesh,"IsSurface",Q2_streamlines_edz_fault)
    Q3_streamlines_edz_fault_surface=hybridFlow.isPtInSTL(surfaceMesh,"IsSurface",Q3_streamlines_edz_fault)
    Q1_streamlines_edz_fault_surface.save("Q1_streamlines_edz_fault_surface.vtk")
    Q2_streamlines_edz_fault_surface.save("Q2_streamlines_edz_fault_surface.vtk")
    Q3_streamlines_edz_fault_surface.save("Q3_streamlines_edz_fault_surface.vtk")
  else:
    Q1_streamlines_edz_fault_surface=pv.read("Q1_streamlines_edz_fault_surface.vtk")
    Q2_streamlines_edz_fault_surface=pv.read("Q2_streamlines_edz_fault_surface.vtk")
    Q3_streamlines_edz_fault_surface=pv.read("Q3_streamlines_edz_fault_surface.vtk")
  print("isPtInSurface OK") 

  '''
  # 判斷streamline是否在dfn
  '''
  case=1
  if (case==1):
    Q1_streamlines_edz_fault_surface_dfn=hybridFlow.isPtInDFN(dfnOrUsg,Q1_streamlines_edz_fault_surface)
    Q2_streamlines_edz_fault_surface_dfn=hybridFlow.isPtInDFN(dfnOrUsg,Q2_streamlines_edz_fault_surface)
    Q3_streamlines_edz_fault_surface_dfn=hybridFlow.isPtInDFN(dfnOrUsg,Q3_streamlines_edz_fault_surface)
    Q1_streamlines_edz_fault_surface_dfn.save("Q1_streamlines_edz_fault_surface_dfn.vtk")
    Q2_streamlines_edz_fault_surface_dfn.save("Q2_streamlines_edz_fault_surface_dfn.vtk")
    Q3_streamlines_edz_fault_surface_dfn.save("Q3_streamlines_edz_fault_surface_dfn.vtk")
  else:
    Q1_streamlines_edz_fault_surface_dfn=pv.read("Q1_streamlines_edz_fault_surface_dfn.vtk")
    Q2_streamlines_edz_fault_surface_dfn=pv.read("Q2_streamlines_edz_fault_surface_dfn.vtk")
    Q3_streamlines_edz_fault_surface_dfn=pv.read("Q3_streamlines_edz_fault_surface_dfn.vtk")
  print("isPtInDFN OK")

  '''
  # 計算功能測度值
  '''  
  case=1
  if (case==1):  
    Q1PerformaceMeasurement=hybridFlow.performanceMeasurement(\
      dfnOrUsg,Q1_streamlines_edz_fault_surface_dfn,dfnsp,\
      poro_fracture,poro_surface,poro_rock,poro_F1F2K,\
      fracture_alpha_w,F1F2K_alpha_w,surface_alpha_w,matrix_alpha_w)
    Q1PerformaceMeasurement.save("Q1PerformaceMeasurement.vtk")
  else:
    Q1PerformaceMeasurement=pv.read("Q1PerformaceMeasurement.vtk")
  print("Q1PerformaceMeasurement OK")

  case=1
  if (case==1):  
    Q2PerformaceMeasurement=hybridFlow.performanceMeasurement(dfnOrUsg,Q2_streamlines_edz_fault_surface_dfn,dfnsp,poro_fracture,poro_surface,poro_rock,poro_F1F2K,fracture_alpha_w,F1F2K_alpha_w,surface_alpha_w,matrix_alpha_w)
    Q2PerformaceMeasurement.save("Q2PerformaceMeasurement.vtk")
  else:
    Q2PerformaceMeasurement=pv.read("Q2PerformaceMeasurement.vtk")
  print("Q2PerformaceMeasurement OK")

  case=1
  if (case==1):
    Q3PerformaceMeasurement=hybridFlow.performanceMeasurement(dfnOrUsg,Q3_streamlines_edz_fault_surface_dfn,dfnsp,poro_fracture,poro_surface,poro_rock,poro_F1F2K,fracture_alpha_w,F1F2K_alpha_w,surface_alpha_w,matrix_alpha_w)
    Q3PerformaceMeasurement.save("Q3PerformaceMeasurement.vtk")
  else:    
    Q3PerformaceMeasurement=pv.read("Q3PerformaceMeasurement.vtk")
  print("Q3PerformaceMeasurement OK")




  end = time.time()


  # 輸出結果
  print("執行時間：%f 秒" % (end - start))
  