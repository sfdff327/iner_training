import copy
import pyvista as pv
# from PVGeo.grids import EsriGridReader
import numpy as np
import vtk
import matplotlib.pyplot as plt
from  gltflib  import (
     GLTF , GLTFModel , Asset , Scene , Node , Mesh , Primitive , Attributes , Buffer , BufferView , Accessor , AccessorType ,
     BufferTarget , ComponentType , GLBResource , FileResource )
# from xvfbwrapper import Xvfb
# display = Xvfb(width=1920, height=1080)
# display.start()

def calculateQuad(j,i,nrow,ncol,cells):
  c0=(j  )*ncol+i
  c1=(j  )*ncol+i+1
  c2=(j+1)*ncol+i+1
  c3=(j+1)*ncol+i
  cells.append([4,c0,c1,c2,c3])
  return cells

def demToMesh(nx,ny,demXYZ,domain):

  # 以下為產生dem points資料
  Csvcoords=[]
  nb=-1  #代表demXYZ數值矩陣的順序，起點編號等於0
  '''
  # 測試pandas dataframe.values
  print("demXYZ['x'].values[0]=",demXYZ['x'].values[0])
  print("demXYZ['x']=",demXYZ['x'])
  quit()
  '''
  for j in range(0,ny):
    for i in range(0, nx):
      nb=nb+1
      Csvcoords.append(np.array([demXYZ['x'].values[nb],demXYZ['y'].values[nb],0.0],dtype=np.float64))
  Csvcoords=np.array(Csvcoords)
  
  # 以下為產生dem cells資料
  cells=[]
  for j in range(0,ny-1):
    for i in range(0, nx-1):
      cells=calculateQuad(j,i,ny,nx,cells) #產生每個網格順序
  no_cells=len(cells)
  cells=np.array(cells).ravel()
  celltypes = np.empty(no_cells, dtype=np.uint32)
  celltypes[:] = vtk.VTK_QUAD
  grid = pv.UnstructuredGrid(cells, celltypes, Csvcoords)  

  z=demXYZ['z'].values
  grid['z']=z
  # edges = grid.extract_feature_edges()
  # print(edges)
  print('dem OK')
  # grid.save('dem.vtk')    
  # quit()

  Domaincoords=[]
  domainz=[]
  cells=[]
  cells.append(len(domain))
  domain[['x', 'y']].values
  for i in range(0, len(domain)):
    Domaincoords.append(np.array([domain['x'].values[i],domain['y'].values[i],0.0],dtype=np.float64))
    domainz.append(np.float64(0.0))
    cells.append(i)
  print("cells=",cells)
  surf = pv.PolyData(Domaincoords,cells)
  surf['z']=domainz
  surf.save('domain.vtk')
  print('surf OK')

  clipGrid=grid.clip_surface(surf,invert=False,compute_distance = True)
  # clipGrid=surf.clip_surface(grid,invert=False,compute_distance = True)
  # print(clipGrid.array_names)
  clipGrid.save('clipDtm.vtk')
  # quit()

  return surf,clipGrid,grid