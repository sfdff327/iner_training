import json
import geojson
#set read Geojson precision = 15
geojson.geometry.Geometry.__init__.__defaults__ = (None, False, 15) 
#set read Geojson precision = 15
import io
# from chardet import detect
import pyproj
from pyproj import Transformer, transform
from geojson_rewind import rewind
import numpy as np
# from pysimgeo.projection import projection
# from pysimgeo.ctypes import utils as ctypesUtils
# from pysimgeo.pipe import pipe
# import requests

def transGeoCoor(x,y,EpsgIn,EpsgOut):

    # ox,oy=120.61271667480467,23.841253589287316
    # print("EpsgIn,EpsgOut=",EpsgIn,EpsgOut)
    # print("ox,oy=",ox,oy)

    crs_in = pyproj.CRS.from_epsg(EpsgIn)
    crs_out = pyproj.CRS.from_epsg(EpsgOut)
    # print("crs_in=",crs_in)
    # print("crs_out=",crs_out)
    # transformer = Transformer.from_proj(crs_out, crs_in, always_xy=True)
    # (tx,ty) = transformer.transform(ox,oy)
    # print("tx,ty=",tx,ty)
    # x=tx
    # y=ty

    transformer = Transformer.from_proj(crs_in, crs_out, always_xy=True)
    (longitude,latitude) = transformer.transform(x, y)
    # print("x,y=",x,y)
    # print("longitude,latitude=",longitude,latitude)
    # quit()
    return longitude,latitude

def readGeoJSONcoor(data):
    no_polygon=0
    polygonXY=[]
    polygonAB=[]
    polygon_edge_group=[]
    oldEdge_polygon=1

    no_line=0
    lineXY=[]
    lineAB=[]
    line_edge_group=[]
    oldEdge_line=1

    no_point=0
    pointXY=[]


    for feature in data["features"]:
        if feature["geometry"]["type"] == "Polygon":
            no_polygon = no_polygon + 1
            for k in range(0, len(feature["geometry"]["coordinates"][0])-1):
                polygonXY.append(feature["geometry"]["coordinates"][0][k])
            edgeS_polygon=oldEdge_polygon-1
            edgeE_polygon=oldEdge_polygon
            for k in range(0,len(feature["geometry"]["coordinates"][0])-2):
                edgeS_polygon=edgeS_polygon+1
                edgeE_polygon=edgeE_polygon+1
                polygonAB.append([edgeS_polygon,edgeE_polygon])
                polygon_edge_group.append([no_polygon])
            for k in range(len(feature["geometry"]["coordinates"][0])-2,len(feature["geometry"]["coordinates"][0])-1):
                edgeS_polygon=edgeS_polygon+1
                edgeE_polygon=oldEdge_polygon
                polygonAB.append([edgeS_polygon,edgeE_polygon])
                oldEdge_polygon=edgeS_polygon+1
                polygon_edge_group.append([no_polygon])
            # no_polygon_np = no_polygon_np + len(feature["geometry"]["coordinates"][0]) - 1
        elif feature["geometry"]["type"] == "LineString":
            no_line=no_line+1
            for k in range(0, len(feature["geometry"]["coordinates"])):
                lineXY.append(feature["geometry"]["coordinates"][k])
            edgeS_line=oldEdge_line-1
            edgeE_line=oldEdge_line
            for k in range(0,len(feature["geometry"]["coordinates"])-1):
                edgeS_line=edgeS_line+1
                edgeE_line=edgeE_line+1
                lineAB.append([edgeS_line,edgeE_line])
                line_edge_group.append([no_line])
        elif feature["geometry"]["type"] == "Point":
            no_point = no_point +1
            pointXY.append(feature["geometry"]["coordinates"]) 
            

#data to result                
    result = {
        "no_polygon":no_polygon,
        "polygonXY": polygonXY,
        "polygonAB": polygonAB,
        "polygon_edge_group":polygon_edge_group,
        "no_line":no_line,
        "lineXY":lineXY,
        "lineAB":lineAB,
        "line_edge_group":line_edge_group,
        "no_point":no_point,
        "pointXY":pointXY,
    }
    return result

def readLayercoor(data):

    # print(no_polygon,no_polygon_np,no_polygon_nb)           #20210531 10:47

    # print(data)
    # quit()
    no_polygon=0
    polygonXY=[]
    polygonAB=[]
    polygonConductivity=[]
    polygon_edge_group=[]
    polygonRech=[]
    polygonEvt=[]
    polygonTopEle=[]
    polygonBotEle=[]
    oldEdge_polygon=1


    no_line=0
    lineXY=[]
    lineAB=[]
    line_edge_group=[]
    type_line=[]
    line_const_head=[]
    oldEdge_line=1

    no_point=0
    pointXY=[]
    wellTopEle_type=[]
    wellTopEle_value=[]
    wellBotEle_type=[]
    wellBotEle_value=[]
    wellPumping_type=[]
    wellPumping_value=[]


    for semi_data in data:
        for feature in semi_data["features"]:
            if feature["geometry"]["type"] == "Polygon":
                no_polygon = no_polygon + 1
                # # read Polygon Rech
                polygonRech.append(float(feature["properties"]['SourcesAndSinks']['Rech']['value']))
                polygonEvt.append(float(feature["properties"]['SourcesAndSinks']['Evapotranspiration']['MaxET']['value']))
                # # read Polygon Top Elevation
                caseStr=feature["properties"]['AquiferElevationsCalibrationData']['AquiferElevation']['TopElevation']['case']
                # print(caseStr)
                if (caseStr=='Constant'):
                    polygonTopEle.append(float(feature["properties"]['AquiferElevationsCalibrationData']['AquiferElevation']['TopElevation']['cases']['Constant']['value']))
                elif (caseStr=='Random'):
                    print('需要增加code')
                elif (caseStr=='SameAsStartingHead'):
                    print('需要增加code')
                elif (caseStr=='SameAsSurfaceElevation'):
                    print('需要增加code')
                
                # # read Polygon Bot Elevation
                polygonBotEle.append(float(feature["properties"]['AquiferElevationsCalibrationData']['AquiferElevation']['BottomElevation']['value']))


                # print("no_polygon=",no_polygon)                               #20210531 10:47
                # print('len(feature["geometry"]["coordinates"][0])-1=')        #20210531 10:47
                # print(len(feature["geometry"]["coordinates"][0])-1)           #20210531 10:47
                # print('len(feature["geometry"]["coordinates"][0][0])=')       #20210531 10:47
                # print(len(feature["geometry"]["coordinates"][0][0]))          #20210531 10:47
                polygonConductivity.append(float(feature["properties"]['Physical']['Conductivity']['value']))
                for k in range(0, len(feature["geometry"]["coordinates"][0])-1):
                    # print(feature["geometry"]["coordinates"][0][k][0],feature["geometry"]["coordinates"][0][k][1])         #20210531 10:47
                    polygonXY.append(feature["geometry"]["coordinates"][0][k])
                edgeS_polygon=oldEdge_polygon-1
                edgeE_polygon=oldEdge_polygon
                for k in range(0,len(feature["geometry"]["coordinates"][0])-2):
                    edgeS_polygon=edgeS_polygon+1
                    edgeE_polygon=edgeE_polygon+1
                    polygonAB.append([edgeS_polygon,edgeE_polygon])
                    polygon_edge_group.append([no_polygon])
                for k in range(len(feature["geometry"]["coordinates"][0])-2,len(feature["geometry"]["coordinates"][0])-1):
                    edgeS_polygon=edgeS_polygon+1
                    edgeE_polygon=oldEdge_polygon
                    polygonAB.append([edgeS_polygon,edgeE_polygon])
                    oldEdge_polygon=edgeS_polygon+1
                    polygon_edge_group.append([no_polygon])
                # no_polygon_np = no_polygon_np + len(feature["geometry"]["coordinates"][0]) - 1
            elif feature["geometry"]["type"] == "LineString":
                no_line=no_line+1
                # print("no_line=",no_line)
                # print(feature["properties"])
                # type_line.append(feature["properties"]["type_line"])
                caseStr=feature["properties"]['Head']['case']
                if (caseStr=='NonSpecified'):
                    type_line.append(0)
                elif (caseStr=='ConstantHead'):
                    type_line.append(1)
                    line_const_head.append(float(feature["properties"]['Head']['cases']['ConstantHead']['value']))
                elif (caseStr=='SameAs'):
                    type_line.append(2)
                    print('需要增加code')
                elif (caseStr=='VariablePrescribedHead'):
                    type_line.append(3)
                    print('需要增加code')
                elif (caseStr=='River'):
                    type_line.append(4)
                    print('需要增加code')
                elif (caseStr=='Drain'):
                    type_line.append(5)
                    print('需要增加code')

                for k in range(0, len(feature["geometry"]["coordinates"])):
                    lineXY.append(feature["geometry"]["coordinates"][k])
                edgeS_line=oldEdge_line-1
                edgeE_line=oldEdge_line
                for k in range(0,len(feature["geometry"]["coordinates"])-1):
                    edgeS_line=edgeS_line+1
                    edgeE_line=edgeE_line+1
                    lineAB.append([edgeS_line,edgeE_line])                    
                    line_edge_group.append([no_line])
                    # print("lineAB=",lineAB)
                oldEdge_line=edgeS_line+2
            elif feature["geometry"]["type"] == "Point":
                # well TopElevation
                caseStr=feature["properties"]['Well'][
                'TopElevation']['case']
                if (caseStr=='Constant'):
                    wellTopEle_type.append(1)
                    wellTopEle_value.append(float(feature["properties"]['Well']['TopElevation']['cases']['Constant']['value']))
                elif (caseStr=='SameAs'):
                    wellTopEle_type = 2
                    wellTopEle_value.append(None)
                # well BotElevation
                caseStr=feature["properties"]['Well'][
                'BottomElevation']['case']
                if (caseStr=='Constant'):
                    wellBotEle_type.append(1)
                    wellBotEle_value.append(float(feature["properties"]['Well']['BottomElevation']['cases']['Constant']['value']))
                elif (caseStr=='SameAs'):
                    wellBotEle_type = 2
                    wellBotEle_value.append(None)

                caseStr=feature["properties"]['Well'][
                'WellType']['case']
                if (caseStr=='Pumping'):
                    wellPumping_type.append(1)
                    caseStr2=feature["properties"]['Well']['PrescribedHead']['case']
                    if (caseStr2=='Constant'):
                        wellPumping_value.append(float(feature["properties"]['Well']['PrescribedHead']['cases']['Constant']['value']))
                    elif (caseStr2=='EqualsTo'):
                        print('需要增加code')
                        wellPumping_value.append(None)
                    elif (caseStr2=='None'):
                        wellPumping_value.append(None)
                # print(caseStr)
                # print(caseStr2)
                # quit()
                no_point = no_point +1
                pointXY.append(feature["geometry"]["coordinates"])
    # print("polygonEvt=",polygonEvt)
    # quit()
    
    xmax=-1.0e90
    ymax=-1.0e90
    xmin=1.0e90
    ymin=1.0e90
    for i in range(0,len(polygonXY)):
        if (xmax<polygonXY[i][0]): xmax=polygonXY[i][0]
        if (ymax<polygonXY[i][1]): ymax=polygonXY[i][1]
        if (xmin>polygonXY[i][0]): xmin=polygonXY[i][0]
        if (ymin>polygonXY[i][1]): ymin=polygonXY[i][1]
    # print("max=",[xmax,ymax])
    # print("min=",[xmin,ymin])
    # # print("pointXY",pointXY)
    # quit()
    nlay = 10 # nz
    nrow = 20 #20 #4 # ny
    ncol = 20 #20 #5 # nx

    # print(len(lineAB))
    # print(lineAB)
    # quit()

    #data to result                
    result = {
        "xmax":xmax,
        "ymax":ymax,
        "xmin":xmin,
        "ymin":ymin,
        "no_polygon":no_polygon,
        "polygonXY": polygonXY,
        "polygonAB": polygonAB,
        "polygonRech": polygonRech,
        "polygonEvt": polygonEvt,
        "polygonTopEle": polygonTopEle,
        "polygonBotEle": polygonBotEle,
        "polygonConductivity":polygonConductivity,
        "polygon_edge_group":polygon_edge_group,
        "no_line":no_line,
        "lineXY":lineXY,
        "lineAB":lineAB,
        "line_edge_group":line_edge_group,
        "type_line":type_line,
        "line_const_head":line_const_head,
        "no_point":no_point,
        "pointXY":pointXY,
        "wellTopEle_type":wellTopEle_type,
        "wellTopEle_value":wellTopEle_value,
        "wellBotEle_type":wellBotEle_type,
        "wellBotEle_value":wellBotEle_value,
        "wellPumping_type":wellPumping_type,
        "wellPumping_value":wellPumping_value,
        "ncol":ncol,
        "nrow":nrow,
        "nlay":nlay
    }
                


    # print("len(polygonAB)=",len(polygonAB))         #20210531 10:47
    # print("len(polygonXY)=",len(polygonXY))         #20210531 10:47
    # no_polygon_np=len(polygonXY)                    #20210531 10:47
    # no_polygon_nb=len(polygonAB)                    #20210531 10:47
    # print("polygonXY=",polygonXY)                     #20210531 10:47
    # print("======")                                   #20210531 10:47
    # print("polygonAB=",polygonAB)                     #20210531 10:47
    # print("======")                                   #20210531 10:47

    # print("no_line=",no_line)                        #20210531 11:54
    # print("lineXY=",lineXY)                          #20210531 11:54
    # print("lineAB=",lineAB)                          #20210531 11:54
    # print("line_edge_group=",line_edge_group)        #20210531 11:54
    # quit()


    return result
    # return no_polygon,polygonXY,polygonAB,polygon_edge_group,no_line,lineXY,lineAB,line_edge_group

def transGeoJSON(data):
    crs_in = pyproj.CRS.from_epsg(4326)
    crs_out = pyproj.CRS.from_epsg(3826)
    transformer = Transformer.from_proj(crs_in, crs_out, always_xy=True)
   
    features = []
    for feature in data["features"]:
        features.append(feature)
    
    
    no_polygon=0
    no_line=0
    no_point=0
    no_point_group=[]
    no_line_group=[]
    no_polygon_group=[]

    coords=[]
    jsonFileType = 0
    xy = [] 

    for feature in features:
        if feature["geometry"]["type"] == "Polygon":
            jsonFileType=3
        elif feature["geometry"]["type"] == "MultiPolygon":
            jsonFileType=6


    if jsonFileType==3 or 6:
        output = rewind(data) #確保polygon符合外圈逆時針，內圈順時針
        data = output
        features = []
        for feature in data["features"]:
            features.append(feature)   

    NB=-1
    for feature in features:
        # print("NB = ",NB)
        # print(features[NB])
        NB=NB+1
        if feature["geometry"]["type"] == "Point":
            xy = []
            jsonFileType=1
            no_point = no_point + 1
            xy.append(feature["geometry"]["coordinates"])
            # print("point NB = ", no_point)
            # print(features[NB])
            (feature["geometry"]["coordinates"][0],feature["geometry"]["coordinates"][1]) = transformer.transform(xy[0][0], xy[0][1])            

            # print(xy[0][0])
            # print(feature["geometry"]["coordinates"][1])
        elif feature["geometry"]["type"] == "LineString":
            xy = []
            jsonFileType=2
            no_line = no_line + 1
            xy.append(feature["geometry"]["coordinates"]) 
            
            for j in range(0, len(xy)):
                
                for i in range(0, len(xy[j])):
                    # print("len(xy[j][i])=",len(xy[j][i])) 
                    # print("xy[j][i]=",xy[j][i])
                    # print(feature["geometry"]["coordinates"][i])
                    (feature["geometry"]["coordinates"][i][0],feature["geometry"]["coordinates"][i][1]) = transformer.transform(xy[j][i][0], xy[j][i][1])                    

            # for j in range(0, len(xy)): 
            #     for i in range(0, len(xy[j])):
            #         (feature["geometry"]["coordinates"][j][i][0],feature["geometry"]["coordinates"][j][i][1]) = transformer.transform(xy[j][i][0], xy[j][i][1])                    
        elif feature["geometry"]["type"] == "Polygon":
            xy = []
            jsonFileType=3
            no_polygon = no_polygon + 1
            no_polygon_semiPolygon = len(feature["geometry"]["coordinates"])
            no_polygon_group.append(no_polygon_semiPolygon)   
            # print("polygon NB = ", no_polygon)      
            for i in range(0, no_polygon_semiPolygon):
                xy.append(feature["geometry"]["coordinates"][i])
            for j in range(0, len(xy)):       
                for i in range(0, len(xy[j])):
                    (feature["geometry"]["coordinates"][j][i][0],feature["geometry"]["coordinates"][j][i][1]) = transformer.transform(xy[j][i][0], xy[j][i][1])
            # feature["geometry"]["coordinates"]=xy
        elif feature["geometry"]["type"] == "MultiPoint":
            xy = []
            jsonFileType=4
            no_point = no_point + 1
            no_point_semiPoint = len(feature["geometry"]["coordinates"])
            no_point_group.append(no_point_semiPoint)
            for i in range(0, no_point_semiPoint):
                xy.append(feature["geometry"]["coordinates"][i])
        elif feature["geometry"]["type"] == "MultiLineString":
            xy = []
            jsonFileType = 5
            no_line = no_line +1
            no_line_semiLine = len(feature["geometry"]["coordinates"])
            no_line_group.append(no_line_semiLine)
            for i in range(0, no_line_group[no_line-1]):
                xy.append(feature["geometry"]["coordinates"][i])
        elif feature["geometry"]["type"] == "MultiPolygon":
            xy = []
            jsonFileType = 6
            no_polygon = no_polygon +1
            no_polygon_semiPolygon = len(feature["geometry"]["coordinates"])
            no_polygon_group.append(no_polygon_semiPolygon)
            xy.append(feature["geometry"]["coordinates"])
            for k in range(0, len(xy)):
                for j in range(0, len(xy[k])):
                    for i in range(0, len(xy[k][j])):
                        for ii in range(0, len(xy[k][j][i])):
                            # if ii==0:
                                # print('len(xy[k][j][i])=',len(xy[k][j][i]))
                            # print('k,j,i,ii=',k,j,i,ii)
                            # (xy[k][j][i][ii][0],xy[k][j][i][ii][1]) = transformer.transform(xy[k][j][i][ii][0], xy[k][j][i][ii][1])                            
                            (feature["geometry"]["coordinates"][j][i][ii][0],feature["geometry"]["coordinates"][j][i][ii][1]) = transformer.transform(xy[k][j][i][ii][0], xy[k][j][i][ii][1])
                            # print(xy[k][j][i][ii])
    result = data
    return result

def main(data):
    result = transGeoJSON(data)
    return result

# if __name__== "__main__":
#     result = main(data)

'''
def ElevationAPI(x,y):
    # body = '{"format_in":"point","geometry":['+str(x)+','+str(y)+']}'
    # print(body)
    # # quit()
    # # body = {"format_in":"point","geometry":[120.31299591042912,76.03507971212744]}
    # body = {"format_in":"point","geometry":[120.312996,76.035079]}
    # x=120.31299591042912 
    # y=24.09386061742964
  # x = 120.312995 
  # y =  24.093860
    # x = 120.47332763671874
    # y =  24.084081797317943
    # x = 120.957283
    # y =  23.47
    # https://dtm.moi.gov.tw/services/polate/polate.asmx/getPolate?apikey=&wkt=MULTIPOINT((120.31299591042912 24.09386061742964))&data=TW_DLA_20010814_20061226_20M_3826
    # https://dtm.moi.gov.tw/services/polate/polate.asmx/getPolate?apikey=&wkt=MULTIPOINT((121.29571522027256 24.984277301840486))&data=TW_DLA_20110101_20161101_20M_3826_DEM

    # x = 120.36449432222287 
    # y = 24.078314451029897
    URL = 'https://dtm.moi.gov.tw/services/polate/polate.asmx/getPolate?apikey=&wkt=MULTIPOINT(('+str(x)+ '%20' + str(y) +'))&data=TW_DLA_20110101_20161101_20M_3826_DEM'

    # call = requests.get('https://dtm.moi.gov.tw/services/polate/polate.asmx/getPolate?apikey=&wkt=MULTIPOINT((111.29571522027256%2024.984277301840486))&data=TW_DLA_20110101_20161101_20M_3826_DEM')
    call = requests.get(URL)    
    text = call.text
    # print(text)
    for i in range(0, len(text)):
        if text[i]=='{':
            SE=i
            exit
    # print(SE)
    for i in range(len(text)-1,0,-1):
        if text[i]=='}':
            EE=i+1
            exit
    # print(EE)
    jsonData = json.loads(text[SE:EE])
    # print(jsonData)
    err =-9998.0
    if 'z' in jsonData:
        # print(jsonData["z"])
        z = float(jsonData["z"])
        if (z < err) :
            z = 0.0
    else:
        z = 0.0

    # print(z)
    # quit()

    # body = '[['+str(x)+','+str(y)+']]'
    # headers = {
    #     'Accept': 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8','Authorization': 'Bearer eyJqdGkiOiJmYzE1ZGIzOGNjYjI0NTFlYWIzNzU5Yzk4Mzg5YjExMCJ9','Content-Type': 'application/json'
    # }
    # call = requests.post('https://outdoorsafetylab.org/api/v1/elevations', data=body, headers=headers)
    # statusCode = call.status_code
    # if (statusCode==200) :
    #     res = json.loads(call.text)
    #     z=res[0]
    # if z is None:
    #     z = 0.0


    # quit()
    
    # word = call.text.strip()
    # word = word.replace("[","")
    # word = word.replace("'","")
    # print(float(word))
    # data = {
    #     "z": res[0]
    # }
    # print(data)
    # quit()
    
    # curl -X POST \
    # -H "Authorization: Bearer eyJqdGkiOiJmYzE1ZGIzOGNjYjI0NTFlYWIzNzU5Yzk4Mzg5YjExMCJ9" \
    # -H "Content-Type: application/json" \
    # -d '[[120.957283,23.47]]' \
    

    # z = float(data["geometry"]["coordinates"][2])
    return z
'''
