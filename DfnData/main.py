import numpy as np
import pandas as pd
import time
import dem
import pyvista as pv
import plotPyvista

import os

if __name__ == "__main__":
  start = time.time()
  
  OriginPointsF=r'../../INER_DATA/Domain2D/OriginPoints.txt'
  # path=r'../../HybridTW/000_fabToMesh/testWriteMeshALL.poly'
  # q123Folder=r'../../HybridTW/001_stlToMesh'
  # FABResfolder=r'../../HybridTW/000_fabToMesh/FABRes'

  EpsgIn=3825
  EpsgOut=4326

  '''
  # 模擬底部高程
  '''
  STLminZ=-2000.0

  '''
  # 表土層深度
  '''
  depthSurfaceSoil=-70.0  

  # OriginPointsF=r'/root/ihsienlee/CGAL/CGAL_LEE/INER_DATA/Domain2D/OriginPoints.txt'
  orPointCsv = pd.read_csv(OriginPointsF,sep='\\s+')
  orPointCsv = orPointCsv.fillna('')
  orPointCsv.columns=["x", "y"]
  delta_x=np.float64(orPointCsv['x'].values[0])
  delta_y=np.float64(orPointCsv['y'].values[0])
  # print(orPointCsv)
  print("delta_x,delta_y=",delta_x,delta_y)
  # quit()  

  '''
  # DTM 與 模擬範圍
  '''
  CsvFile=r'../../INER_DATA/Domain2D/DEM_120m_resolution500m_201x_139y.dat'
  domainFile=r'../../INER_DATA/Domain2D/domainboundary_large.xy'
  nx=201
  ny=139
  # CsvFile=r'../../INER_DATA/Domain2D/DTM_largescale_263x_263y.dat'
  # nx=263
  # ny=263  

  case=1
  if (case==1):
    '''
    # 讀取500m DEM
    '''    
    xyzCsv = pd.read_csv(CsvFile,sep='\\s+',header=None)
    xyzCsv = xyzCsv.fillna('')
    xyzCsv.columns=["x", "y", "z"]   
    # print(xyzCsv) 
    # quit()
    '''csv points to vtk'''
    points=np.zeros((len(xyzCsv),3),dtype=np.float64)
    points[:,0]=xyzCsv['x'].values[:]
    points[:,1]=xyzCsv['y'].values[:]
    xyzPoints = pv.PolyData(points)
    xyzPoints.save("xyzPoints.vtk")

    ''' # 讀取模擬範圍,並且依據dem計算表面高程 '''    
    xyzDomain = pd.read_csv(domainFile,sep='\\s+',header=None)
    xyzDomain = xyzDomain.fillna('')
    xyzDomain.columns=["x", "y"]

    ''' #建立模擬範圍、DTM vtk檔案 '''
    domain2d,demClip2d,largeDem = dem.demToMesh(
      nx,ny,
      xyzCsv,
      xyzDomain)
    print("read dem and doamin files")

    ''' #將DTM切割為模擬範圍形狀,並完成座標移動'''
    domain2d,demClip2d,largeDem = plotPyvista.dem2dClip(
      delta_x,delta_y,
      EpsgIn,EpsgOut,
      domain2d,demClip2d,largeDem)
  # quit()
  domain2d=pv.read('domain2d.vtk')
  demClip2d=pv.read('demClip2d.vtk')
  largeDem=pv.read('largeDem.vtk') 
  # print("largeDem") 
  # print(largeDem)

  case=1
  if (case==1):   
    STLminZ=np.float64(STLminZ)
    '''
    use demClip2d create 3d dem
    '''
    demClip3dsurf=plotPyvista.dem2dTo3d(demClip2d)
    StlFile=r'../../INER_DATA/STL/GeoModel_SKB_AModify_fault_1.stl'
    fault_1=pv.read(StlFile)
    StlFile=r'../../INER_DATA/STL/GeoModel_SKB_AModify_fracture.stl'
    fault_2=pv.read(StlFile)
    fault_1Mtr=100.0
    fault_2Mtr=20.0
    fault12=plotPyvista.faultStl(demClip2d,delta_x,delta_y,fault_1,fault_2,fault_1Mtr,fault_2Mtr)
    boxMtr=500.0
    ''' #創造3D多面體,組合成'''
    poly3d=plotPyvista.mergeOutsideBox(depthSurfaceSoil,demClip3dsurf,STLminZ,boxMtr)    
  else:
    demClip3dsurf=pv.read('demClip3dsurf.vtk')
    poly3d=pv.read('poly3d.vtk')
    fault12=pv.read('fault12.vtk')
  # print(fault12.array_names)
  # print(poly3d.array_names)
  # print('poly3d, demclip3d and fault12')

  case=1
  if (case==1):
    ''' #讀取DFN參數與座標'''
    dfndataF=r'dfnData.csv'
    dfndata = pd.read_csv(dfndataF)
    
    no_node=dfndata['no_node']
    Transmissivity=dfndata['Transmissivity']
    Storativity=dfndata['Storativity']
    aperture=dfndata['aperture']
    
    ''' # 計算K=T/b'''
    fractureK=Transmissivity/aperture

    ''' #fracNb=裂隙編號; 初始化'''
    fracNb=0

    ''' #lineNb=dfnData.csv檔案行數編號; 初始化'''
    lineNb=0

    ''' # len_dfndata=dfnData.csv檔案[總]行數'''
    len_dfndata=len(no_node)

    ''' # 創立空的vtk'''
    dfnmesh = pv.PolyData()
  
    ''' # 當lineNb小於len_dfndata,代表並未讀完dfnData.csv'''
    while(lineNb<len_dfndata):
      ''' # 裂隙編號+1'''    
      fracNb=fracNb+1

      ''' # 讀取裂隙節點個數 = no_each_frac_node'''
      no_each_frac_node=np.int64(no_node[lineNb])

      ''' # 計算單一個別裂隙K'''
      fractureK=np.float64(Transmissivity[lineNb])/np.float64(aperture[lineNb])
      fractureAperture=np.float64(aperture[lineNb])

      ''' # 組裝單一裂隙的PolyData'''
      xyz=np.zeros((no_each_frac_node,3),dtype=np.float64)
      faces=np.linspace(start=-1,stop=no_each_frac_node,
                        num=no_each_frac_node+1,
                        endpoint=False,
                        dtype=np.int64)
      faces[0]=no_each_frac_node
      xyz[:,0]=np.float64(Transmissivity[lineNb+1:lineNb+1+no_each_frac_node])
      xyz[:,1]=np.float64(Storativity[lineNb+1:lineNb+1+no_each_frac_node])
      xyz[:,2]=np.float64(aperture[lineNb+1:lineNb+1+no_each_frac_node])

      ''' 計算下一個裂隙組在dfnData.csv的行數'''
      lineNb=lineNb+1+no_each_frac_node

      ''' # 創立單一裂隙為PolyData'''
      dfnpoly=pv.PolyData(xyz,faces)

      ''' # 移動座標'''
      dfnpoly.translate([delta_x,delta_y,0],inplace=True)

      ''' 將dfnData.csv的水文參數加入PolyData'''
      dfnpoly['fractureK']=[fractureK]
      dfnpoly['aperture']=[fractureAperture]
      dfnpoly['No']=[fracNb]

      ''' 累加裂隙PolyData'''
      dfnmesh=dfnmesh+dfnpoly

    dfnmesh.save('dfnPoly.vtk') 
    dfnPoly=dfnmesh
    print('fracNb=',fracNb)
  elif (case==0):
    dfnPoly=pv.read('dfnPoly.vtk') 
  print("dfnPoly",dfnPoly)
  print("dfnPoly.array_names=",dfnPoly.array_names)
  print("noTestFrac=",dfnPoly.n_cells)

  case=1
  if (case==1):
    noface=np.max(dfnPoly["No"])
    ind=np.zeros(noface,dtype=np.int64)
    fractureK=np.zeros(noface,dtype=np.float64)
    aperture=np.zeros(noface,dtype=np.float64)
    for i in range(0,dfnPoly.n_cells):
      # if (ind[dfnPoly["No"][i]-1]==0):
      #  fractureK[dfnPoly["No"][i]-1]=dfnPoly["fractureK"][i]
      #  aperture[dfnPoly["No"][i]-1]=dfnPoly["aperture"][i]
      if (ind[i]==0):
       fractureK[i]=dfnPoly["fractureK"][i]
       aperture[i]=dfnPoly["aperture"][i]
       
    ''' # 將水文參數利用numpy模組儲存為檔案'''
    np.savez('dfnPolyNp', 
    fractureK=fractureK, 
    aperture=aperture)
    print("save dfnPolyNp OK")  
  dfnPolyNp = np.load('dfnPolyNp.npz')
  print("dfnPolyNp OK")    